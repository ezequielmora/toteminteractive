﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepScale : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        try
        {
            if(transform.parent.transform.localScale.x != 0 && transform.parent.transform.localScale.y != 0 && transform.parent.transform.localScale.z != 0)
            transform.localScale = new Vector3(1 / transform.parent.transform.localScale.x, 1 / transform.parent.transform.localScale.y, 1 / transform.parent.transform.localScale.z);
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }
}
