﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepSize : MonoBehaviour {

    public RectTransform skinObject;

    private RectTransform thisObject;

	// Use this for initialization
	void Start () {
        thisObject = this.gameObject.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        thisObject.GetComponent<RectTransform>().sizeDelta = new Vector2(skinObject.GetComponent<RectTransform>().sizeDelta.x, skinObject.GetComponent<RectTransform>().sizeDelta.y);
	}
}
