﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UI.Pagination;

//IDragHandler
public class OnScrollDetect :  MonoBehaviour, IEndDragHandler, IBeginDragHandler {

    public PagedRect_ScrollRect rectVertical;

    public PagedGlobalHandler globalHandler;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnBeginDrag(PointerEventData data)
    {
        rectVertical.GetComponent<PagedRect_ScrollRect>().onValueChanged.RemoveAllListeners();
        Debug.Log("drag start called.");
    }

    public void OnEndDrag(PointerEventData data)
    {
        rectVertical.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(globalHandler.GetComponent<PagedGlobalHandler>().ChangeParallelButtonNew);
        Debug.Log("drag end called.");
    }
}
