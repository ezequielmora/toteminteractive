﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GlobalButtonView : MonoBehaviour {

    [Header("Numero Correspondiente al Boton")]
    [SerializeField]
    private int buttonNumber;
    public int ButtonNumber
    { 
        get
        {
            return buttonNumber;

        }
        set
        {
            buttonNumber = value;
        }
    }


    [Header("GameObject correspondiente al nombre popular")]
    public TextMeshProUGUI nombrePopular;
    [Header("GameObject correspondiente al nombre cientifico")]
    public TextMeshProUGUI nombreCientifico;


	// Use this for initialization
	void Start () {
        //buttonNumber = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
