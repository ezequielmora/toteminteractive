﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using SQL.Structs;

public class videoFormView : MonoBehaviour {

    [Header("Numero Correspondiente al Video en la lista")]
    [SerializeField]
    private int videoNumber;
    public int VideoNumber
    {
        get
        {
            return videoNumber;

        }
        set
        {
            videoNumber = value;
        }
    }

    [Header("Listado de etiquetas del video en string")]
    [SerializeField]
    private List<string> videoTags;
    public List<string> VideoTags
    {
        get
        {
            return videoTags;
        }
        set
        {
            videoTags = value;
        }
    }


    [Header("URL PATH DEL VIDEO CORRESPONDIENTE")]
    [SerializeField]
    private string urlPath;
    public string UlrPath
    {
        get
        {
            return urlPath;
        }
        set
        {
            urlPath = value;
        }
    }
		
    [Header("Tags del video")]
    //[SerializeField]
    private List<string> tagList;
    public List<string> TagList
    {
        get
        {
            return tagList;
        }
        set
        {
            tagList = value;
        }
    }


    [Header("Bloque sobre el que se aplicará el color")]
    [SerializeField]
    public Image imageToColorize;

    

    
    [Header("Objeto correspondiente a Mini BG del Video")]
    [SerializeField]
    private Image miniBG;
    public Image MiniBG
    {
        get
        {
            return miniBG;
        }
        set
        {
            miniBG = value;
        }
    }
    
    [Header("Objeto correspondiente a FULL BG del Video")]
    [SerializeField]
    private Sprite fullBG;
    public Sprite FullBG
    {
        get
        {
            return fullBG;
        }
        set
        {
			fullBG = value;
        }
    }


    [Header("GameObject correspondiente al texto inferior del video")]
    [SerializeField]
    private TextMeshProUGUI textoCaratulaDelVideo;
    public TextMeshProUGUI TextoCaratulaDelVideo
    {
        get
        {
            return textoCaratulaDelVideo;
        }
    }


    [Header("GameObject correspondiente al Titulo del Bloque")]
    [SerializeField]
    private string tituloTextoBloque;
    public string TituloTextoBloque
    {
        get
        {
            return tituloTextoBloque;
        }
        set
        {
            tituloTextoBloque = value;
        }
    }


    [Header("GameObject correspondiente al Texto del Bloque de Video")]
    [SerializeField]
    private string textoDelBloque;
    public string TextoDelBloque
    {
        get
        {
			return textoDelBloque;
        }
        set
        {
            tituloTextoBloque = value;
        }
    }


    [Header("Texto correspondiente Direccion")]
    [SerializeField]
    private string textoDireccion;
    public string TextoDireccion
    {
        get
        {
            return textoDireccion;
        }
        set
        {
            textoDireccion = value;
        }
    }

    [Header("Texto correspondiente Guion")]
    [SerializeField]
    private string textoGuion;
    public string TextoGuion
    {
        get
        {
            return textoGuion;
        }
        set
        {
            textoGuion = value;
        }
    }

    [Header("Texto correspondiente Produccion")]
    [SerializeField]
    private string textoProduccion;
    public string TextoProduccion
    {
        get
        {
            return textoProduccion;
        }
        set
        {
            textoProduccion = value;
        }
    }
}
