﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UI.Pagination;
using System.Linq;

public class videoPlayerController : MonoBehaviour {

    /// <summary>
    /// Controlador que maneja el videoplayer y todos los cambios de ventanas entre si
    /// </summary>
    /// 

    [Header("Paginas de todos los videos invocados en runtime")]
    [SerializeField]
    private List<Page> videoPages = new List<Page>();
    public List<Page> VideoPages
    {
        get
        {
            return videoPages;
        }
    }

    [Header("Botones que controlará el reproductor")]
    public Button playButton;
    public Button restartButton;
    public Button audioSwitch;
    public Button audioSwitchOFF;
    
    [Header("Contenedores de FULL Image y Video que habilita y deshabilita")]
    public Image fullIMGArea;
    public RawImage videoAndPauseArea;

    [Header("URL del video que reproducira el player")]
    private string currentUrlPath = "http://192.168.1.8/video/content/totem_videos/TOTEM_VIDEOS_216_e5303f5d0ef2f139c3a4b819d6df987b_video-ogg?mime=video/ogg";
    public string CurrentUrlPath
    {
        set
        {
            currentUrlPath = value;
        }
    }


    public bool isVideoPlaying = false;

    public bool isStarted = false;

    private MovieTexture myClip;
    private AudioSource myAudio;
    private bool initialLoaded = false;

    private bool isPaused = false;

    private Action callback;

    [Header("Componente Audio Source Relacionado al video que se habilitara y deshabilitara")]
    public AudioSource myAudioSource;
    private bool soundSwitched;

    [Header("Animator que define la animacion de transicion por un cambio de ficha de video")]
    public Animator changeInfoAnimator;





    [Header("Bloque de imagen que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private Image currentFullBG;
    public Image CurrentFullBG
    {
        get
        {
            return currentFullBG;
        }
        set
        {
            currentFullBG = value;
        }
    }

    [Header("Bloque de color principal que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private Image bloquePrincipal;
    public Image BloquePrincipal
    {
        get
        {
            return bloquePrincipal;
        }
        set
        {
            bloquePrincipal.color = value.color;
        }
    }

    [Header("Bloque de Titulo del texto principal que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private TextMeshProUGUI currentTitle;
    public TextMeshProUGUI CurrentTitle
    {
        get
        {
            return currentTitle;
        }
        set
        {
            currentTitle = value;
        }
    }

    [Header("Bloque de texto principal que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private TextMeshProUGUI currentDescription;
    public TextMeshProUGUI CurrentDescription
    {
        get
        {
            return currentDescription;
        }
        set
        {
            currentDescription = value;
        }
    }

    [Header("Bloque de color secundario que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private Image bloqueSecundario;
    public Image BloqueSecundario
    {
        get
        {
            return bloqueSecundario;
        }
        set
        {
            bloqueSecundario.color = value.color;
        }
    }

    [Header("Bloque de texto secundario de Guion que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private TextMeshProUGUI currentGuion;
    public TextMeshProUGUI CurrentGuion
    {
        get
        {
            return currentGuion;
        }
        set
        {
            currentGuion = value;
        }
    }

    [Header("Bloque de texto secundario de direccion que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private TextMeshProUGUI currentDireccion;
    public TextMeshProUGUI CurrentDireccion
    {
        get
        {
            return currentDireccion;
        }
        set
        {
            currentDireccion = value;
        }
    }

    [Header("Bloque de texto secundario de produccion que seteará cada vez que se seleccione un video")]
    [SerializeField]
    private TextMeshProUGUI currentProduccion;
    public TextMeshProUGUI CurrentProduccion
    {
        get
        {
            return currentProduccion;
        }
        set
        {
            currentProduccion = value;
        }
    }

    [Serializable]
    public class VideoFilters
    {
        [Header("Nombres de los Filtros de Video")]
        [SerializeField]
        private string filterName;
        public string FilterName
        {
            get
            {
                return filterName;
            }
        }

        [Header("Boton de UI correspondiente a ese Filtro de Video")]
        [SerializeField]
        private Button videoFilterButton;
        public Button VideoFilterButton
        {
            get
            {
                return videoFilterButton;
            }
        }

        
    }

    [Header("Marcadores de seleccion del botones de filtro")]
    //[SerializeField]
    public Image filterMarkTodos;
    public Image filterMarkCiencia;
    public Image filterMarkArte;
    public Image filterMarkRio;
    public Image filterMarkAcuario;






    [Header("Listado de todos los TIPOS / TAGS de Videos")]
    [SerializeField]
    private List<VideoFilters> myVideoFilters;


    // Poner Listeners de Botones y arrancan con OnEnable
    void Start()
    {
        playButton.onClick.AddListener(btn_Play);
        restartButton.onClick.AddListener(btn_Restart);
        // Boton Encendido
        audioSwitch.onClick.AddListener(btn_SoundSwitch);
        // Boton Apagado
        audioSwitchOFF.onClick.AddListener(btn_SoundSwitch);


        EventTrigger pointerTrigger = videoAndPauseArea.GetComponent<EventTrigger>();
        EventTrigger.Entry pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;


        pointerTrigger.triggers.Add(pointerDownEntry);

        pointerDownEntry.callback.AddListener((eventData) =>
        {
            //Do something when pointer is down the button
            block_Pause();

        });

        foreach (var item in myVideoFilters)
        {


            switch (item.FilterName)
            {
                case "Ciencia":

                    item.VideoFilterButton.GetComponent<Button>().onClick.AddListener(() => ClickedCiencia());

                    break;

                case "Arte":

                    item.VideoFilterButton.GetComponent<Button>().onClick.AddListener(() => ClickedArte());


                    break;


                case "Acuario":

                    item.VideoFilterButton.GetComponent<Button>().onClick.AddListener(() => ClickedAcuario());


                    break;

                case "Rio":

                    item.VideoFilterButton.GetComponent<Button>().onClick.AddListener(() => ClickedRio());


                    break;


                case "Todos":


                    item.VideoFilterButton.GetComponent<Button>().onClick.AddListener(() => ClickedTodos());


                    break;



            }

        }
    }





    void ClickedCiencia()
    {
        print("CLICKED CIENCIA");

        StartCoroutine(CienciaRoutine());


        filterMarkTodos.gameObject.SetActive(false);
        filterMarkAcuario.gameObject.SetActive(false);
        filterMarkArte.gameObject.SetActive(false);
        filterMarkCiencia.gameObject.SetActive(true);
        filterMarkRio.gameObject.SetActive(false);

        //this.gameObject.GetComponent<PagedRect>().RemoveAllPages();


        /*
        for (int i = 0; i < PagedVideoHandler.Instance.videoData.Count; i++)
        {

        }
        */
        /*
        foreach (var item in videoPages)
        {

            item.GetComponent<videoFormView>().gameObject.SetActive(false);



            this.gameObject.GetComponent<PagedRect>().UpdatePages();

            this.gameObject.GetComponent<PagedRect>().UpdatePagination();


        }
        foreach (var item in videoPages)
        {

            if (item.GetComponent<videoFormView>().VideoTags.Contains("Ciencia"))
            {
                item.GetComponent<videoFormView>().gameObject.SetActive(true);

                this.gameObject.GetComponent<PagedRect>().AddPage(item);


                this.gameObject.GetComponent<PagedRect>().UpdatePages();

                this.gameObject.GetComponent<PagedRect>().UpdatePagination();
            }
        }
            
        */


    }

    void ClickedArte()
    {
        print("CLICKED ARTE");

        StartCoroutine(ArteRoutine());


        filterMarkTodos.gameObject.SetActive(false);
        filterMarkAcuario.gameObject.SetActive(false);
        filterMarkArte.gameObject.SetActive(true);
        filterMarkCiencia.gameObject.SetActive(false);
        filterMarkRio.gameObject.SetActive(false);

        /*
        //foreach (var item in this.gameObject.GetComponent<PagedRect>().Pages)
        foreach (var item in videoPages)
        {

            //videoFormView tagList = item.GetComponent<videoFormView>().VideoTags.Find(x => x.Contains == Tecno);
            //item.GetComponent<videoFormView>().TagList.Find("Tecno").

            item.GetComponent<videoFormView>().gameObject.SetActive(false);

            this.gameObject.GetComponent<PagedRect>().RemovePage(item);


            this.gameObject.GetComponent<PagedRect>().UpdatePages();

            this.gameObject.GetComponent<PagedRect>().UpdatePagination();



            //GameObject pageToHide = item.GetComponent<videoFormView>().VideoTags.Contains("Tecno"); //Find(obj => obj.name == "Gun");

            if (item.GetComponent<videoFormView>().VideoTags.Contains("Arte"))
            {
                item.GetComponent<videoFormView>().gameObject.SetActive(true);

                this.gameObject.GetComponent<PagedRect>().AddPage(item);

                this.gameObject.GetComponent<PagedRect>().UpdatePages();

                this.gameObject.GetComponent<PagedRect>().UpdatePagination();
            }

        }
        */
    }


    void ClickedAcuario()
    {
        print("CLICKED ACUARIO");

        StartCoroutine(AcuarioRoutine());


        filterMarkTodos.gameObject.SetActive(false);
        filterMarkAcuario.gameObject.SetActive(true);
        filterMarkArte.gameObject.SetActive(false);
        filterMarkCiencia.gameObject.SetActive(false);
        filterMarkRio.gameObject.SetActive(false);

        /*
        //foreach (var item in this.gameObject.GetComponent<PagedRect>().Pages)
        foreach (var item in videoPages)
        {

            //videoFormView tagList = item.GetComponent<videoFormView>().VideoTags.Find(x => x.Contains == Tecno);
            //item.GetComponent<videoFormView>().TagList.Find("Tecno").

            item.GetComponent<videoFormView>().gameObject.SetActive(false);

            this.gameObject.GetComponent<PagedRect>().RemovePage(item);


            this.gameObject.GetComponent<PagedRect>().UpdatePages();

            this.gameObject.GetComponent<PagedRect>().UpdatePagination();



            //GameObject pageToHide = item.GetComponent<videoFormView>().VideoTags.Contains("Tecno"); //Find(obj => obj.name == "Gun");

            if (item.GetComponent<videoFormView>().VideoTags.Contains("Acuario"))
            {
                item.GetComponent<videoFormView>().gameObject.SetActive(true);


                this.gameObject.GetComponent<PagedRect>().AddPage(item);


                this.gameObject.GetComponent<PagedRect>().UpdatePages();

                this.gameObject.GetComponent<PagedRect>().UpdatePagination();
            }

        }
        */
    }

    void ClickedRio()
    {
        print("CLICKED RIO");

        StartCoroutine(RioRoutine());


        filterMarkTodos.gameObject.SetActive(false);
        filterMarkAcuario.gameObject.SetActive(false);
        filterMarkArte.gameObject.SetActive(false);
        filterMarkCiencia.gameObject.SetActive(false);
        filterMarkRio.gameObject.SetActive(true);

        /*
        //foreach (var item in this.gameObject.GetComponent<PagedRect>().Pages)
        foreach (var item in videoPages)
        {

            //videoFormView tagList = item.GetComponent<videoFormView>().VideoTags.Find(x => x.Contains == Tecno);
            //item.GetComponent<videoFormView>().TagList.Find("Tecno").

            item.GetComponent<videoFormView>().gameObject.SetActive(false);

            this.gameObject.GetComponent<PagedRect>().RemovePage(item);

            this.gameObject.GetComponent<PagedRect>().UpdatePages();

            this.gameObject.GetComponent<PagedRect>().UpdatePagination();

            //GameObject pageToHide = item.GetComponent<videoFormView>().VideoTags.Contains("Tecno"); //Find(obj => obj.name == "Gun");

            if (item.GetComponent<videoFormView>().VideoTags.Contains("Rio"))
            {
                item.GetComponent<videoFormView>().gameObject.SetActive(true);

                this.gameObject.GetComponent<PagedRect>().AddPage(item);


                this.gameObject.GetComponent<PagedRect>().UpdatePages();

                this.gameObject.GetComponent<PagedRect>().UpdatePagination();
            }

        }
        */
    }

    void ClickedTodos()
    {
        print("CLICKED TODOS");

        StartCoroutine(TodosRoutine());

        filterMarkTodos.gameObject.SetActive(true);
        filterMarkAcuario.gameObject.SetActive(false);
        filterMarkArte.gameObject.SetActive(false);
        filterMarkCiencia.gameObject.SetActive(false);
        filterMarkRio.gameObject.SetActive(false);

    /*
    foreach (var item in videoPages)
    {


        item.GetComponent<videoFormView>().gameObject.SetActive(false);

        this.gameObject.GetComponent<PagedRect>().RemovePage(item);

        this.gameObject.GetComponent<PagedRect>().UpdatePages();

        this.gameObject.GetComponent<PagedRect>().UpdatePagination();


    }


    foreach (var item in videoPages)
    {


        item.GetComponent<videoFormView>().gameObject.SetActive(true);

        this.gameObject.GetComponent<PagedRect>().AddPage(item);


        this.gameObject.GetComponent<PagedRect>().UpdatePages();

        this.gameObject.GetComponent<PagedRect>().UpdatePagination();


    }*/

    }

    IEnumerator CienciaRoutine()
    {

        PagedRect paged = this.gameObject.GetComponent<PagedRect>();
        paged.RemoveAllPages();
        paged.UpdatePages();
        paged.UpdatePagination();

        foreach (var itemPage in paged.Pages)
        {
            itemPage.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        }

        List<String> tempTags;

        ////

        for (int i = 0; i < PagedVideoHandler.Instance.videoData.Count; i++)
        {
            tempTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

            if (tempTags.Contains("Ciencia"))
            {


                var page = paged.AddPageUsingTemplate();
                page.PageTitle = "Number" + paged.NumberOfPages;


                // Update Pagination to update title and fields
                paged.UpdatePagination();


                // Agregar paginas a lista en el reproductor
                //paged.VideoPages.Add(page);





                // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                page.GetComponent<videoFormView>().VideoNumber = paged.NumberOfPages;

                /////////////////
                ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
                /////////////////
                ///// DESCOMENTAR
                page.GetComponent<videoFormView>().TextoCaratulaDelVideo.text = PagedVideoHandler.Instance.videoData[i].titleDescText; //variable_titulo_texto_bloque.count; // Misma variable arriba y abajo
                page.GetComponent<videoFormView>().TituloTextoBloque = PagedVideoHandler.Instance.videoData[i].titleDescText; // Misma variable arriba
                page.GetComponent<videoFormView>().TextoDelBloque = PagedVideoHandler.Instance.videoData[i].descText; //variable_descripcion_json.count;
                page.GetComponent<videoFormView>().UlrPath = PagedVideoHandler.Instance.videoData[i].videoURL; //variable_de_url_del_json.count;

                //print("URL DE VIDEOS: " + videoData[i].videoURL);
                page.GetComponent<videoFormView>().VideoTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

                //page.GetComponent<videoFormView>().TagList = videoData[i].tags; //variable_de_tags_para_ese_video.count;
                page.GetComponent<videoFormView>().TextoDireccion = PagedVideoHandler.Instance.videoData[i].textDireccion; //variable_direccion_json.count;
                page.GetComponent<videoFormView>().TextoGuion = PagedVideoHandler.Instance.videoData[i].textGuion; // variable_guion_json.count;
                page.GetComponent<videoFormView>().TextoProduccion = PagedVideoHandler.Instance.videoData[i].textProduccion; //variable_produccion_json.count;
                page.GetComponent<videoFormView>().MiniBG.sprite = PagedVideoHandler.Instance.videoData[i].miniBGSprite; //variable_minibg_json.count;
                page.GetComponent<videoFormView>().FullBG = PagedVideoHandler.Instance.videoData[i].fullBGSprite; //variable_fullbg_json.count;


            }
        }



        paged.UpdatePages();


        //selectorVideo = this.gameObject.GetComponent<SelectorVideoHelper>();

        // Sacar Init() por el listener de onvaluedchanged de paged - solo la primera vez
        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SetNumber());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.DisableAllColor());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SelectorCreation());

        foreach (var itemPage in paged.Pages)
        {
            var pageVideoCell = itemPage.GetComponent<videoFormView>();


            itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => SelectorVideoHelper.Instance.goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));
        }

        PagedVideoHandler.Instance.SetInPlayer();

        yield return null;
        //
        
    }


    IEnumerator RioRoutine()
    {

        PagedRect paged = this.gameObject.GetComponent<PagedRect>();
        paged.RemoveAllPages();
        paged.UpdatePages();
        paged.UpdatePagination();

        foreach (var itemPage in paged.Pages)
        {
            itemPage.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        }


        List<String> tempTags;

        ////

        for (int i = 0; i < PagedVideoHandler.Instance.videoData.Count; i++)
        {
            tempTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

            if (tempTags.Contains("Rio"))
            {


                var page = paged.AddPageUsingTemplate();
                page.PageTitle = "Number" + paged.NumberOfPages;


                // Update Pagination to update title and fields
                paged.UpdatePagination();


                // Agregar paginas a lista en el reproductor
                //paged.VideoPages.Add(page);





                // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                page.GetComponent<videoFormView>().VideoNumber = paged.NumberOfPages;

                /////////////////
                ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
                /////////////////
                ///// DESCOMENTAR
                page.GetComponent<videoFormView>().TextoCaratulaDelVideo.text = PagedVideoHandler.Instance.videoData[i].titleDescText; //variable_titulo_texto_bloque.count; // Misma variable arriba y abajo
                page.GetComponent<videoFormView>().TituloTextoBloque = PagedVideoHandler.Instance.videoData[i].titleDescText; // Misma variable arriba
                page.GetComponent<videoFormView>().TextoDelBloque = PagedVideoHandler.Instance.videoData[i].descText; //variable_descripcion_json.count;
                page.GetComponent<videoFormView>().UlrPath = PagedVideoHandler.Instance.videoData[i].videoURL; //variable_de_url_del_json.count;

                //print("URL DE VIDEOS: " + videoData[i].videoURL);
                page.GetComponent<videoFormView>().VideoTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

                //page.GetComponent<videoFormView>().TagList = videoData[i].tags; //variable_de_tags_para_ese_video.count;
                page.GetComponent<videoFormView>().TextoDireccion = PagedVideoHandler.Instance.videoData[i].textDireccion; //variable_direccion_json.count;
                page.GetComponent<videoFormView>().TextoGuion = PagedVideoHandler.Instance.videoData[i].textGuion; // variable_guion_json.count;
                page.GetComponent<videoFormView>().TextoProduccion = PagedVideoHandler.Instance.videoData[i].textProduccion; //variable_produccion_json.count;
                page.GetComponent<videoFormView>().MiniBG.sprite = PagedVideoHandler.Instance.videoData[i].miniBGSprite; //variable_minibg_json.count;
                page.GetComponent<videoFormView>().FullBG = PagedVideoHandler.Instance.videoData[i].fullBGSprite; //variable_fullbg_json.count;


            }
        }


        paged.UpdatePages();



        //selectorVideo = this.gameObject.GetComponent<SelectorVideoHelper>();

        // Sacar Init() por el listener de onvaluedchanged de paged - solo la primera vez
        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SetNumber());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.DisableAllColor());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SelectorCreation());

        foreach (var itemPage in paged.Pages)
        {
            var pageVideoCell = itemPage.GetComponent<videoFormView>();


            itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => SelectorVideoHelper.Instance.goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));
        }

        PagedVideoHandler.Instance.SetInPlayer();

        yield return null;
        //

    }


    IEnumerator AcuarioRoutine()
    {

        PagedRect paged = this.gameObject.GetComponent<PagedRect>();
        paged.RemoveAllPages();
        paged.UpdatePages();
        paged.UpdatePagination();

        foreach (var itemPage in paged.Pages)
        {
            itemPage.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        }


        List<String> tempTags;

        ////

        for (int i = 0; i < PagedVideoHandler.Instance.videoData.Count; i++)
        {
            tempTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

            if (tempTags.Contains("Acuario"))
            {


                var page = paged.AddPageUsingTemplate();
                page.PageTitle = "Number" + paged.NumberOfPages;


                // Update Pagination to update title and fields
                paged.UpdatePagination();


                // Agregar paginas a lista en el reproductor
                //paged.VideoPages.Add(page);





                // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                page.GetComponent<videoFormView>().VideoNumber = paged.NumberOfPages;

                /////////////////
                ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
                /////////////////
                ///// DESCOMENTAR
                page.GetComponent<videoFormView>().TextoCaratulaDelVideo.text = PagedVideoHandler.Instance.videoData[i].titleDescText; //variable_titulo_texto_bloque.count; // Misma variable arriba y abajo
                page.GetComponent<videoFormView>().TituloTextoBloque = PagedVideoHandler.Instance.videoData[i].titleDescText; // Misma variable arriba
                page.GetComponent<videoFormView>().TextoDelBloque = PagedVideoHandler.Instance.videoData[i].descText; //variable_descripcion_json.count;
                page.GetComponent<videoFormView>().UlrPath = PagedVideoHandler.Instance.videoData[i].videoURL; //variable_de_url_del_json.count;

                //print("URL DE VIDEOS: " + videoData[i].videoURL);
                page.GetComponent<videoFormView>().VideoTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

                //page.GetComponent<videoFormView>().TagList = videoData[i].tags; //variable_de_tags_para_ese_video.count;
                page.GetComponent<videoFormView>().TextoDireccion = PagedVideoHandler.Instance.videoData[i].textDireccion; //variable_direccion_json.count;
                page.GetComponent<videoFormView>().TextoGuion = PagedVideoHandler.Instance.videoData[i].textGuion; // variable_guion_json.count;
                page.GetComponent<videoFormView>().TextoProduccion = PagedVideoHandler.Instance.videoData[i].textProduccion; //variable_produccion_json.count;
                page.GetComponent<videoFormView>().MiniBG.sprite = PagedVideoHandler.Instance.videoData[i].miniBGSprite; //variable_minibg_json.count;
                page.GetComponent<videoFormView>().FullBG = PagedVideoHandler.Instance.videoData[i].fullBGSprite; //variable_fullbg_json.count;


            }
        }


        paged.UpdatePages();



        //selectorVideo = this.gameObject.GetComponent<SelectorVideoHelper>();

        // Sacar Init() por el listener de onvaluedchanged de paged - solo la primera vez
        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SetNumber());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.DisableAllColor());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SelectorCreation());

        foreach (var itemPage in paged.Pages)
        {
            var pageVideoCell = itemPage.GetComponent<videoFormView>();


            itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => SelectorVideoHelper.Instance.goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));
        }

        PagedVideoHandler.Instance.SetInPlayer();

        yield return null;
        //

    }


    IEnumerator ArteRoutine()
    {

        PagedRect paged = this.gameObject.GetComponent<PagedRect>();
        paged.RemoveAllPages();
        paged.UpdatePages();
        paged.UpdatePagination();

        foreach (var itemPage in paged.Pages)
        {
            itemPage.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        }


        List<String> tempTags;

        ////

        for (int i = 0; i < PagedVideoHandler.Instance.videoData.Count; i++)
        {
            tempTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

            if (tempTags.Contains("Arte"))
            {


                var page = paged.AddPageUsingTemplate();
                page.PageTitle = "Number" + paged.NumberOfPages;


                // Update Pagination to update title and fields
                paged.UpdatePagination();


                // Agregar paginas a lista en el reproductor
                //paged.VideoPages.Add(page);





                // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                page.GetComponent<videoFormView>().VideoNumber = paged.NumberOfPages;

                /////////////////
                ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
                /////////////////
                ///// DESCOMENTAR
                page.GetComponent<videoFormView>().TextoCaratulaDelVideo.text = PagedVideoHandler.Instance.videoData[i].titleDescText; //variable_titulo_texto_bloque.count; // Misma variable arriba y abajo
                page.GetComponent<videoFormView>().TituloTextoBloque = PagedVideoHandler.Instance.videoData[i].titleDescText; // Misma variable arriba
                page.GetComponent<videoFormView>().TextoDelBloque = PagedVideoHandler.Instance.videoData[i].descText; //variable_descripcion_json.count;
                page.GetComponent<videoFormView>().UlrPath = PagedVideoHandler.Instance.videoData[i].videoURL; //variable_de_url_del_json.count;

                //print("URL DE VIDEOS: " + videoData[i].videoURL);
                page.GetComponent<videoFormView>().VideoTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

                //page.GetComponent<videoFormView>().TagList = videoData[i].tags; //variable_de_tags_para_ese_video.count;
                page.GetComponent<videoFormView>().TextoDireccion = PagedVideoHandler.Instance.videoData[i].textDireccion; //variable_direccion_json.count;
                page.GetComponent<videoFormView>().TextoGuion = PagedVideoHandler.Instance.videoData[i].textGuion; // variable_guion_json.count;
                page.GetComponent<videoFormView>().TextoProduccion = PagedVideoHandler.Instance.videoData[i].textProduccion; //variable_produccion_json.count;
                page.GetComponent<videoFormView>().MiniBG.sprite = PagedVideoHandler.Instance.videoData[i].miniBGSprite; //variable_minibg_json.count;
                page.GetComponent<videoFormView>().FullBG = PagedVideoHandler.Instance.videoData[i].fullBGSprite; //variable_fullbg_json.count;


            }
        }


        paged.UpdatePages();



        //selectorVideo = this.gameObject.GetComponent<SelectorVideoHelper>();

        // Sacar Init() por el listener de onvaluedchanged de paged - solo la primera vez
        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SetNumber());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.DisableAllColor());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SelectorCreation());

        foreach (var itemPage in paged.Pages)
        {
            var pageVideoCell = itemPage.GetComponent<videoFormView>();


            itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => SelectorVideoHelper.Instance.goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));
        }

        PagedVideoHandler.Instance.SetInPlayer();

        yield return null;
        //

    }


    IEnumerator TodosRoutine()
    {
        PagedRect paged = this.gameObject.GetComponent<PagedRect>();
        paged.RemoveAllPages();
        paged.UpdatePages();
        paged.UpdatePagination();

        foreach (var itemPage in paged.Pages)
        {
            itemPage.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        }


        ////

        for (int i = 0; i < PagedVideoHandler.Instance.videoData.Count; i++)
        {
            var page = paged.AddPageUsingTemplate();
            page.PageTitle = "Number" + paged.NumberOfPages;


            // Update Pagination to update title and fields
            paged.UpdatePagination();


            // Agregar paginas a lista en el reproductor
            //paged.VideoPages.Add(page);





            // Toda la data de cada ficha de video que se carga por medio del CMS Handler

            page.GetComponent<videoFormView>().VideoNumber = paged.NumberOfPages;

            /////////////////
            ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
            /////////////////
            ///// DESCOMENTAR
            page.GetComponent<videoFormView>().TextoCaratulaDelVideo.text = PagedVideoHandler.Instance.videoData[i].titleDescText; //variable_titulo_texto_bloque.count; // Misma variable arriba y abajo
            page.GetComponent<videoFormView>().TituloTextoBloque = PagedVideoHandler.Instance.videoData[i].titleDescText; // Misma variable arriba
            page.GetComponent<videoFormView>().TextoDelBloque = PagedVideoHandler.Instance.videoData[i].descText; //variable_descripcion_json.count;
            page.GetComponent<videoFormView>().UlrPath = PagedVideoHandler.Instance.videoData[i].videoURL; //variable_de_url_del_json.count;

            //print("URL DE VIDEOS: " + videoData[i].videoURL);
            page.GetComponent<videoFormView>().VideoTags = PagedVideoHandler.Instance.videoData[i].tags.Split(',').ToList();

            //page.GetComponent<videoFormView>().TagList = videoData[i].tags; //variable_de_tags_para_ese_video.count;
            page.GetComponent<videoFormView>().TextoDireccion = PagedVideoHandler.Instance.videoData[i].textDireccion; //variable_direccion_json.count;
            page.GetComponent<videoFormView>().TextoGuion = PagedVideoHandler.Instance.videoData[i].textGuion; // variable_guion_json.count;
            page.GetComponent<videoFormView>().TextoProduccion = PagedVideoHandler.Instance.videoData[i].textProduccion; //variable_produccion_json.count;
            page.GetComponent<videoFormView>().MiniBG.sprite = PagedVideoHandler.Instance.videoData[i].miniBGSprite; //variable_minibg_json.count;
            page.GetComponent<videoFormView>().FullBG = PagedVideoHandler.Instance.videoData[i].fullBGSprite; //variable_fullbg_json.count;
        }



        paged.UpdatePages();


        //selectorVideo = this.gameObject.GetComponent<SelectorVideoHelper>();

        // Sacar Init() por el listener de onvaluedchanged de paged - solo la primera vez
        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SetNumber());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.DisableAllColor());

        yield return StartCoroutine(PagedVideoHandler.Instance.selectorVideo.SelectorCreation());

        foreach (var itemPage in paged.Pages)
        {
            var pageVideoCell = itemPage.GetComponent<videoFormView>();


            itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => SelectorVideoHelper.Instance.goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));
        }

        PagedVideoHandler.Instance.SetInPlayer();

        yield return null;
        //
    }


    private void Update()
        {
        // TODO: Detect finishing video
        /*
        if (!myClip.isPlaying && isStarted)
        {

            fullIMGArea.gameObject.SetActive(true);

            videoAndPauseArea.gameObject.SetActive(false);

            playButton.gameObject.SetActive(true);

            isStarted = false;
            
        }
        */
        }



    void btn_Play()
    {

        fullIMGArea.gameObject.SetActive(false);
        videoAndPauseArea.gameObject.SetActive(true);

        playButton.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);

        isVideoPlaying = true;

        isStarted = true;

        isPaused = false;

        if (!initialLoaded)
        {
            StartCoroutine(GetMovieTexture());
        }
        else
        {
            //myClip.Stop();
            //myAudio.Stop();

            // El boton de play reproduce el video luego de la pausa
            //
            myClip.Play();
            myAudio.Play();
            //StartCoroutine(WaitFinish());
            StartCoroutine(FindEnd(callback));

        }

    }


    void btn_Restart()
    {
        //if (myClip != null && myClip.isPlaying)
        if (isPaused)
        {
            // Inhabilita y esconde botones
            playButton.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);

            // Detiene para resetear
            myClip.Stop();
            myAudio.Stop();
            // Resetea el video y el audio
            myClip.Play();
            myAudio.Play();

            isPaused = false;

            StartCoroutine(FindEnd(callback));

        }
    }


    void block_Pause()
    {

        print("pointer down button");


        if (myClip != null && myClip.isPlaying)
        {
            isVideoPlaying = false;


            playButton.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);

            myClip.Pause();
            myAudio.Pause();

            isPaused = true;
        }
        else
        {
            return;
        }


    }

    public void init_State()
    {


        //if (myClip != null && myClip.isPlaying)
        //{
        if(myClip != null)
        { 
            myClip.Stop();
            myAudio.Stop();
        }
            playButton.gameObject.SetActive(true);


            initialLoaded = false;
            fullIMGArea.gameObject.SetActive(true);
            videoAndPauseArea.gameObject.SetActive(false);
            isPaused = false;
        //}
    }
	

    void btn_SoundSwitch()
    {
        if (!soundSwitched)
        {
            //myAudioSource.gameObject.SetActive(false);
            myAudioSource.GetComponent<AudioSource>().enabled = false;
            audioSwitchOFF.gameObject.SetActive(true);
            audioSwitch.gameObject.SetActive(false);

            soundSwitched = !soundSwitched;

        } else
        {
            //myAudioSource.gameObject.SetActive(true);
            myAudioSource.GetComponent<AudioSource>().enabled = true;
            audioSwitchOFF.gameObject.SetActive(false);
            audioSwitch.gameObject.SetActive(true);
            soundSwitched = !soundSwitched;
        }

    }

    IEnumerator GetMovieTexture()
    {
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetMovieTexture(currentUrlPath))
        {

			Debug.Log("Cargando video...: " + currentUrlPath);

            yield return www.Send();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                myClip = DownloadHandlerMovieTexture.GetContent(www);
                videoAndPauseArea.texture = myClip;
                myAudio = GetComponent<AudioSource>();
                myAudio.clip = myClip.audioClip;
                myClip.Play();
                myAudio.Play();
                initialLoaded = true;

                //myClip.loop = true;

                //yield return WaitUntil(myClip.isPlaying){ }
                //yield return new WaitUntil(() => myClip.isPlaying);

                //StartCoroutine(WaitFinish());

                //print("Duracion 1 + :" + myClip.duration);
                StartCoroutine(FindEnd(callback));

            }
        }
    }

    /*
    private IEnumerator WaitFinish()
    {
        if (myClip.isPlaying)
        {
            yield return new WaitForSeconds(myClip.duration);
            myClip.Stop();
            myAudio.Stop();
            playButton.gameObject.SetActive(true);
            init_State();
            print("Duracion 2 + :" + myClip.duration);
        }
    }
    */

    private IEnumerator FindEnd(Action callback)
    {
        while (myClip.isPlaying)
        {
            yield return 0;
        }

        if (!isPaused)
        {
            print("finished play");
            //callback();

            //myClip.Stop();
            //myAudio.Stop();
            //playButton.gameObject.SetActive(true);
            init_State();


            yield break;
        }
    }
}
