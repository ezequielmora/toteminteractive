﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;
using EnhancedScrollerDemos.CellControllerIMG;

namespace EnhancedScrollerDemos.CellControllerInstitucional
{
    /// <summary>
    /// This class sets up the data for the inventory and handles the EnhancedScrollers'
    /// callbacks. It also allows changes to the scrollers through some UI interfaces.
    /// </summary>
    public class CellControllerIMG : MonoBehaviour, IEnhancedScrollerDelegate
    {
        /// <summary>
        /// The list of inventory data
        /// </summary>
        [HideInInspector]
        public SmallList<ImgData> _data;

        /// <summary>
        /// The vertical inventory scroller
        /// </summary>
        /// public EnhancedScroller vScroller;

        /// <summary>
        /// The horizontal inventory scroller
        /// </summary>
        public EnhancedScroller hScroller;
        //public EnhancedScroller vScroller;

        /// <summary>
        /// The cell view prefab for the vertical scroller
        /// </summary>
        /// public EnhancedScrollerCellView vCellViewPrefab;

        /// <summary>
        /// The cell view prefab for the horizontal scroller
        /// </summary>
        public EnhancedScrollerCellView hCellViewPrefab;
        //public EnhancedScrollerCellView vCellViewPrefab;



        //public EnhancedScroller.TweenType hScrollerTweenType = EnhancedScroller.TweenType.easeOutCubic;
        //public float hScrollerTweenTime = 1f;

        //public EnhancedScroller.TweenType vScrollerTweenType = EnhancedScroller.TweenType.easeOutCubic;
        //public float vScrollerTweenTime = 1f;

        /// <summary>
        /// The image that shows which item is selected
        /// </summary>
        /// SELECTION UI HELPERS
        //public Image selectedImage;
        //public Text selectedImageText;

        /// <summary>
        /// The base path to the resources folder where the inventory
        /// item sprites are located
        /// </summary>
        public string imgResourcePath;

 
        void Awake()
        {


            // turn on the mask and loop functionality for each scroller based
            // on the UI settings of this controller

            /* ST
            var maskToggle = GameObject.Find("Mask Toggle").GetComponent<Toggle>();
            MaskToggle_OnValueChanged(maskToggle.isOn);

            var loopToggle = GameObject.Find("Loop Toggle").GetComponent<Toggle>();
            LoopToggle_OnValueChanged(loopToggle.isOn);
            */

            CellViewSelected(null);
        }

        void Start()
        {
            // set up the delegates for each scroller

            //vScroller.Delegate = this;
            hScroller.Delegate = this;
            //vScroller.Delegate = this;

            // reload the data
            Reload();
        }

        /// <summary>
        /// This function sets up our inventory data and tells the scrollers to reload
        /// </summary>
        private void Reload()
        {
            // if the data existed previously, loop through
            // and remove the selection change handlers before
            // clearing out the data.
            if (_data != null)
            {
                for (var i = 0; i < _data.Count; i++)
                {
                    _data[i].selectedChanged = null;
                }
            }

            // set up a new inventory list
            _data = new SmallList<ImgData>();


            // TODO// Hacer for con contenedor de todos los json que luego asigna segun cantidad de fichas

            // First Form
            Color newColor1a = new Color();
            ColorUtility.TryParseHtmlString("#F55126FF", out newColor1a);

            Color newColor1b = new Color();
            ColorUtility.TryParseHtmlString("#C9D9D9FF", out newColor1b);

            Color newColor1c = new Color();
            ColorUtility.TryParseHtmlString("#549279FF", out newColor1c);



            

            // Second Form
            Color newColor2a = new Color();
            ColorUtility.TryParseHtmlString("#3a7059FF", out newColor2a);

            Color newColor2b = new Color();
            ColorUtility.TryParseHtmlString("#2c676bFF", out newColor2b);


            // Third Form
            Color newColor3a = new Color();
            ColorUtility.TryParseHtmlString("#ffcf01FF", out newColor3a);

            Color newColor3b = new Color();
            ColorUtility.TryParseHtmlString("#5eb6b8FF", out newColor3b);


            // Text Colors
            // Grey
            Color textColorA = new Color();
            ColorUtility.TryParseHtmlString("#4F4A4AFF", out textColorA);
            // White
            Color textColorB = new Color();
            ColorUtility.TryParseHtmlString("#FFFFFFFF", out textColorB);

            // add inventory items to the list
            /*_data.Add(new ImgData() {
                titleName = "Ficha Especie Numero 1",
                subTitleName = "Nombre cientifico especie 1",
                mainDescription = "Esta es una especie unica en el mundo...",
                fullDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet volutpat augue, ut sollicitudin lorem. Fusce ac ullamcorper orci. Vestibulum faucibus at odio suscipit scelerisque. Ut pellentesque eros diam, at iaculis tellus ultricies at. Aliquam facilisis condimentum sollicitudin. Pellentesque nec tortor a ligula pretium facilisis et at erat. Donec efficitur blandit dolor, eu volutpat nulla. Maecenas condimentum, dui at efficitur dapibus, tortor ipsum porta lacus, et suscipit nulla felis vel felis. Pellentesque efficitur elit vitae nisl tincidunt tempor. Cras efficitur, lectus eget posuere vulputate, nibh felis gravida ipsum, nec aliquet felis elit ut elit. Ut gravida dui quis tortor mattis, id posuere metus varius. Mauris rhoncus arcu eu elit lacinia eleifend. In sem neque, rutrum a lectus quis, mollis aliquet mauris. Morbi ac ex ornare nibh interdum consectetur.In ornare velit non diam venenatis mattis.Etiam pellentesque dictum nunc,id pretium quam.Fusce mauris turpis,laoreet nec neque non,sagittis sodales lectus.Ut volutpat ex cursus risus finibus,sit amet ornare nisi gravida.Phasellus congue neque ut condimentum accumsan.",
                imgPath = imgResourcePath + "/ANFIB_BG1",
                titleColor = newColor1a,
                mainColor = newColor1b,
                mainTextColor = textColorA,
                fullTextColor = textColorA,

                subDescription_1 = "Aditional info number 1",
                subDescriptionTextColor_1 = newColor1b,
                subColor_1 = newColor1c,

                subDescription_2 = "Aditional info number 2",
                subDescriptionTextColor_2 = newColor1c,
                subColor_2 = newColor1a,

                subDescription_3 = "Aditional info number 3",
                subDescriptionTextColor_3 = newColor1a,
                subColor_3 = newColor2b,



            });

            _data.Add(new ImgData() {
                titleName = "Ficha Especie Numero 2",
                subTitleName = "Nombre cientifico especie 2",
                mainDescription = "Esta es una especie diferentes a las demas por su tipo...",
                fullDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet volutpat augue, ut sollicitudin lorem. Fusce ac ullamcorper orci. Vestibulum faucibus at odio suscipit scelerisque. Ut pellentesque eros diam, at iaculis tellus ultricies at. Aliquam facilisis condimentum sollicitudin. Pellentesque nec tortor a ligula pretium facilisis et at erat. Donec efficitur blandit dolor, eu volutpat nulla. Maecenas condimentum, dui at efficitur dapibus, tortor ipsum porta lacus, et suscipit nulla felis vel felis. Pellentesque efficitur elit vitae nisl tincidunt tempor. Cras efficitur, lectus eget posuere vulputate, nibh felis gravida ipsum, nec aliquet felis elit ut elit. Ut gravida dui quis tortor mattis, id posuere metus varius. Mauris rhoncus arcu eu elit lacinia eleifend. In sem neque, rutrum a lectus quis, mollis aliquet mauris. Morbi ac ex ornare nibh interdum consectetur.In ornare velit non diam venenatis mattis.Etiam pellentesque dictum nunc,id pretium quam.Fusce mauris turpis,laoreet nec neque non,sagittis sodales lectus.Ut volutpat ex cursus risus finibus,sit amet ornare nisi gravida.Phasellus congue neque ut condimentum accumsan.",
                imgPath = imgResourcePath + "/ANFIB_BG2",
                titleColor = newColor2a,
                mainColor = newColor2b,
                mainTextColor = textColorB,
                fullTextColor = textColorB,

                subDescription_1 = "Aditional info number 1",
                subDescriptionTextColor_1 = newColor1b,
                subColor_1 = newColor1c,

                subDescription_2 = "Aditional info number 2",
                subDescriptionTextColor_2 = newColor1c,
                subColor_2 = newColor1a,

                subDescription_3 = "Aditional info number 3",
                subDescriptionTextColor_3 = newColor1a,
                subColor_3 = newColor2b,



            });
            */


            // tell the scrollers to reload
            hScroller.ReloadData();
            //vScroller.ReloadData();
        }

        /// <summary>
        /// This function handles the cell view's button click event
        /// </summary>
        /// <param name="cellView">The cell view that had the button clicked</param>
        private void CellViewSelected(EnhancedScrollerCellView cellView)
        {
            if (cellView == null)
            {
                // nothing was selected
                //selectedImage.gameObject.SetActive(false);
                //selectedImageText.text = "None";
            }
            else
            {
                // get the selected data index of the cell view
                var selectedDataIndex = (cellView as CellViewInstitucional).DataIndex;

                // loop through each item in the data list and turn
                // on or off the selection state. This is done so that
                // any previous selection states are removed and new
                // ones are added.
                for (var i = 0; i < _data.Count; i++)
                {
                    _data[i].Selected = (selectedDataIndex == i);
                }

                //selectedImage.gameObject.SetActive(true);
                //selectedImage.sprite = Resources.Load<Sprite>(_data[selectedDataIndex].spritePath + "_v");

                //selectedImageText.text = _data[selectedDataIndex].titleName;
            }
        }

        #region Controller UI Handlers

        /// <summary>
        /// This handles the toggle for the masks
        /// </summary>
        /// <param name="val">Is the mask on?</param>
        public void MaskToggle_OnValueChanged(bool val)
        {
            // set the mask component of each scroller
           // vScroller.GetComponent<Mask>().enabled = val;
            hScroller.GetComponent<Mask>().enabled = val;
        }

        /// <summary>
        /// This handles the toggle fof the looping
        /// </summary>
        /// <param name="val">Is the looping on?</param>

        /*
        public void LoopToggle_OnValueChanged(bool val)
        {
            // set the loop property of each scroller
            vScroller.Loop = val;
            hScroller.Loop = val;
        }
        */
        #endregion

        #region EnhancedScroller Callbacks

        /// <summary>
        /// This callback tells the scroller how many inventory items to expect
        /// </summary>
        /// <param name="scroller">The scroller requesting the number of cells</param>
        /// <returns>The number of cells</returns>
        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _data.Count;
        }

        /// <summary>
        /// This callback tells the scroller what size each cell is.
        /// </summary>
        /// <param name="scroller">The scroller requesting the cell size</param>
        /// <param name="dataIndex">The index of the data list</param>
        /// <returns>The size of the cell (Height for vertical scrollers, Width for Horizontal scrollers)</returns>
        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            /*
            if (scroller == vScroller)
            {
                // return a static height for all vertical scroller cells
                return 320f;
            }
            else
            {
            */
                // return a static width for all horizontal scroller cells
                return 260f;
            /*
            }
            */
        }


        /// <summary>
        /// This callback gets the cell to be displayed by the scroller
        /// </summary>
        /// <param name="scroller">The scroller requesting the cell</param>
        /// <param name="dataIndex">The index of the data list</param>
        /// <param name="cellIndex">The cell index (This will be different from dataindex if looping is involved)</param>
        /// <returns>The cell to display</returns>
        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            // first get a cell from the scroller. The scroller will recycle if it can.
            // We want a vertical cell prefab for the vertical scroller and a horizontal
            // prefab for the horizontal scroller.
            //InventoryCellView cellView = scroller.GetCellView(scroller == vScroller ? vCellViewPrefab : hCellViewPrefab) as CellViewInstitucional;
            CellViewIMG cellView = scroller.GetCellView(hCellViewPrefab) as CellViewIMG;

            // set the name of the cell. This just makes it easier to see in our
            // hierarchy what the cell is
            //cellView.name = (scroller == vScroller ? "Vertical" : "Horizontal") + " " + _data[dataIndex].titleName;
			cellView.name = ("Horizontal") + " " + _data[dataIndex].nameText;

            // set the selected callback to the CellViewSelected function of this controller. 
            // this will be fired when the cell's button is clicked
            cellView.selected = CellViewSelected;

            // set the data for the cell
            //cellView.SetData(dataIndex, _data[dataIndex], (scroller == vScroller));
            cellView.SetData(dataIndex, _data[dataIndex], false);

            // return the cell view to the scroller
            return cellView;
        }

        #endregion
    }
}