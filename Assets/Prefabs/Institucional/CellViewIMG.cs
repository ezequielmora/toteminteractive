﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EnhancedUI.EnhancedScroller;
using EnhancedScrollerDemos.CellControllerIMG;
using UI.Pagination;
using System.Collections;

namespace EnhancedScrollerDemos.CellControllerInstitucional
{
    /// <summary>
    /// This delegate handles the UI's button click
    /// </summary>
    /// <param name="cellView">The cell view that had the button click</param>
    public delegate void SelectedDelegateIMG(EnhancedScrollerCellView cellView);

    /// <summary>
    /// This class handles the presentation of the inventory cell view. Both the 
    /// horizontal and vertical cell views share the same view class. The difference
    /// between them is layout and sprite for this example.
    /// </summary>
    public class CellViewIMG : EnhancedScrollerCellView
    {
        /// <summary>
        /// Reference to the underlying data driving this view
        /// </summary>
        private ImgData _data;

        /// <summary>
        /// These are the UI elements that will be updated when the data changes
        /// </summary>
        /// 

        [SerializeField]
        [Header("Pagina Padre con Paged")]
        public PagedRect parentPaged;

        [SerializeField]
		[Header("Numero de Ficha o Pagina")]
        private int pageNumber;
        public int PageNumber
        {
            get
            {
                return pageNumber;
            }
            set
            {
                pageNumber = value;
            }
        }
		
        [Header("Componentes que se definiran en las diferentes fichas de Imagen de Flora / Fauna / Anfibio")]
        public Image imageBackground;
        [Header("Habilita / Deshabilita el boton si la informacion excede el contenedor principal")]
        public bool enableInfoButton;
        [Header("Contenedor del boton plus")]
        public Image moreInfoContainer;
        [Header("Boton Plus")]
        public Button plusButton;
        [Header("Boton Minus")]
        public Button minusButton;
        [Header("Contenedor del campo titulo Horizontal")]
        public TextMeshProUGUI hTextTitle;
        [Header("Contenedor del campo subtitulo Horizontal")]
        public TextMeshProUGUI hSubTitle;
        [Header("Contenedor del campo texto principal")]
        public TextMeshProUGUI mainText;
        [Header("Contenedor para texto expandido del formulario")]
        public TextMeshProUGUI fullText;
        [Header("Colores para contenedor Expandido")]
        public Image moreInfoPanel;
        [Header("Contenedor para bloque de color del titulo u subtitulo")]
        public Image titleColor;
        [Header("Colores para contenedor comprimido")]
        public Image mainColorField;
        [Header("Colores de sub-bloque inferior 1")]
        public Image subColorField_1;
        [Header("Contenedor para texto del sub-bloque inferior 1")]
        public TextMeshProUGUI subText_1;
        [Header("Colores de sub-bloque inferior 2")]
        public Image subColorField_2;
        [Header("Contenedor para texto del sub-bloque inferior 2")]
        public TextMeshProUGUI subText_2;
        [Header("Colores de sub-bloque inferior 3")]
        public Image subColorField_3;
        [Header("Contenedor para texto del sub-bloque inferior 3")]
        public TextMeshProUGUI subText_3;



        // More Additional panel and info
        [Header("Objeto que controla la animación del bloque de texto adicional")]
        public Animator formAdditionalAnimator;


        /// Datos que se veran en la parte del scroll vertical
        /// 
        //public TextMeshProUGUI vMainTitle;
        //public TextMeshProUGUI vSubTitle;


        /// <summary>
        /// These are the colors for the selection of the cells
        /// </summary>
        // Usar para el caso de la ficha global
        //public Color selectedColor;
        //public Color unSelectedColor;

        /// <summary>
        /// Public reference to the index of the data
        /// </summary>
        public int DataIndex { get; private set; }

        /// <summary>
        /// The handler to call when this cell's button traps a click event
        /// </summary>
        public SelectedDelegateInst selected;

        private void Awake()
        {


            //formAdditionalAnimator = this.transform.GetComponent<Animator>();//this.transform.Find("Skin").GetComponent<Animator>();

        }

        /// <summary>
        /// This is called if the cell is destroyed. The EnhancedScroller will
        /// not call this since it uses recycling, but we include it in case 
        /// the user decides to destroy the cell anyway
        /// </summary>
        void OnDestroy()
        {
            if (_data != null)
            {
                // remove the handler from the data so 
                // that any changes to the data won't try
                // to call this destroyed view's function
                _data.selectedChanged -= SelectedChanged;
            }
        }

        /// <summary>
        /// This function sets up the data for the cell view
        /// </summary>
        /// <param name="dataIndex">The index of the data</param>
        /// <param name="data">The data to use</param>
        /// <param name="isVertical">Whether this view is vertical or horizontal (which will determine the sprite to use)</param>
        public void SetData(int dataIndex, ImgData data, bool isVertical)
        {
            // if there was previous data assigned to this cell view,
            // we need to remove the handler for the selection change
            if (_data != null)
            {
                _data.selectedChanged -= SelectedChanged;
            }

            // link the data to the cell view
            DataIndex = dataIndex;
            _data = data;

            // update the cell view's UI
            //textTitle.text = data.titleName;

            if (hTextTitle != null)
				hTextTitle.text = (data.nameText != "" ? data.nameText.ToString() : "-");
            if (hSubTitle != null)
				hSubTitle.text = (data.nameText != "" ? data.nameText.ToString() : "-");
            if (mainText != null)
				mainText.text = (data.textA != "" ? data.textA.ToString() : "-");
            if (fullText != null)
				fullText.text = (data.textA != "" ? data.textA.ToString() : "-");
            if (subText_1 != null)
				subText_1.text = (data.textB != "" ? data.textB.ToString() : "-");
            if (subText_2 != null)
				subText_2.text = (data.textC != "" ? data.textC.ToString() : "-");
            if (subText_3 != null)
				subText_3.text = (data.textD != "" ? data.textD.ToString() : "-");

            // the description is only shown on the vertical cell view
            //if (isVertical)
            //{
            //
            //}
            //    itemDescriptionText.text = data.itemDescription;

            // TODO: HACER HANDLER SEPARADO
            /*
            if (vMainTitle != null)
                vMainTitle.text = (data.titleName != "" ? data.titleName.ToString() : "-");
            if (vSubTitle != null)
                vSubTitle.text = (data.titleName != "" ? data.titleName.ToString() : "-");
            */

            // set up the sprite based on the sprite path and whether the
            // view is horizontal or vertical
            //imageBackground.sprite = Resources.Load<Sprite>(data.spritePath + (isVertical ? "_v" : "_h"));

            // Definicion de imagen en tiempo de ejecucion /// TODO: Descarga de json a streaming assets y asignacion desde /StreamingAssets
            imageBackground.sprite = Resources.Load<Sprite>(data.imgPath);
            // Definicion de Colores de botones y divisiones de las fichas

            if (mainText.text.ToString().Length < 590)
            {
                moreInfoContainer.gameObject.SetActive(false);
            }


            // Color de boton plus
            moreInfoContainer.color = data.titleColor;
            // Color de contenedor comprimido
			mainColorField.color = data.colorA;
            // Color de contenedor expandido
			moreInfoPanel.color = data.colorA;

            // Color de bloque de titulo
            titleColor.color = data.titleColor;



            // Text colors
			mainText.color = data.colorA;
			fullText.color = data.colorB;


            // Aditional Fields sets colors with data model
			subColorField_1.color = data.colorB;
			subColorField_2.color = data.colorC;
			subColorField_3.color = data.colorD;



            

            // set up a handler so that when the data changes
            // the cell view will update accordingly. We only
            // want a single handler for this cell view, so 
            // first we remove any previous handlers before
            // adding the new one
            _data.selectedChanged -= SelectedChanged;
            _data.selectedChanged += SelectedChanged;

            // update the selection state UI
            SelectedChanged(data.Selected);
        }


        public void OnEnable()
        {
            // Add button and listener to plus button
            //moreInfoButton.gameObject.AddComponent<Button>();
            //moreInfoButton.gameObject.GetComponent<Button>().onClick.AddListener(() => btn_ClickPlus());
            //print("added button");
        }


        /// <summary>
        /// This function changes the UI state when the item is 
        /// selected or unselected.
        /// </summary>
        /// <param name="selected">The selection state of the cell</param>
        private void SelectedChanged(bool selected)
        {
            // Selected Functions Removed
            //moreInfoButton.color = (selected ? selectedColor : unSelectedColor);
            //moreInfoPanel.color = (selected ? selectedColor : unSelectedColor);

        }

        /// <summary>
        /// This function is called by the cell's button click event
        /// </summary>
        public void OnSelected()
        {
            // if a handler exists for this cell, then
            // call it.
            if (selected != null) selected(this);
        }


        public void btn_ClickPlus()
        {


            //formAdditionalAnimator.SetTrigger("ChangeOpen");
            formAdditionalAnimator.SetBool("hasOpened", true);

            //moreInfoButton.gameObject.GetComponent<Button>().onClick.RemoveListener(() => btn_ClickPlus());
            //moreInfoButton.gameObject.GetComponent<Button>().onClick.AddListener(() => btn_ClickMinus());
            plusButton.gameObject.SetActive(false);
            minusButton.gameObject.SetActive(true);

            //Invoke("ResetPage", 2f);

            StartCoroutine(ResetPage());



        }


        public void btn_ClickMinus()
        {


            //formAdditionalAnimator.SetTrigger("ChangeClose");
            formAdditionalAnimator.SetBool("hasOpened", false);


            //moreInfoButton.gameObject.GetComponent<Button>().onClick.RemoveListener(() => btn_ClickMinus());
            //moreInfoButton.gameObject.GetComponent<Button>().onClick.AddListener(() => btn_ClickPlus());

            plusButton.gameObject.SetActive(true);
            minusButton.gameObject.SetActive(false);



        }

        /*
        private void OnEnable()
        {
            formAdditionalAnimator.SetTrigger("appear");
            print("Script from forms institucional was enabled");
        }
        */
        /*
        private void Update()
        {
            if (this.gameObject.activeSelf == true)
            {
                formAdditionalAnimator.SetTrigger("appear");
            }
        }
        */

        public void runtime_ClickMinus()
        {



            //formAdditionalAnimator.SetTrigger("ChangeClose");
            formAdditionalAnimator.SetBool("hasOpened", false);


            //moreInfoButton.gameObject.GetComponent<Button>().onClick.RemoveListener(() => btn_ClickMinus());
            //moreInfoButton.gameObject.GetComponent<Button>().onClick.AddListener(() => btn_ClickPlus());

            plusButton.gameObject.SetActive(true);
            minusButton.gameObject.SetActive(false);



        }


        private IEnumerator ResetPage()
        {
            if (parentPaged.GetCurrentPage().PageNumber != pageNumber)
                yield return new WaitForSeconds(2f);

            parentPaged.SetCurrentPage(pageNumber);
        }

        private void OnDisable()
        {
            //formAdditionalAnimator.Rebind();
            //formAdditionalAnimator.SetBool("hasOpened", false);
            //print("on disable it");
        }
    }
}