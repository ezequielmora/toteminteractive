﻿using EnhancedScrollerDemos.CellControllerInstitucional;
using EnhancedUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaleriaController : MonoBehaviour {

    public Animator galleryAnimator;

    public CellViewInstitucional[] animatorGroup;

    //public SmallList<InstitucionalData> fichaAnimator;

    // Use this for initialization
    void Start () {

        //Animator galleryAnimator = this.gameObject.GetComponentInChildren<Animator>();
        //galleryAnimator.SetTrigger("appear");

        //galleryAnimator.SetBool("stay", true);




        galleryAnimator.gameObject.SetActive(false);


        animatorGroup = GameObject.FindObjectsOfType(typeof(CellViewInstitucional)) as CellViewInstitucional[];

        
        /*
        fichaAnimator = CellControllerInstitucional.instance._data;

        for (int i = 0; i < fichaAnimator.Count; i++)
        {

            //fichaAnimator[0].

        }
        */

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Appear()
    {

        //Animator galleryAnimator = this.gameObject.GetComponentInChildren<Animator>();
        galleryAnimator.gameObject.SetActive(true);
        //galleryAnimator.SetBool("stay", false);

        //galleryAnimator.SetBool("stay", false);

        StartCoroutine(Animation());

    }

    public IEnumerator Animation()
    {
        galleryAnimator.SetTrigger("appear");
        //galleryAnimator.SetBool("stay", true);
        //fichaAnimator.SetTrigger("appear");

        /*
        for (int i = 0; i < animatorGroup.Length; i++)
        {
            animatorGroup[i].GetComponent<Animator>().SetTrigger("appear");
        }
        */
        yield return null;
    }
}
