﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SQL.Structs;


public class VideoData
{

	public int id;
	public string hash;
	public int miniBGId;
	public Sprite miniBGSprite;
	public int fullBGId; 
	public Sprite fullBGSprite;
	public int videoId;
	public string videoURL;
    //public List<string> tags = new List<string>();
    public string tags;
	public string titleDescText;
	public string descText;
	public string textDireccion;
	public string textGuion;
	public string textProduccion;

	public VideoData () {
	}

	public VideoData(SQLVideoData videoData) {
		id = videoData.id;
		hash = videoData.hash;
		miniBGId = videoData.miniBGId;
		fullBGId = videoData.fullBGId; 
		videoId = videoData.videoId;
		videoURL = string.Empty;
        //tags.Add (videoData.tag);
        tags = videoData.tag;
		titleDescText = videoData.titleDescText;
		descText = videoData.descText;
		textDireccion = videoData.textDireccion;
		textGuion = videoData.textGuion;
		textProduccion = videoData.textProduccion;
	}

}

