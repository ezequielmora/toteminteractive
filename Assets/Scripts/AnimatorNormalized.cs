﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorNormalized : MonoBehaviour {

	// Use this for initialization
	void Start () {

        // Setea tiempo de inicio aleatorio para las aimaciones de ese animator
        //this.gameObject.GetComponent<Animator>().ForceStateNormalizedTime(Random.Range(0.0f, 1.0f));
        // Animator.Play(state, layer, normalizedTime);
        this.gameObject.GetComponent<Animator>().Play("Wiggle", 0, Random.Range(0.0f, 1.0f));

    }

    // Update is called once per frame
    void Update () {
		
	}
}
