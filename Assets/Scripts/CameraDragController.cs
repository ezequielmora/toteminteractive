﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript;
using EnhancedScrollerDemos.CellControllerInstitucional;
using TouchScript.InputSources;

public class CameraDragController : MonoBehaviour
{

    //ST [Header("Objeto de Loading UI Secundario")]
    //ST public GameObject canvasLoading;


    // Singleton
    public static CameraDragController instance;


    private Vector2 initialMousePosition = Vector2.zero;
    private Vector2 initialCameraPosition = Vector2.zero;
    private float initialTime = 0;


    private Vector2 targetDragPosition = Vector2.zero;
    private Vector2 initialDragPosition = Vector2.zero;
    private float dragTotalTime = 0;

    [SerializeField]
    private float dragProportion = 4F;

    [HideInInspector]
    public bool doDrag = false;

    [HideInInspector]
    public bool doSnap = false;

    [HideInInspector]
    public bool allowFollow = true;

    // tiempo que tarda en moverse desde el punto de inicio hasta el punto de llegada
    [HideInInspector]
    float timeTakenDuringLerp = 1f;
    // Variable donde guardaremos el tiempo desde que comenzo el lerp
    [HideInInspector]
    private float _timeStartedLerping;


    // Tipo que referencia a los elementos de UI para get / set de sus propiedades
    [SerializeField]
    RectTransform canvasTransform;

    // Variable bool para cursor hide
    [Header("Condicional para determinar si mostrar u ocultar el cursor en la app")]
    public bool cursorVisible = true;

    [Header("TouchScript Manager para habilitar o deshabilitar TUIO")]
    public TuioInput tuioSupport;

    // Definicion de los limites o bounds de la camara en base tamaño y posicion relativa del canvas global
    private float leftBound;
    private float rightBound;
    private float bottonBound;
    private float topBound;


    // Relacion de area sobre la que se hará el snapping
    [SerializeField]
    private int snapAreaRatio = 4;



    [SerializeField]
    RectTransform[] snapTransform;
    private float[] snapXAreaMin;
    private float[] snapXAreaMax;
    private float[] snapYAreaMin;
    private float[] snapYAreaMax;


    private GameObject[] objectCell;


    private void OnEnable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan += touchesBeganHandler;
            TouchManager.Instance.TouchesMoved += touchesMovedHandler;
            TouchManager.Instance.TouchesEnded += touchesEndedHandler;
        }
    }

    private void OnDisable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan -= touchesBeganHandler;
            TouchManager.Instance.TouchesMoved -= touchesMovedHandler;
            TouchManager.Instance.TouchesEnded -= touchesEndedHandler;

        }
    }


    private void touchesBeganHandler(object sender, TouchEventArgs e)
    {

        // Restringir la deteccion a un unico toque
        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched down at " + e.Touches[0].Position);

        initialMousePosition = e.Touches[0].Position;//Input.mousePosition;

        /*if (e.Touches.Count == 1)
        {
            foreach (var item in e.Touches)
            {
                initialMousePosition = item.Position;
            }

        }
        */
        initialCameraPosition = transform.position;
        initialTime = Time.time;
        doDrag = false;
        doSnap = false;


        /*
        foreach (var point in e.Touches)
        {

            Debug.Log(point.Tags + " touched down at " + point.Position);


            initialMousePosition = point.Position;//Input.mousePosition;
                                                        //initialMousePosition = touchVar; ;//Input.mousePosition;
            initialCameraPosition = transform.position;
            initialTime = Time.time;
            doDrag = false;
            doSnap = false;
        }
        */

    }


    private void touchesMovedHandler(object sender, TouchEventArgs e)
    {

        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched hold at " + e.Touches[0].Position);

        //if (e.Touches.Count == 1)
        //{
        //    foreach (var item in e.Touches)
        //    {

                if (allowFollow)
                {
                    // Posicion real del mouse sobre todo el canvas, resto para obtener posicion real de la camara, multiplca por -1 para movimiento inverso del drag
                    Vector2 _prePos = ((Vector2)e.Touches[0].Position - (initialMousePosition + initialCameraPosition)) * -1; //((Vector2)Input.mousePosition - (initialMousePosition + initialCameraPosition)) * -1;


                    // Definicion de bound limit en forma preventiva, no reactiva
                    transform.position = new Vector3(
                      Mathf.Clamp(Mathf.Round(_prePos.x), leftBound, rightBound),

                      Mathf.Clamp(Mathf.Round(_prePos.y), bottonBound, topBound),

                      -10);


                }
        //    }

    }
    

        

        /*
        foreach (var point in e.Touches)
        {
            Debug.Log(point.Tags + " touched hold at " + point.Position);


            if (allowFollow)
            {
                // Posicion real del mouse sobre todo el canvas, resto para obtener posicion real de la camara, multiplca por -1 para movimiento inverso del drag
                Vector2 _prePos = ((Vector2)point.Position - (initialMousePosition + initialCameraPosition)) * -1; //((Vector2)Input.mousePosition - (initialMousePosition + initialCameraPosition)) * -1;


                // Definicion de bound limit en forma preventiva, no reactiva
                transform.position = new Vector3(
                  Mathf.Clamp(Mathf.Round(_prePos.x), leftBound, rightBound),

                  Mathf.Clamp(Mathf.Round(_prePos.y), bottonBound, topBound),

                  -10);


            }

            return;

        }
        */

    


    private void touchesEndedHandler(object sender, TouchEventArgs e)
    {

        // Restringir la deteccion a un unico toque
        //Debug.Log(e.Touches[0]);
        //Debug.Log(e.Touches[0].Tags + " touched release at " + e.Touches[0].Position);

        StopFollow();


        /*
        foreach (var point in e.Touches)
        {
            Debug.Log(point.Tags + " touched release at " + point.Position);

            StopFollow();

        }
        */

    }

    // SINGLETON
    void Awake()
    {
        //Chequea si la instancia del singleton ya existe
        if (instance == null)

            //si no existe, setea la instacia a esta clase
            instance = this;

        //si la instancia ya existe pero no esta, la destruye
        else if (instance != this)

            //destruya esa instancia reforzando el singleton, y asegurando que sea la unica clase existente en todo momento
            Destroy(gameObject);

        //Setea que esta instancia no se destruya por cambios de escenas
        DontDestroyOnLoad(gameObject);


    }
    
    IEnumerator Init()
    {

        allowFollow = false;

        // Espera la carga de datos antes de ocultas las fichas
        if (ContentHandler.Instance != null)
        {
            yield return new WaitUntil(() => ContentHandler.Instance.isLoaded == true);

            //yield return new WaitUntil(() => SelectorVideoHelper.Instance.isLoaded == true);

        }
        // Setea elementos de la lista en falso luego de cargar las cells views
        // ST

        yield return new WaitForSeconds(30f);

        //yield return StartCoroutine(SelectorVideoHelper.Instance.DisableAllColor());

        yield return StartCoroutine(SelectorVideoHelper.Instance.SelectorCreation());



        //yield return StartCoroutine(SelectorGlobalHelper.Instance.DisableAllColor());

        yield return StartCoroutine(SelectorGlobalHelper.Instance.SelectorCreation());

        //ST if (canvasLoading != null)
        //ST     canvasLoading.SetActive(false);

        allowFollow = true;
    }
    

    // Use this for initialization
    void Start()
    {
        // Define si muestra cursor y log dependiendo de build o editor
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
        // Oculta el cursor
        Cursor.visible = cursorVisible;
#else 
        Debug.unityLogger.logEnabled = false;
        Cursor.visible = false;
#endif


        StartCoroutine(Init());

        


        // Asignacion de los valores relativos para los limites de la camara sobres los que se realizara el snapping
        leftBound = 0 - canvasTransform.sizeDelta.x / 2 + Camera.main.pixelWidth / 2;
        rightBound = canvasTransform.sizeDelta.x / 2 - Camera.main.pixelWidth / 2;
        bottonBound = 0 - canvasTransform.sizeDelta.y / 2 + Camera.main.pixelHeight / 2;
        topBound = canvasTransform.sizeDelta.y / 2 - Camera.main.pixelHeight / 2;

        // Asignacion de las areas de pantalla sobre las que se realizará el snapping de la camara para el area A, B, C...etc
        // NO PONER NEW PORQUE BORRA REFERENCIAS DESDE EDITOR
        //snapTransform = new RectTransform[2];
        snapXAreaMin = new float[3] { 0, 0, 0 };
        snapXAreaMax = new float[3] { 0, 0, 0 };
        snapYAreaMin = new float[3] { 0, 0, 0 };
        snapYAreaMax = new float[3] { 0, 0, 0 };





        for (int i = 0; i < snapTransform.Length; i++)
        {


            snapXAreaMin[i] = snapTransform[i].transform.position.x - snapTransform[i].sizeDelta.x / snapAreaRatio;
            snapXAreaMax[i] = snapTransform[i].transform.position.x + snapTransform[i].sizeDelta.x / snapAreaRatio;
            snapYAreaMin[i] = snapTransform[i].transform.position.y - snapTransform[i].sizeDelta.y / snapAreaRatio;
            snapYAreaMax[i] = snapTransform[i].transform.position.y + snapTransform[i].sizeDelta.y / snapAreaRatio;


        }

    }

    // Update is called once per frame
    void Update()
    {

        _timeStartedLerping += Time.deltaTime;
        if (_timeStartedLerping > timeTakenDuringLerp)
        {
            _timeStartedLerping = timeTakenDuringLerp;
        }



        if (doDrag)
        {
            Drag();
        }

        if (doSnap)
        {

            StartCoroutine(Snap());

        }
        /*
        if (GameObject.FindGameObjectsWithTag("fichasInst") != null && allowFollow == true)
        {
            objectCell = GameObject.FindGameObjectsWithTag("fichasInst");
            foreach (var item in objectCell)
            {
                item.GetComponent<CellViewInstitucional>().btn_ClickMinus();
            }
        }

        */
    }


    private void StopFollow()
    {
        initialDragPosition = transform.position;
        dragTotalTime = Time.time - initialTime;

        float xDistance = (transform.position.x - initialCameraPosition.x) / 2;
        float yDistance = (transform.position.y - initialCameraPosition.y) / 2;



        Vector2 targetPosition = (Vector2)transform.position + new Vector2(xDistance, yDistance);

        targetDragPosition = targetPosition;
        doDrag = true;

        // Solo se puede ejecutar el snapping en otros objetos si el mouse esta soltado
        doSnap = true;

    }

    // Funcion que controla el drag una vez que se cumpla la condicion bool doDrag
    private void Drag()
    {
        //MATI// transform.position = new Vector3(_preDragPosition.x, _preDragPosition.y, -10);


        Vector2 _preDragPosition = Vector2.Lerp(transform.position, targetDragPosition, Time.deltaTime * dragProportion);

        transform.position = new Vector3(
          Mathf.Clamp(Mathf.Round(_preDragPosition.x), leftBound, rightBound),

          Mathf.Clamp(Mathf.Round(_preDragPosition.y), bottonBound, topBound),

          -10);

    }

    //Funcion que controla snap en puntos de canvas
    IEnumerator Snap()
    {
        for (int i = 0; i < snapTransform.Length; i++)
        {

            // Checkeo que el centro de la camara este dentro del Area A,B,C,etc , si es asi ejecutar el drag
            if (transform.position.x > snapXAreaMin[i] && transform.position.x < snapXAreaMax[i] && transform.position.y > snapYAreaMin[i] && transform.position.y < snapYAreaMax[i])
            {

                // Si es posible ejecutar el snapping previene de no hacer drag
                doDrag = false;


                //float timeSinceStarted = Time.time - _timeStartedLerping;
                float percentageComplete = _timeStartedLerping / timeTakenDuringLerp;


                /*
                Vector2 _preSnapPosition = Vector2.Lerp(transform.position, snapTransform[i].transform.position, Time.deltaTime * 6);

                transform.position = new Vector3(

                    Mathf.CeilToInt
                    (_preSnapPosition.x),
                    Mathf.CeilToInt
                    (_preSnapPosition.y),
                    -10);
                */

                // Divide el porcentaje para obtener la velocidad a la cual hara el snap - mientras divida por un numero mayor mas lento sera
                Vector2 _preSnapPosition = Vector3.Lerp(transform.position, snapTransform[i].transform.position, percentageComplete / 6);

                transform.position = new Vector3(

                    Mathf.CeilToInt
                    (_preSnapPosition.x),
                    Mathf.CeilToInt
                    (_preSnapPosition.y),
                    -30);

                print("PORCENTAGE: " + percentageComplete);

                yield return null;

            }

        }

    }

    IEnumerator AdjustSnap()
    {

        print("AJUSTE DE AREA");


        yield return StartCoroutine(Snap());

        /*
        for (int i = 0; i < snapTransform.Length; i++)
        {

            transform.position = snapTransform[i].transform.position;

            _timeStartedLerping = 0f;

        }
        */

    }
}
    