﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FichasAnimatorHelper : MonoBehaviour {

    public Animator fichaAnimator;

    private bool activated = false;

	// Use this for initialization
	void Start () {

        fichaAnimator = fichaAnimator.GetComponent<Animator>();

        //fichaAnimator.enabled = false;

        //fichaAnimator.enabled = true;

	}

    // Update is called once per frame
    //void Update () {
    //}

    /*
    public void Appear()
    {
        if (this.gameObject.activeInHierarchy && activated == false)
        {

            fichaAnimator.SetTrigger("appear");

            activated = true;

            print("ANIMACION ACTIVADA");

        }

        if (!this.gameObject.activeInHierarchy)
        {
            activated = false;
        }
    }
    */
    /*
    private void Disappear()
    {
        activated = false;
    }
    */


    /*
    public void OnEnable()
    {
        if (this.gameObject.activeInHierarchy && activated == false)
        {

            fichaAnimator.SetTrigger("appear");

            activated = true;

        }
       
        if (!this.gameObject.activeInHierarchy)
        {
            activated = false;
        }
        
    }

    
    private void OnDisable()
    {
        activated = false;
    }
    */

    public void OnEnable()
    {
        fichaAnimator.SetTrigger("appear");
    }


    public void CallAnimation()
    {

        fichaAnimator.SetTrigger("appear");

    }

}
