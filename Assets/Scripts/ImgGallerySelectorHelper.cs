﻿using System;
using System.Collections;
using System.Collections.Generic;
using UI.Pagination;
using UnityEngine;
using UnityEngine.UI;

public class ImgGallerySelectorHelper : MonoBehaviour {


    public Color32 noSelectedColor;


    [Serializable]
    public class GalleyClass
    {
        [SerializeField]
        [Header("Nombres de tipos de galerias")]
        private string nombresTipos;
        public string NombresTipos
        {
            get
            {
                return nombresTipos;
            }
            set
            {
                nombresTipos = value; 
            }
        }


        [Header("PagedRect de los Bloques de formulario del mismo tipo")]
        [SerializeField]
        private List<PagedRect> typeFormPaged;
        public List<PagedRect> TypeFormPaged
        {
            get
            {
                return typeFormPaged;
            }
        }

        [Header("PagedRect de los Botones del mismo tipo - Matcheando los Bloques")]
        [SerializeField]
        private List<PagedRect> typeButtonPaged;
        public List<PagedRect> TypeButtonPaged
        {
            get
            {
                return typeButtonPaged;
            }
        }


        [SerializeField]
        [Header("Listado de colores de cada galeria")]
        private Color32 galleryColor;
        public Color32 GalleryColor
        {
            get
            {
                return galleryColor;
            }
            set
            {
                galleryColor = value;
            }
        }


        private int tempNumber;
        public int TempNumber
        {
            get
            {
                return tempNumber;
            }
            set
            {
                tempNumber = value;
            }
        }


        private Transform tempGameObject;
        public Transform TempGameObject
        {
            get
            {
                return tempGameObject;
            }
            set
            {
                tempGameObject = value;
            }
        }
    }


    [SerializeField]
    [Header("Tipos de elementos")]
    public List<GalleyClass> claseGaleria;


    private int tempNumber;
    private Transform tempGameObject;

    // Pasar a otra funcion no start que la llame al final de la invocacion de todas las fichas
    //
    public void CheckSelector()
    {

        print("ARRANCA START");

        foreach (var item in claseGaleria)
        {


            foreach (var itemButton in item.TypeButtonPaged)
            {

                print("NUMERO PAGINAS: " + itemButton.NumberOfPages);



                //if (item.GalleryButtons.NumberOfPages == 1)
                //{

                tempNumber = 1;

                tempGameObject = itemButton.GetCurrentPage().transform.Find("GameObject");
                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                // Cambia el color del selector segun el tipo
                tempGameObject.GetComponent<Image>().color = item.GalleryColor;

                print("print color :" + item.GalleryColor);

                //}




                //for (int i = 0; i < item.GalleryButtons.Pages.Count; i++)
                foreach (var itemPages in itemButton.Pages)
                {
                    //Page pageButtonTemp = item.GalleryButtons.GetPageByNumber(i);

                    Page pageButtonTemp = itemPages;


                    var pageButtonCell = itemPages.GetComponent<GlobalButtonView>();

                    print("BUTTON NUMBER:  " + pageButtonTemp.GetComponent<GlobalButtonView>().ButtonNumber);


                    pageButtonTemp.GetComponentInChildren<Button>().onClick.AddListener(() => PagedGlobalHandler.Instance.goToPage(pageButtonCell, pageButtonTemp.GetComponent<GlobalButtonView>().ButtonNumber));


                }




            }




        }

        /*
        foreach (var item in claseGaleria)
        {

            

        }
        */

    }

    /*
    public void goToPage(GlobalButtonView paginaBoton, int number)
    {

            //number = 1;

            foreach (var item in claseGaleria)
            {

                print("LLEGA AL NUMERO: " + number + "LLEGA A PAGINA: " + item.GalleryButtons.CurrentPage);

                if (item.TempNumber != item.GalleryButtons.CurrentPage)
                {

                    item.TempGameObject.GetComponent<Image>().color = noSelectedColor;

                }


                if (number == item.GalleryButtons.CurrentPage)
                {

                    print("LLEGA A APARADO NUMBER");

                    item.TempNumber = number;

                    item.TempGameObject = paginaBoton.transform.Find("GameObject");


                    item.TempGameObject.GetComponent<Image>().color = item.GalleryColor;




                }



                //foreach (var item in claseGaleria)
                //{


                print("SET PAGINA NUMERO : " + number);

                item.GalleryButtons.SetCurrentPage(number);



                //}

            }





        

    }
    */
    
    // Update is called once per frame
    void Update () {


        }


}
