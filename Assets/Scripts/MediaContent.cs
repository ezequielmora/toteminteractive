﻿using UnityEngine;
using System.Collections;
using SQL.Structs;

[System.Serializable]
public class MediaContent{

	public int id;
	public string hash;
	public string url;
	public string localPath;
	public string mimeType;

	public MediaContent()
	{

	}

	public MediaContent(int _id, string _hash, string _url, string _localPath)
	{
		id = _id;
		hash = _hash;
		url = _url;
		localPath = _localPath;
	}

	public MediaContent(SQLMediaContent media)
	{
		id = media.id;
		hash = media.hash;
		url = media.url;
		localPath = media.localPath;
	}

}
