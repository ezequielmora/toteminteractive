﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SQL.Structs;


namespace EnhancedScrollerDemos.CellControllerIMG
{
    /// <summary>
    /// This delegate handles any changes to the selection state of the data
    /// </summary>
    /// <param name="val">The state of the selection</param>
    public delegate void SelectedChangedDelegateInst(bool val);

    /// <summary>
    /// This class represents an inventory record
    /// </summary>
    public class ImgData
    {

		public string type;
		public int imageBGId;
		public Sprite imageBGSprite;

		public string nameText;
		public string nameCientText;

		public string textA;
		public string textB;
		public string textC;
		public string textD;

		public Color titleColor;

		public Color colorA;
		public Color colorB;
		public Color colorC;
		public Color colorD;

		/*
        public string titleName;
        public Color titleColor;

        public string subTitleName;
        public Color subTitleColor;
	
        public string mainDescription;
        public Color mainColor;
        public Color mainTextColor;

        public string fullDescription;
        public Color fullColor;
        public Color fullTextColor;

        public string subDescription_1;
        public Color subDescriptionTextColor_1;
        public Color subColor_1;

        public string subDescription_2;
        public Color subDescriptionTextColor_2;
        public Color subColor_2;

        public string subDescription_3;
        public Color subDescriptionTextColor_3;
        public Color subColor_3;*/


		/*

"hash": "06973c4c293faa0d6bf3740e9b7664c7",
      "type": "flora",
      "enabled": 1,
      "imageBG_id": 207,
      "nameText": "Planta",
      "nameCientText": "Plantaulis naturalis",
      "TextA": "Lorem Ipsum mortis",
      "TextB": "Lorem Ipsum mortis",
      "TextC": "Lorem Ipsum mortis",
      "TextD": "Lorem Ipsum mortis",
      "titleColor": "#549379",
      "ColorA": "#2b686b",
      "ColorB": "#c8d9d8",
      "ColorC": "#f05323",
      "ColorD": "#2b686b"

		*/

        public string imgPath;

        /// <summary>
        /// The delegate to call if the data's selection state
        /// has changed. This will update any views that are hooked
        /// to the data so that they show the proper selection state UI.
        /// </summary>
        public SelectedChangedDelegateInst selectedChanged;

        /// <summary>
        /// The selection state
        /// </summary>
        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                // if the value has changed
                if (_selected != value)
                {
                    // update the state and call the selection handler if it exists
                    _selected = value;
                    if (selectedChanged != null)
                    {
                        selectedChanged(_selected);
                    }
                }
            }
        }

        // Saca los datos de las base de datos y los asigna a las variables de ImgData
		public ImgData() {
		}

		public ImgData(SQLImgData imgData) {
			type = imgData.type;
			imageBGId = imgData.imageBGId;
			nameText = imgData.nameText;
			nameCientText = imgData.nameCientText;
			textA = imgData.textA;
			textB = imgData.textB;
			textC = imgData.textC;
			textD = imgData.textD;
			ColorUtility.TryParseHtmlString(imgData.titleColor, out titleColor);
			ColorUtility.TryParseHtmlString(imgData.colorA, out colorA);
			ColorUtility.TryParseHtmlString(imgData.colorB, out colorB);
			ColorUtility.TryParseHtmlString(imgData.colorC, out colorC);
			ColorUtility.TryParseHtmlString(imgData.colorD, out colorD);
		}
    }
}