﻿using UnityEngine;
using System.Collections;
using SQL.Structs;

namespace EnhancedScrollerDemos.CellControllerInstitucional
{
    /// <summary>
    /// This delegate handles any changes to the selection state of the data
    /// </summary>
    /// <param name="val">The state of the selection</param>
    public delegate void SelectedChangedDelegateInst(bool val);

    /// <summary>
    /// This class represents an inventory record
    /// </summary>
    public class InstitucionalData
    {
        
		/// <summary>
		/// Type
		/// </summary>
		public string type;

		/// <summary>
        /// The name of the form item
        /// </summary>
        public string titleName;
        public Color titleColor;

        /// <summary>
        /// This description of the item
        /// </summary>
        public string mainDescription;
        public Color mainColor;
		public Color mainColorBack;
        public Color mainTextColor;

        /// <summary>
        /// This sub-description of the item
        /// </summary>
        public string subDescription;
        public Color subColor;

        /// <summary>
        /// This full-description of the item
        /// </summary>
        public string fullDescription;
        public Color fullColor;
        public Color fullTextColor;

        /// <summary>
        /// The path to the resources folder for the sprite
        /// representing this inventory item
        /// </summary>
        public string spritePath;
		public int imgId;
		public Sprite sprite;
        /// <summary>
        /// The delegate to call if the data's selection state
        /// has changed. This will update any views that are hooked
        /// to the data so that they show the proper selection state UI.
        /// </summary>
        public SelectedChangedDelegateInst selectedChanged;


		public InstitucionalData() {
		}

		public InstitucionalData(SQLInstitucionalData instData) {
			titleName = instData.titleName;
			type = instData.type;
			ColorUtility.TryParseHtmlString(instData.titleColor, out titleColor);
			mainDescription = instData.mainDescription;
			ColorUtility.TryParseHtmlString(instData.mainColor, out mainColor);
			ColorUtility.TryParseHtmlString(instData.mainTextColor, out mainTextColor);
			subDescription = instData.subDescription;
			ColorUtility.TryParseHtmlString(instData.subColor, out subColor);
			fullDescription = instData.fullDescription;
			ColorUtility.TryParseHtmlString(instData.fullColor, out fullColor);
			ColorUtility.TryParseHtmlString(instData.fullTextColor, out fullTextColor);
			imgId = instData.imgId;
		}

        /// <summary>
        /// The selection state
        /// </summary>
        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                // if the value has changed
                if (_selected != value)
                {
                    // update the state and call the selection handler if it exists
                    _selected = value;
                    if (selectedChanged != null)
                    {
                        selectedChanged(_selected);
                    }
                }
            }
        }
    }
}