using UnityEngine;
using System.Collections.Generic;
using SQL.Structs;


/// <summary>
/// This class represents an inventory record
/// </summary>
public class InunSequiaData
{

	public int id;
	public string status;
	public string generalTitle;
	public string generalText;
	public string dynamicText;


	/*

	  "id": 1,
      "hash": "58e667e0e32400355d80986c97d99754",
      "status": "medio",
      "enabled": 1,
      "generalTitle": "Titulo general del estado alto....",
      "generalText": "Información general del estado....",
      "dynamicText": "Informacion dinamica del estado......",

	*/

	public InunSequiaData() {
	}

	public InunSequiaData(SQLInunSequiaData	 inunSequiaData) {
		id = inunSequiaData.id;
		status = inunSequiaData.status;
		generalTitle = inunSequiaData.generalTitle;
		generalText = inunSequiaData.generalText;
		dynamicText = inunSequiaData.dynamicText;
	}
}