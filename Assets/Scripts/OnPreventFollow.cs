﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnPreventFollow : MonoBehaviour {


    //public Button button;


	// Use this for initialization
	void Start () {

        //button = gameObject.GetComponent<Button>();

        //button.onClick.AddListener(TaskOnClick);

        // Eventos van lado izquierdo, se subscriben a funciones, para instancias singleton
        //cameraDragController.cameraControllerEvent 

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // Event trigger en el slider control para evitar movimiento de camara mientras hago el drag
    // demas ui elements, control puesto en la misma transicion
    public void TaskOnClick()
    {
        //Debug.Log("You have clicked the button!");


        CameraDragController.instance.allowFollow = false; //CameraDragController.instance.PreventFollow();
        
    }


    public void TaskOnRelease()
    {
        //Debug.Log("You have release the button!");



        CameraDragController.instance.allowFollow = true;

    }
}
