﻿using EnhancedScrollerDemos.CellControllerIMG;
using EnhancedScrollerDemos.CellControllerInstitucional;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI.Pagination;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PagedGlobalHandler : MonoBehaviour
{


    /// <summary>
    /// Controlador principal para manejar todas las fichas de los 5 bloques de institucional
    /// </summary>
    /// 

    /// <summary>
    /// Clase para manejar los distintos tipos de contenedores globales, dentro a su vez
    /// Listado para manejar todos las fichas y botones al mismo tiempo de manera de aplicar operaciones sobre el
    /// activo y sobre el resto cuando no esten activos
    /// </summary>

    //[Header("Color para boton seleccionado")]
    //[SerializeField]
    //public Color32 selectedColor = new Color32(255, 207, 1, 255);

    private SelectorGlobalHelper selectorGlobal;


    // Variable de control para permitir movimiento de drag una vez que se cumpla el tiempo
    [HideInInspector]
    public bool allowDragging = false;


    [Header("Color para boton No seleccionado")]
    [SerializeField]
    public Color32 noSelectedColor = new Color32(150, 166, 166, 255);

    //[Header("Selector Helper")]
    //public ImgGallerySelectorHelper imgHelper;

    [Serializable]
    public class tipoContenido
    {

        [Header("Nombre del Tipo de Galeria Indicado")]
        [SerializeField]
        private string typeName;
        public string TypeName
        {
            get
            {
                return typeName;
            }
        }

        [Header("Selection Color diferencial de ese tipo de Galeria")]
        [SerializeField]
        private Color32 typeColor;
        public Color32 TypeColor
        {
            get
            {
                return typeColor;
            }
        }


        [Header("Este boton esta saltando")]
        [SerializeField]
        private bool isJumping = true;
        public bool IsJumping
        {
            get
            {
                return isJumping;
            }
            set
            {
                isJumping = value;
            }
        }


        [Header("PagedRect de los Bloques de formulario del mismo tipo")]
        [SerializeField]
        private PagedRect typeFormPaged;
        public PagedRect TypeFormPaged
        {
            get
            {
                return typeFormPaged;
            }
        }

        [Header("PagedRect de los Botones del mismo tipo - Matcheando los Bloques")]
        [SerializeField]
        private PagedRect typeButtonPaged;
        public PagedRect TypeButtonPaged
        {
            get
            {
                return typeButtonPaged;
            }
        }

        [Header("Listado de Formularios Runtime generados en ese tipo")]
        [SerializeField]
        private List<Page> formList = new List<Page>();
        public List<Page> FormList
        {
            get
            {
                return formList;
            }
        }

        [Header("Listado de botones Runtime generados en ese tipo")]
        [SerializeField]
        private List<Page> buttonList = new List<Page>();
        public List<Page> ButtonList
        {
            get
            {
                return buttonList;
            }
        }


        /// <summary>
        /// Variable temporal para guardar el objeto Ficha o Boton anterior
        /// </summary>
        /// 
        private Transform tempGameObject;
        public Transform TempGameObject
        {
            get
            {
                return tempGameObject;
            }
            set
            {
                tempGameObject = value;
            }
        }
        /// <summary>
        /// Variable temporal para guardar el numero de boton o ficha anterior
        /// </summary>
        private int tempNumber;
        public int TempNumber
        {
            get
            {
                return tempNumber;
            }
            set
            {
                tempNumber = value;
            }
        }

        // Deteccion de si se presiono el boton relacionado a una ficha
        //[Header("Detecta salto de pagina")]
        //[SerializeField]
        //public bool isPressed = false;


        /// <summary>
        /// Variable temporal para guardar el objeto Ficha o Boton anterior
        /// </summary>
        private Transform tempGameObject2;
        public Transform TempGameObject2
        {
            get
            {
                return tempGameObject2;
            }
            set
            {
                tempGameObject2 = value;
            }
        }
        /// <summary>
        /// Variable temporal para guardar el numero de boton o ficha anterior
        /// </summary>
        private int tempNumber2;
        public int TempNumber2
        {
            get
            {
                return tempNumber2;
            }
            set
            {
                tempNumber2 = value;
            }
        }


        private Transform tempPage;
        public Transform TempPage
        {
            get
            {
                return tempPage;
            }
            set
            {
                tempPage = value;
            }
        }

        private Transform tempPage2;
        public Transform TempPage2
        {
            get
            {
                return tempPage2;
            }
            set
            {
                tempPage2 = value;
            }
        }


        /*
        /// Variable temporal para guardar el numero de boton o ficha anterior
        /// </summary>
        private int tempNumberCache;
        public int TempNumberCache
        {
            get
            {
                return tempNumberCache;
            }
            set
            {
                tempNumberCache = value;
            }
        }

        // Deteccion de si se presiono el boton relacionado a una ficha
        //[Header("Detecta salto de pagina")]
        //[SerializeField]
        //public bool isPressed = false;


        /// <summary>
        /// Variable temporal para guardar el objeto Ficha o Boton anterior
        /// </summary>
        private Transform tempGameObjectCache;
        public Transform TempGameObjectCache
        {
            get
            {
                return tempGameObjectCache;
            }
            set
            {
                tempGameObjectCache = value;
            }
        }
        */

    }

    [Header("Cantidad de Diferentes Tipos de Galerias de Imagenes Globales")]
    [SerializeField]
    public List<tipoContenido> tiposDeContenido;

    /// <summary>
    /// Forma de trackear cual es el boton activo
    /// </summary>
    //private int currentButtonIndex = 0;
    //private Page currentButton;

    //[Header("Paged Rect Respectivo del bloque Anfibio | Lista para todas las galerias de anfibios")]
    //public List<PagedRect> anfibioPaged;

    //[Header("Paged Rect Respectivo a los Botones de Anfibio | Lista para todas las galerias de anfibios")]
    //public List<PagedRect> anfibioButtonPaged;



    //private Transform tempPage3;

    //private bool isMovingH = false;
    //private bool isMovingV = false;



    //private Page[] page;

    [HideInInspector]
    public List<ImgData> imgData;
    private bool imgDataLoaded;


    private Page page;
    private Page buttonPage;


    /// <summary>
    /// Flag de control del controlador de galeria de imagenes
    /// </summary>
    [HideInInspector]
    public bool feedGlobalLoaded = false;


    private static PagedGlobalHandler instance;
    public static PagedGlobalHandler Instance
    {
        get
        {
            return instance;
        }
    }


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);

        DontDestroyOnLoad(this);
    }

    /*
    // Use this for initialization
    void AddListeners()
    {

        foreach (var itemType in tiposDeContenido)
        {
            
                itemType.TypeFormPaged.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(ChangeParallelPageNew);
                print("Added horizontal listener");
          


            
                itemType.TypeButtonPaged.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(ChangeParallelButtonNew);
                print("Added vertical listener");

                //TODO 
                //itemButton.GetComponent<PagedRect_ScrollRect>().DisableDragging = false;
            
        }


    }
    */


    // Update is called once per frame
    void Start()
    {
        StartCoroutine(Init());

        //imgHelper.CheckSelector();
    }

    public IEnumerator Init()
    {

        while (!imgDataLoaded)
        {
            yield return null;
        }

        Debug.Log("IMGDATA LOADED!");

        // Recorre todos los items de la DB para instanciar las paginas
        for (int i = 0; i < imgData.Count; i++)
        {
            //string searchType = imgData.FindAll(x => x.type);

            switch (imgData[i].type)
            {
                case "flora":
                    var floraForm = tiposDeContenido.FindAll(x => x.TypeName == "Flora");

                    foreach (var item in floraForm)
                    {
                        page = item.TypeFormPaged.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.TypeFormPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeFormPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.TypeFormPaged.NumberOfPages;


                        // Page customization fields and functions
                        //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

                        page.GetComponent<CellViewIMG>().hTextTitle.text = imgData[i].nameText; //json_nameText;
                        page.GetComponent<CellViewIMG>().hSubTitle.text = imgData[i].nameCientText; //json_nameCientText;
                        page.GetComponent<CellViewIMG>().imageBackground.sprite = imgData[i].imageBGSprite; //json_imageBG;

                        page.GetComponent<CellViewIMG>().mainText.text = imgData[i].textA; //json_TextA;
                        page.GetComponent<CellViewIMG>().fullText.text = imgData[i].textA;  //json_TextA;
                        page.GetComponent<CellViewIMG>().subText_1.text = imgData[i].textB; //json_TextB;
                        page.GetComponent<CellViewIMG>().subText_2.text = imgData[i].textC; //json_TextC;
                        page.GetComponent<CellViewIMG>().subText_3.text = imgData[i].textD; //json_TextD;


                        // Color del boton mas segun el color de titulo
                        page.GetComponent<CellViewIMG>().moreInfoContainer.GetComponent<Image>().color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().titleColor.color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().mainColorField.GetComponent<Image>().color = imgData[i].colorA; //json_ColorA;
                        page.GetComponent<CellViewIMG>().subColorField_1.GetComponent<Image>().color = imgData[i].colorB; //json_ColorB;
                        page.GetComponent<CellViewIMG>().subColorField_2.GetComponent<Image>().color = imgData[i].colorC; //json_ColorC;
                        page.GetComponent<CellViewIMG>().subColorField_3.GetComponent<Image>().color = imgData[i].colorD; //json_ColorD;

                        // No se va a usar pero Dejar para implementacion futura de colores de demas textos
                        //page.GetComponent<CellViewIMG>().mainText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().fullText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().subText_1.color = json_ColorTextB;
                        //page.GetComponent<CellViewIMG>().subText_2.color = json_ColorTextC;
                        //page.GetComponent<CellViewIMG>().subText_3.color = json_ColorTextD;


                        if (page.GetComponent<CellViewIMG>().mainText.text.ToString().Length < 590)
                        {
                            page.GetComponent<CellViewIMG>().moreInfoContainer.gameObject.SetActive(false);
                        }



                        buttonPage = item.TypeButtonPaged.AddPageUsingTemplate();

                        buttonPage.PageTitle = "Number" + item.TypeButtonPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeButtonPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        buttonPage.GetComponent<GlobalButtonView>().ButtonNumber = item.TypeButtonPaged.NumberOfPages;


                        buttonPage.GetComponent<GlobalButtonView>().nombrePopular.text = imgData[i].nameText; //json_nameText;
                        buttonPage.GetComponent<GlobalButtonView>().nombreCientifico.text = imgData[i].nameCientText; //json_nameCientText;


                        //if (thisItemType.TypeButtonPaged.Count == 1)
                        /*
                        if (itemButton.NumberOfPages == 1)
                        {

                            floraForm.TempNumber = 1;

                            floraForm.TempGameObjectCache = itemButton.GetCurrentPage().transform.Find("GameObject");
                            //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                            // Cambia el color del selector segun el tipo
                            floraForm.TempGameObjectCache.GetComponent<Image>().color = floraForm.TypeColor;

                        }
                        */
                        // Agrega el boton creada al listado de botones
                        //floraForm.ButtonList.Add(buttonPage);










                      
                    }

                    break;

                    

                case "fauna":

                    var faunaForm = tiposDeContenido.FindAll(x => x.TypeName == "Fauna");

                    foreach (var item in faunaForm)
                    {
                        page = item.TypeFormPaged.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.TypeFormPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeFormPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.TypeFormPaged.NumberOfPages;

                        // Page customization fields and functions
                        //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

                        page.GetComponent<CellViewIMG>().hTextTitle.text = imgData[i].nameText; //json_nameText;
                        page.GetComponent<CellViewIMG>().hSubTitle.text = imgData[i].nameCientText; //json_nameCientText;
                        page.GetComponent<CellViewIMG>().imageBackground.sprite = imgData[i].imageBGSprite; //json_imageBG;

                        page.GetComponent<CellViewIMG>().mainText.text = imgData[i].textA; //json_TextA;
                        page.GetComponent<CellViewIMG>().fullText.text = imgData[i].textA;  //json_TextA;
                        page.GetComponent<CellViewIMG>().subText_1.text = imgData[i].textB; //json_TextB;
                        page.GetComponent<CellViewIMG>().subText_2.text = imgData[i].textC; //json_TextC;
                        page.GetComponent<CellViewIMG>().subText_3.text = imgData[i].textD; //json_TextD;


                        // Color del boton mas segun el color de titulo
                        page.GetComponent<CellViewIMG>().moreInfoContainer.GetComponent<Image>().color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().titleColor.color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().mainColorField.GetComponent<Image>().color = imgData[i].colorA; //json_ColorA;
                        page.GetComponent<CellViewIMG>().subColorField_1.GetComponent<Image>().color = imgData[i].colorB; //json_ColorB;
                        page.GetComponent<CellViewIMG>().subColorField_2.GetComponent<Image>().color = imgData[i].colorC; //json_ColorC;
                        page.GetComponent<CellViewIMG>().subColorField_3.GetComponent<Image>().color = imgData[i].colorD; //json_ColorD;

                        // No se va a usar pero Dejar para implementacion futura de colores de demas textos
                        //page.GetComponent<CellViewIMG>().mainText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().fullText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().subText_1.color = json_ColorTextB;
                        //page.GetComponent<CellViewIMG>().subText_2.color = json_ColorTextC;
                        //page.GetComponent<CellViewIMG>().subText_3.color = json_ColorTextD;

                        if (page.GetComponent<CellViewIMG>().mainText.text.ToString().Length < 590)
                        {
                            page.GetComponent<CellViewIMG>().moreInfoContainer.gameObject.SetActive(false);
                        }


                        buttonPage = item.TypeButtonPaged.AddPageUsingTemplate();

                        buttonPage.PageTitle = "Number" + item.TypeButtonPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeButtonPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        buttonPage.GetComponent<GlobalButtonView>().ButtonNumber = item.TypeButtonPaged.NumberOfPages;

                        buttonPage.GetComponent<GlobalButtonView>().nombrePopular.text = imgData[i].nameText; //json_nameText;
                        buttonPage.GetComponent<GlobalButtonView>().nombreCientifico.text = imgData[i].nameCientText; //json_nameCientText;

                        //if (thisItemType.TypeButtonPaged.Count == 1)

                        /*
                        if (itemButton.NumberOfPages == 1)
                        {

                            faunaForm.TempNumberCache = 1;

                            faunaForm.TempGameObjectCache = itemButton.GetCurrentPage().transform.Find("GameObject");
                            //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                            // Cambia el color del selector segun el tipo
                            faunaForm.TempGameObjectCache.GetComponent<Image>().color = faunaForm.TypeColor;

                        }
                        */
                        // Agrega el boton creada al listado de botones
                        //faunaForm.ButtonList.Add(buttonPage);





                    }



                    break;

                case "aves":

                    var avesForm = tiposDeContenido.FindAll(x => x.TypeName == "Aves");

                    foreach (var item in avesForm)
                    {
                        page = item.TypeFormPaged.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.TypeFormPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeFormPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.TypeFormPaged.NumberOfPages;

                        // Page customization fields and functions
                        //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

                        page.GetComponent<CellViewIMG>().hTextTitle.text = imgData[i].nameText; //json_nameText;
                        page.GetComponent<CellViewIMG>().hSubTitle.text = imgData[i].nameCientText; //json_nameCientText;
                        page.GetComponent<CellViewIMG>().imageBackground.sprite = imgData[i].imageBGSprite; //json_imageBG;

                        page.GetComponent<CellViewIMG>().mainText.text = imgData[i].textA; //json_TextA;
                        page.GetComponent<CellViewIMG>().fullText.text = imgData[i].textA;  //json_TextA;
                        page.GetComponent<CellViewIMG>().subText_1.text = imgData[i].textB; //json_TextB;
                        page.GetComponent<CellViewIMG>().subText_2.text = imgData[i].textC; //json_TextC;
                        page.GetComponent<CellViewIMG>().subText_3.text = imgData[i].textD; //json_TextD;


                        // Color del boton mas segun el color de titulo
                        page.GetComponent<CellViewIMG>().moreInfoContainer.GetComponent<Image>().color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().titleColor.color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().mainColorField.GetComponent<Image>().color = imgData[i].colorA; //json_ColorA;
                        page.GetComponent<CellViewIMG>().subColorField_1.GetComponent<Image>().color = imgData[i].colorB; //json_ColorB;
                        page.GetComponent<CellViewIMG>().subColorField_2.GetComponent<Image>().color = imgData[i].colorC; //json_ColorC;
                        page.GetComponent<CellViewIMG>().subColorField_3.GetComponent<Image>().color = imgData[i].colorD; //json_ColorD;

                        // No se va a usar pero Dejar para implementacion futura de colores de demas textos
                        //page.GetComponent<CellViewIMG>().mainText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().fullText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().subText_1.color = json_ColorTextB;
                        //page.GetComponent<CellViewIMG>().subText_2.color = json_ColorTextC;
                        //page.GetComponent<CellViewIMG>().subText_3.color = json_ColorTextD;


                        if (page.GetComponent<CellViewIMG>().mainText.text.ToString().Length < 590)
                        {
                            page.GetComponent<CellViewIMG>().moreInfoContainer.gameObject.SetActive(false);
                        }




                        buttonPage = item.TypeButtonPaged.AddPageUsingTemplate();

                        buttonPage.PageTitle = "Number" + item.TypeButtonPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeButtonPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        buttonPage.GetComponent<GlobalButtonView>().ButtonNumber = item.TypeButtonPaged.NumberOfPages;


                        buttonPage.GetComponent<GlobalButtonView>().nombrePopular.text = imgData[i].nameText; //json_nameText;
                        buttonPage.GetComponent<GlobalButtonView>().nombreCientifico.text = imgData[i].nameCientText; //json_nameCientText;

                        //if (thisItemType.TypeButtonPaged.Count == 1)
                        /*
                        if (itemButton.NumberOfPages == 1)
                        {

                            avesForm.TempNumberCache = 1;

                            avesForm.TempGameObjectCache = itemButton.GetCurrentPage().transform.Find("GameObject");
                            //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                            // Cambia el color del selector segun el tipo
                            avesForm.TempGameObjectCache.GetComponent<Image>().color = avesForm.TypeColor;

                        }
                        */
                        // Agrega el boton creada al listado de botones
                        //avesForm.ButtonList.Add(buttonPage);





                    }



                    break;

                case "anfibios":

                    var anfibForm = tiposDeContenido.FindAll(x => x.TypeName == "Anfibios");

                    foreach (var item in anfibForm)
                    {
                        page = item.TypeFormPaged.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.TypeFormPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeFormPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.TypeFormPaged.NumberOfPages;

                        // Page customization fields and functions
                        //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

                        page.GetComponent<CellViewIMG>().hTextTitle.text = imgData[i].nameText; //json_nameText;
                        page.GetComponent<CellViewIMG>().hSubTitle.text = imgData[i].nameCientText; //json_nameCientText;
                        page.GetComponent<CellViewIMG>().imageBackground.sprite = imgData[i].imageBGSprite; //json_imageBG;

                        page.GetComponent<CellViewIMG>().mainText.text = imgData[i].textA; //json_TextA;
                        page.GetComponent<CellViewIMG>().fullText.text = imgData[i].textA;  //json_TextA;
                        page.GetComponent<CellViewIMG>().subText_1.text = imgData[i].textB; //json_TextB;
                        page.GetComponent<CellViewIMG>().subText_2.text = imgData[i].textC; //json_TextC;
                        page.GetComponent<CellViewIMG>().subText_3.text = imgData[i].textD; //json_TextD;


                        // Color del boton mas segun el color de titulo
                        page.GetComponent<CellViewIMG>().moreInfoContainer.GetComponent<Image>().color = imgData[i].titleColor; //json_titleColor;

                        page.GetComponent<CellViewIMG>().titleColor.color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().mainColorField.GetComponent<Image>().color = imgData[i].colorA; //json_ColorA;
                        page.GetComponent<CellViewIMG>().subColorField_1.GetComponent<Image>().color = imgData[i].colorB; //json_ColorB;
                        page.GetComponent<CellViewIMG>().subColorField_2.GetComponent<Image>().color = imgData[i].colorC; //json_ColorC;
                        page.GetComponent<CellViewIMG>().subColorField_3.GetComponent<Image>().color = imgData[i].colorD; //json_ColorD;

                        // No se va a usar pero Dejar para implementacion futura de colores de demas textos
                        //page.GetComponent<CellViewIMG>().mainText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().fullText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().subText_1.color = json_ColorTextB;
                        //page.GetComponent<CellViewIMG>().subText_2.color = json_ColorTextC;
                        //page.GetComponent<CellViewIMG>().subText_3.color = json_ColorTextD;

                        if (page.GetComponent<CellViewIMG>().mainText.text.ToString().Length < 590)
                        {
                            page.GetComponent<CellViewIMG>().moreInfoContainer.gameObject.SetActive(false);
                        }



                        buttonPage = item.TypeButtonPaged.AddPageUsingTemplate();

                        buttonPage.PageTitle = "Number" + item.TypeButtonPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeButtonPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        buttonPage.GetComponent<GlobalButtonView>().ButtonNumber = item.TypeButtonPaged.NumberOfPages;


                        buttonPage.GetComponent<GlobalButtonView>().nombrePopular.text = imgData[i].nameText; //json_nameText;
                        buttonPage.GetComponent<GlobalButtonView>().nombreCientifico.text = imgData[i].nameCientText; //json_nameCientText;

                        //if (thisItemType.TypeButtonPaged.Count == 1)
                        /*
                        if (itemButton.NumberOfPages == 1)
                        {

                            anfibForm.TempNumberCache = 1;

                            anfibForm.TempGameObjectCache = itemButton.GetCurrentPage().transform.Find("GameObject");
                            //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                            // Cambia el color del selector segun el tipo
                            anfibForm.TempGameObjectCache.GetComponent<Image>().color = anfibForm.TypeColor;

                        }
                        */
                        // Agrega el boton creada al listado de botones
                        //anfibForm.ButtonList.Add(buttonPage);



                    }





                    break;

                case "reptiles":

                    var reptiForm = tiposDeContenido.FindAll(x => x.TypeName == "Reptiles");

                    foreach (var item in reptiForm)
                    {

                        page = item.TypeFormPaged.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.TypeFormPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeFormPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.TypeFormPaged.NumberOfPages;


                        // Page customization fields and functions
                        //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

                        page.GetComponent<CellViewIMG>().hTextTitle.text = imgData[i].nameText; //json_nameText;
                        page.GetComponent<CellViewIMG>().hSubTitle.text = imgData[i].nameCientText; //json_nameCientText;
                        page.GetComponent<CellViewIMG>().imageBackground.sprite = imgData[i].imageBGSprite; //json_imageBG;

                        page.GetComponent<CellViewIMG>().mainText.text = imgData[i].textA; //json_TextA;
                        page.GetComponent<CellViewIMG>().fullText.text = imgData[i].textA;  //json_TextA;
                        page.GetComponent<CellViewIMG>().subText_1.text = imgData[i].textB; //json_TextB;
                        page.GetComponent<CellViewIMG>().subText_2.text = imgData[i].textC; //json_TextC;
                        page.GetComponent<CellViewIMG>().subText_3.text = imgData[i].textD; //json_TextD;


                        // Color del boton mas segun el color de titulo
                        page.GetComponent<CellViewIMG>().moreInfoContainer.GetComponent<Image>().color = imgData[i].titleColor; //json_titleColor;

                        page.GetComponent<CellViewIMG>().titleColor.color = imgData[i].titleColor; //json_titleColor;


                        page.GetComponent<CellViewIMG>().mainColorField.GetComponent<Image>().color = imgData[i].colorA; //json_ColorA;
                        page.GetComponent<CellViewIMG>().subColorField_1.GetComponent<Image>().color = imgData[i].colorB; //json_ColorB;
                        page.GetComponent<CellViewIMG>().subColorField_2.GetComponent<Image>().color = imgData[i].colorC; //json_ColorC;
                        page.GetComponent<CellViewIMG>().subColorField_3.GetComponent<Image>().color = imgData[i].colorD; //json_ColorD;

                        // No se va a usar pero Dejar para implementacion futura de colores de demas textos
                        //page.GetComponent<CellViewIMG>().mainText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().fullText.color = json_ColorTextA;
                        //page.GetComponent<CellViewIMG>().subText_1.color = json_ColorTextB;
                        //page.GetComponent<CellViewIMG>().subText_2.color = json_ColorTextC;
                        //page.GetComponent<CellViewIMG>().subText_3.color = json_ColorTextD;


                        if (page.GetComponent<CellViewIMG>().mainText.text.ToString().Length < 590)
                        {
                            page.GetComponent<CellViewIMG>().moreInfoContainer.gameObject.SetActive(false);
                        }



                        buttonPage = item.TypeButtonPaged.AddPageUsingTemplate();

                        buttonPage.PageTitle = "Number" + item.TypeButtonPaged.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.TypeButtonPaged.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        buttonPage.GetComponent<GlobalButtonView>().ButtonNumber = item.TypeButtonPaged.NumberOfPages;


                        buttonPage.GetComponent<GlobalButtonView>().nombrePopular.text = imgData[i].nameText; //json_nameText;
                        buttonPage.GetComponent<GlobalButtonView>().nombreCientifico.text = imgData[i].nameCientText; //json_nameCientText;

                        //if (thisItemType.TypeButtonPaged.Count == 1)
                        /*
                        if (itemButton.NumberOfPages == 1)
                        {

                            reptiForm.TempNumberCache = 1;

                            reptiForm.TempGameObjectCache = itemButton.GetCurrentPage().transform.Find("GameObject");
                            //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                            // Cambia el color del selector segun el tipo
                            reptiForm.TempGameObjectCache.GetComponent<Image>().color = reptiForm.TypeColor;

                        }
                        */
                        // Agrega el boton creada al listado de botones
                        //reptiForm.ButtonList.Add(buttonPage);





                    }



                    break;
            }

            //ST
            //AddListeners();

            selectorGlobal = this.gameObject.GetComponent<SelectorGlobalHelper>();

            yield return StartCoroutine(selectorGlobal.Init());



        }

        selectorGlobal = this.gameObject.GetComponent<SelectorGlobalHelper>();

        yield return StartCoroutine(selectorGlobal.Init());

        /*if (ContentHandler.Instance != null)
            yield return new WaitUntil(() => ContentHandler.Instance.isLoaded == true);

        // if the data existed previously, loop through
        // and remove the selection change handlers before
        // clearing out the data.
        if (_data != null)
        {
            for (var i = 0; i < _data.Count; i++)
            {
                _data[i].selectedChanged = null;
            }
        }

        // set up a new inventory list
        _data = new List<ImgData>();

        print("WAIT FINISHED");

        // ACA IRIA LA FUNCION "SetGlobalGalleryData" - ESPERA A QUE SE COMPLETE PARA CONTINUAR

        yield return StartCoroutine(SetGlobalGalleryData(_data));


        feedGlobalLoaded = true;


        yield return null;*/

    }


    public void SetGlobalGalleryData(List<ImgData> _imgData)
    {
        imgData = _imgData;
        imgDataLoaded = true;
        // Aca poner toda la operatoria de carga de datos en los formularios

        /////////////////
        ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
        /////////////////
        ///// DESCOMENTAR


        /*

        // Recorre todos los items de la DB para instanciar las paginas
        for (int i = 0; i < _data.Count; i++)
        {
            string searchType = _data.FindAll(x => x.json_totemGlobal_type);

            switch (searchType)
            {
                case "flora":
                    var floraForm = tiposDeContenido.Find(x => x.TypeName == "Flora");


                    foreach (var item in floraForm.TypeFormPaged)
                    {
                        page = item.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.NumberOfPages;


                        foreach (var itemButton in floraForm.TypeButtonPaged)
                        {
                            buttonPage = itemButton.AddPageUsingTemplate();

                            buttonPage.PageTitle = "Number" + itemButton.NumberOfPages;

                            // Update Pagination to update title and fields
                            itemButton.UpdatePagination();

                            ////// DATA PARA QUE CARGUE DARIO

                            // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                            page.GetComponent<GlobalButtonView>().ButtonNumber = itemButton.NumberOfPages;

                            //if (thisItemType.TypeButtonPaged.Count == 1)
                            if (itemButton.NumberOfPages == 1)
                            {

                                tempNumber = 1;

                                tempGameObject = itemButton.GetCurrentPage().transform.Find("GameObject");
                                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                // Cambia el color del selector segun el tipo
                                tempGameObject.GetComponent<Image>().color = floraForm.TypeColor;

                            }

                            // Agrega el boton creada al listado de botones
                            floraForm.ButtonList.Add(buttonPage);

                        }

                    }


                    break;

                case "fauna":

                    var faunaForm = tiposDeContenido.Find(x => x.TypeName == "Fauna");

                    foreach (var item in faunaForm.TypeFormPaged)
                    {
                        page = item.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.NumberOfPages;


                        foreach (var itemButton in faunaForm.TypeButtonPaged)
                        {
                            buttonPage = itemButton.AddPageUsingTemplate();

                            buttonPage.PageTitle = "Number" + itemButton.NumberOfPages;

                            // Update Pagination to update title and fields
                            itemButton.UpdatePagination();

                            ////// DATA PARA QUE CARGUE DARIO

                            // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                            page.GetComponent<GlobalButtonView>().ButtonNumber = itemButton.NumberOfPages;

                            //if (thisItemType.TypeButtonPaged.Count == 1)
                            if (itemButton.NumberOfPages == 1)
                            {

                                tempNumber = 1;

                                tempGameObject = itemButton.GetCurrentPage().transform.Find("GameObject");
                                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                // Cambia el color del selector segun el tipo
                                tempGameObject.GetComponent<Image>().color = faunaForm.TypeColor;

                            }

                            // Agrega el boton creada al listado de botones
                            faunaForm.ButtonList.Add(buttonPage);

                        }

                    }


                    break;

                case "aves":

                    var avesForm = tiposDeContenido.Find(x => x.TypeName == "Aves");

                    foreach (var item in avesForm.TypeFormPaged)
                    {
                        page = item.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.NumberOfPages;


                        foreach (var itemButton in avesForm.TypeButtonPaged)
                        {
                            buttonPage = itemButton.AddPageUsingTemplate();

                            buttonPage.PageTitle = "Number" + itemButton.NumberOfPages;

                            // Update Pagination to update title and fields
                            itemButton.UpdatePagination();

                            ////// DATA PARA QUE CARGUE DARIO

                            // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                            page.GetComponent<GlobalButtonView>().ButtonNumber = itemButton.NumberOfPages;

                            //if (thisItemType.TypeButtonPaged.Count == 1)
                            if (itemButton.NumberOfPages == 1)
                            {

                                tempNumber = 1;

                                tempGameObject = itemButton.GetCurrentPage().transform.Find("GameObject");
                                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                // Cambia el color del selector segun el tipo
                                tempGameObject.GetComponent<Image>().color = avesForm.TypeColor;

                            }

                            // Agrega el boton creada al listado de botones
                            avesForm.ButtonList.Add(buttonPage);

                        }

                    }


                    break;

                case "anfibios":

                    var anfibForm = tiposDeContenido.Find(x => x.TypeName == "Anfibios");

                    foreach (var item in anfibForm.TypeFormPaged)
                    {
                        page = item.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.NumberOfPages;


                        foreach (var itemButton in anfibForm.TypeButtonPaged)
                        {
                            buttonPage = itemButton.AddPageUsingTemplate();

                            buttonPage.PageTitle = "Number" + itemButton.NumberOfPages;

                            // Update Pagination to update title and fields
                            itemButton.UpdatePagination();

                            ////// DATA PARA QUE CARGUE DARIO

                            // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                            page.GetComponent<GlobalButtonView>().ButtonNumber = itemButton.NumberOfPages;

                            //if (thisItemType.TypeButtonPaged.Count == 1)
                            if (itemButton.NumberOfPages == 1)
                            {

                                tempNumber = 1;

                                tempGameObject = itemButton.GetCurrentPage().transform.Find("GameObject");
                                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                // Cambia el color del selector segun el tipo
                                tempGameObject.GetComponent<Image>().color = anfibForm.TypeColor;

                            }

                            // Agrega el boton creada al listado de botones
                            anfibForm.ButtonList.Add(buttonPage);

                        }

                    }


                    break;

                case "reptiles":

                    var reptiForm = tiposDeContenido.Find(x => x.TypeName == "Reptiles");

                    foreach (var item in reptiForm.TypeFormPaged)
                    {
                        page = item.AddPageUsingTemplate();

                        page.PageTitle = "Number" + item.NumberOfPages;

                        // Update Pagination to update title and fields
                        item.UpdatePagination();

                        ////// DATA PARA QUE CARGUE DARIO

                        // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                        page.GetComponent<CellViewIMG>().PageNumber = item.NumberOfPages;


                        foreach (var itemButton in reptiForm.TypeButtonPaged)
                        {
                            buttonPage = itemButton.AddPageUsingTemplate();

                            buttonPage.PageTitle = "Number" + itemButton.NumberOfPages;

                            // Update Pagination to update title and fields
                            itemButton.UpdatePagination();

                            ////// DATA PARA QUE CARGUE DARIO

                            // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                            page.GetComponent<GlobalButtonView>().ButtonNumber = itemButton.NumberOfPages;

                            //if (thisItemType.TypeButtonPaged.Count == 1)
                            if (itemButton.NumberOfPages == 1)
                            {

                                tempNumber = 1;

                                tempGameObject = itemButton.GetCurrentPage().transform.Find("GameObject");
                                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                // Cambia el color del selector segun el tipo
                                tempGameObject.GetComponent<Image>().color = reptiForm.TypeColor;

                            }

                            // Agrega el boton creada al listado de botones
                            reptiForm.ButtonList.Add(buttonPage);

                        }

                    }


                    break;
            }



            // Page customization fields and functions
            //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

            page.GetComponent<CellViewIMG>().hTextTitle = json_nameText;
            page.GetComponent<CellViewIMG>().hSubTitle = json_nameCientText;
            page.GetComponent<CellViewIMG>().imageBackground = json_imageBG;

            page.GetComponent<CellViewIMG>().mainText = json_TextA;
            page.GetComponent<CellViewIMG>().fullText = json_TextA;
            page.GetComponent<CellViewIMG>().subText_1 = json_TextB;
            page.GetComponent<CellViewIMG>().subText_2 = json_TextC;
            page.GetComponent<CellViewIMG>().subText_3 = json_TextD;

        
            // Color del boton mas segun el color de titulo
            page.GetComponent<CellViewIMG>().moreInfoContainer.color = json_titleColor;

            page.GetComponent<CellViewIMG>().titleColor.color = json_titleColor;
            

            page.GetComponent<CellViewIMG>().mainColorField.color = json_ColorA;
            page.GetComponent<CellViewIMG>().subColorField_1.color = json_ColorB;
            page.GetComponent<CellViewIMG>().subColorField_2.color = json_ColorC;
            page.GetComponent<CellViewIMG>().subColorField_3.color = json_ColorD;

            // No se va a usar pero Dejar para implementacion futura de colores de demas textos
            //page.GetComponent<CellViewIMG>().mainText.color = json_ColorTextA;
            //page.GetComponent<CellViewIMG>().fullText.color = json_ColorTextA;
            //page.GetComponent<CellViewIMG>().subText_1.color = json_ColorTextB;
            //page.GetComponent<CellViewIMG>().subText_2.color = json_ColorTextC;
            //page.GetComponent<CellViewIMG>().subText_3.color = json_ColorTextD;

            buttonPage.GetComponent<GlobalButtonView>().nombrePopular = json_nameText;
            buttonPage.GetComponent<GlobalButtonView>().nombreCientifico = json_nameCientText;



        }

        */
    }


    public void btn_CreatePage()
    {

        foreach (var thisItemType in tiposDeContenido)
        {

            var page = thisItemType.TypeFormPaged.AddPageUsingTemplate();
            page.PageTitle = "Number" + thisItemType.TypeFormPaged.NumberOfPages;

            // Update Pagination to update title and fields
            thisItemType.TypeFormPaged.UpdatePagination();


            // Agrega la pagina creada a la nueva lista de fichas
            thisItemType.FormList.Add(page);



            // Page customization fields and functions
            var textTitle = page.GetComponent<CellViewIMG>().hTextTitle;

            textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + thisItemType.TypeFormPaged.NumberOfPages;

            // Setea y obtiene el numero de pagina para esa ficha
            //var pageNumber = page.GetComponent<CellViewIMG>().pageNumber;

            //pageNumber = item.NumberOfPages;

            page.GetComponent<CellViewIMG>().PageNumber = thisItemType.TypeFormPaged.NumberOfPages;


            //print("CREATE PAGE NUMBER: " + page.GetComponent<CellViewIMG>().PageNumber);





            var pageButton = thisItemType.TypeButtonPaged.AddPageUsingTemplate();
            pageButton.PageTitle = "Number" + thisItemType.TypeButtonPaged.NumberOfPages;

            thisItemType.TypeButtonPaged.UpdatePagination();

            //if (thisItemType.TypeButtonPaged.Count == 1)
            if (thisItemType.TypeButtonPaged.NumberOfPages == 1)
            {

                thisItemType.TempNumber = 1;

                thisItemType.TempGameObject = thisItemType.TypeButtonPaged.GetCurrentPage().transform.Find("GameObject");
                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                // Cambia el color del selector segun el tipo
                thisItemType.TempGameObject.GetComponent<Image>().color = thisItemType.TypeColor;

            }

            // Agrega el boton creada al listado de botones
            thisItemType.ButtonList.Add(pageButton);


            var pageButtonCell = pageButton.GetComponent<GlobalButtonView>();

            //var buttonNumberField = pageButton.GetComponent<GlobalButtonView>().buttonNumber;



            //buttonNumberField = itemButton.NumberOfPages;

            pageButton.GetComponent<GlobalButtonView>().ButtonNumber = thisItemType.TypeButtonPaged.NumberOfPages;

            //print(pageButton.GetComponent<GlobalButtonView>().ButtonNumber);


            var textButton = pageButton.GetComponent<GlobalButtonView>().nombrePopular;
            textButton.GetComponent<TextMeshProUGUI>().text = "Nombre Numero: " + thisItemType.TypeButtonPaged.NumberOfPages;

            var textButtonAdic = pageButton.GetComponent<GlobalButtonView>().nombreCientifico;
            textButtonAdic.GetComponent<TextMeshProUGUI>().text = "Nombre Cientifico Nº: " + thisItemType.TypeButtonPaged.NumberOfPages;

            //int indexNumber = item.NumberOfPages;



            pageButton.GetComponentInChildren<Button>().onClick.AddListener(() => goToPage(pageButtonCell, pageButton.GetComponent<GlobalButtonView>().ButtonNumber));

            //print("NUMERO DE BOTON: º:" + pageButton.GetComponent<GlobalButtonView>().ButtonNumber);






            /*
            page = new Page[anfibioPaged.Count];
            TextMeshProUGUI[] textTitle = new TextMeshProUGUI[anfibioPaged.Count];


            for (int i = 0; i < anfibioPaged.Count; i++)
            {

                page[i] = anfibioPaged[i].AddPageUsingTemplate();
                anfibioPaged[i].UpdatePagination();

                textTitle[i] = page[i].GetComponent<CellViewIMG>().hTextTitle;

                textTitle[i].GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + anfibioPaged[i].NumberOfPages;
            }
            */


        }



    }

    public void goToPage(GlobalButtonView paginaBoton, int number)
    {
        /*
        int[] index = new int[anfibioPaged.Count];

        for (int i = 0; i < anfibioPaged.Count; i++)
        {        
            index[i] = anfibioPaged[i].GetPageNumber(page[i]);
            anfibioPaged[i].SetCurrentPage(index[i]);
            Debug.Log("GO TO PAGE NUMBER: " + index[i]);
        }
        */

        foreach (var thisItemType in tiposDeContenido)
        {

            //thisItemType.goingToPage = true;






            //foreach (var item in thisItemType.TypeFormPaged)
            //{


            //print("CURRENT PAGE: " + itemButton.CurrentPage + "  PAGINA SELECTED: " + number + " TEMPORAL NUMBER: " + tempNumber);


            //print("NUMERO TEMPORAL: " + tempNumber + " CURRENT PAGE: " + itemButton.CurrentPage);

            //if (itemButton.GetComponent<PagedRect_ScrollRect>().ResetDragOffset)
            //if(!isPressed)
            //{

            //if (!itemButton.GetComponent<PagedRect_ScrollRect>().isVerticalDragging && !item.GetComponent<PagedRect_ScrollRect>().isHorizontalDragging)
            //{

            if (thisItemType.TempNumber != thisItemType.TypeButtonPaged.CurrentPage)
            {
                //if (tempGameObject != null)
                //if (tempNumber != 0)
                //{
                thisItemType.TempGameObject.GetComponent<Image>().color = noSelectedColor;//new Color32(150, 166, 166, 255);
                                                                                          //print("Cambio de objeto _GRIS");
                                                                                          //}
                                                                                          /*
                                                                                          else
                                                                                          {
                                                                                              tempGameObject.GetComponent<Image>().color = new Color32(150, 166, 166, 255);
                                                                                              print("Cambio de objeto _GRIS INICIAL");
                                                                                          }
                                                                                          */
                                                                                          //ST// if(tempPage3 != null)
                                                                                          //tempPage3.GetComponent<CellViewIMG>().btn_ClickMinus();

            }


            if (number == thisItemType.TypeButtonPaged.CurrentPage)
            {

                // Habilita que pueda cambiar la pagina del formulario
                //thisItemType.IsJumping = false;

                //ST////////////// itemButton.SetCurrentPage(number);



                thisItemType.TempNumber = number;

                thisItemType.TempGameObject = paginaBoton.transform.Find("GameObject");

                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);
                // Cambia el color del selector segun el tipo
                thisItemType.TempGameObject.GetComponent<Image>().color = thisItemType.TypeColor;

                //isPressed = true;

                //print("Seleccion objeto _AMARILLO" + " NUMERO TEMPORAL: " + tempNumber);


                //foreach (var itemForm in thisItemType.TypeFormPaged)
                //{

                //tempPage3 = itemForm.GetCurrentPage().transform;

                //}


            }




            /*
            if (item.CurrentPage == paginaBoton.buttonNumber)
            {
                item.SetCurrentPage(number);
                // Remueve el Boton central de la lista para manipular demas
                //thisItemType.ButtonList.Remove(item.GetPageByNumber(item.CurrentPage));
                thisItemType.ButtonList.Remove(item.GetPageByNumber(paginaBoton.buttonNumber));

                // Cambiar el color del boton al seleccionar
                paginaBoton.transform.Find("GameObject").GetComponent<Image>().color = new Color32(255, 207, 1, 255);
                // TODO: CONTROLAR PRINT
                Debug.Log("GO TO BUTTON NUMBER: " + number);
            }
            else
            {
                // Cambiar el color del boton al seleccionar
                //paginaBoton.transform.Find("GameObject").GetComponent<Image>().color = new Color32(150, 166, 166, 255);
                //GameObject.FindGameObjectsWithTag("botonesGlobal"). GetComponent<Image>().color = new Color32(150, 166, 166, 255);

                // TODO: Hacer que cambie el color de las demas fichas que no esten centradas
                foreach (var itemButton in thisItemType.ButtonList)
                {

                    itemButton.transform.Find("GameObject").GetComponent<Image>().color = new Color32(150, 166, 166, 255);

                    Debug.Log("print color normal");


                }


            }
            */
            //}

            //}

            //}





            //if (!thisItemType.IsJumping)
            //{

            thisItemType.TypeButtonPaged.SetCurrentPage(number);

            //thisItemType.IsJumping = false;
            //}

        }








    }



    
    public void ChangeParallelPageNew(Vector2 value)
    {

        //////////////isMovingH = true;
        //isMovingV = false;

        /////////////if (!isMovingV)
        //////////{


            foreach (var thisItemType in tiposDeContenido)
            {

                

                    var currentPageActive = thisItemType.TypeFormPaged.CurrentPage;
                    //print("PAGINA ACTIVA + :" + currentPageActive);

                    if (thisItemType.TypeFormPaged.GetCurrentPage() != null && thisItemType.TypeFormPaged.GetComponent<PagedRect_ScrollRect>().isHorizontalDragging)
                    {

                        var number = thisItemType.TypeFormPaged.GetCurrentPage().gameObject.GetComponent<CellViewIMG>().PageNumber;


                            // Permite el dragging solo una vez que termino la animacion de entrada de la galeria
                            if (!thisItemType.TypeButtonPaged.GetComponent<PagedRect_ScrollRect>().isVerticalDragging && allowDragging)/// && !isPressed)
                            {
                                //ST//// itemButton.SetCurrentPage(itemButton.CurrentPage);


                                //print("CURRENT PAGE: " + itemButton.CurrentPage + "  PAGINA SELECTED: " + number + " TEMPORAL NUMBER: " + tempNumber);
                                //print("NUMERO TEMPORAL: " + tempNumber + " CURRENT PAGE: " + itemButton.CurrentPage);


                                //print("NUMBER OBTENIDO: " + number);


                                if (thisItemType.TempNumber != currentPageActive)
                                {
                                    //if (tempGameObject != null)
                                    if (thisItemType.TempNumber != 0)
                                    {
                                        //

                                        // ST // PARA HABILITAR AMBOS OPCIONES DE LISTENERS AL MISMO TIEMPO
                                        thisItemType.TempGameObject.GetComponent<Image>().color = noSelectedColor;//new Color32(150, 166, 166, 255);
                                        //print("Cambio de objeto _GRIS");

                                        // ST
                                        thisItemType.TempPage.GetComponent<CellViewIMG>().runtime_ClickMinus();
                                        print("close form expand content");
                                    }
                                }

                                if (number == currentPageActive)
                                {
                                //if (number == itemButton.CurrentPage)
                                //{
                                //itemButton.SetCurrentPage(number);

                                // ST
                                //if(itemForm.GetComponent<PagedRect_ScrollRect>().ResetDragOffset == true)
                                //{ 
                                //if(itemForm.GetComponent<PagedRect_ScrollRect>().onValueChanged != null)
                                //itemForm.GetComponent<PagedRect_ScrollRect>().onValueChanged = null;
                                //if(!itemForm.GetComponent<PagedRect_ScrollRect>().isBeingDragged)

                                // TODO: Inhabilitar con el drag contrario
                                //ST// if(!itemForm.GetComponent<PagedRect_ScrollRect>().isBeingDragged && itemForm.GetComponent<PagedRect_ScrollRect>().ResetDragOffset)

                                if (thisItemType.TypeButtonPaged.isActiveAndEnabled)
                                {

                                    thisItemType.TypeButtonPaged.SetCurrentPage(currentPageActive);


                                    thisItemType.TempNumber = number;



                                    thisItemType.TempGameObject = thisItemType.TypeButtonPaged.GetCurrentPage().transform.Find("GameObject");


                                    thisItemType.TempPage = thisItemType.TypeFormPaged.GetCurrentPage().transform;

                                    //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                    // Cambia el color del selector segun el tipo
                                    thisItemType.TempGameObject.GetComponent<Image>().color = thisItemType.TypeColor;


                                    //isMovingH = false;

                                    //print("MOVING HORIZONTAL: " + isMovingH);

                                    //print("Seleccion objeto _AMARILLO" + " NUMERO TEMPORAL: " + tempNumber);

                                    //}
                                    //}

                                }

                                }

                        

                        }

                    }
                



            /////////}

        }
    }


    public void ChangeParallelButtonNew(Vector2 value)
    {

        //var number = 0;

        ////////////isMovingV = true;
        //isMovingH = false;

        ///////////if (!isMovingH)
        ///////////////{

            foreach (var thisItemType in tiposDeContenido)
            {

                

                    var currentButtonActive = thisItemType.TypeButtonPaged.CurrentPage;
                    //print("BOTON ACTIVO + :" + currentButtonActive);

                    if (thisItemType.TypeButtonPaged.GetCurrentPage() != null)
                    {
                        var number = thisItemType.TypeButtonPaged.GetCurrentPage().gameObject.GetComponent<GlobalButtonView>().ButtonNumber;

                        
                            // Permite el dragging solo una vez que termino la animacion de entrada de la galeria
                            if (!thisItemType.TypeFormPaged.GetComponent<PagedRect_ScrollRect>().isHorizontalDragging && allowDragging)/// && !isPressed)
                            {



                                //print("NUMBER OBTENIDO: " + number);


                                if (thisItemType.TempNumber2 != currentButtonActive)
                                {
                                    //if (tempGameObject != null)
                                    if (thisItemType.TempNumber2 != 0)
                                    {
                                        thisItemType.TempGameObject2.GetComponent<Image>().color = noSelectedColor;//new Color32(150, 166, 166, 255);
                                                                                                                   //print("Cambio de objeto _GRIS");

                                        thisItemType.TempPage2.GetComponent<CellViewIMG>().runtime_ClickMinus();
                                        print("close form expand content");
                                    }
                                }

                                if (number == currentButtonActive)
                                {
                                //if (number == itemButton.CurrentPage)
                                //{
                                //itemButton.SetCurrentPage(number);


                                /// TODO - DETERMINAR COMO SETEAR PAGINA
                                /// 
                                //if(itemButton.GetComponent<PagedRect_ScrollRect>().onValueChanged != null)
                                //if(!itemButton.GetComponent<PagedRect_ScrollRect>().isBeingDragged)

                                //itemButton.GetComponent<PagedRect_ScrollRect>().onValueChanged = null;
                                //if(itemButton.GetComponent<PagedRect_ScrollRect>().onValueChanged != null)





                                //if(!itemButton.GetComponent<PagedRect_ScrollRect>().OnScroll() = null)

                                //ST// if(!itemButton.GetComponent<PagedRect_ScrollRect>().isBeingDragged && itemButton.GetComponent<PagedRect_ScrollRect>().ResetDragOffset)

                                if (thisItemType.TypeFormPaged.isActiveAndEnabled)
                                {
                                    thisItemType.TypeFormPaged.SetCurrentPage(currentButtonActive);


                                    thisItemType.TempNumber2 = number;

                                    thisItemType.TempGameObject2 = thisItemType.TypeButtonPaged.GetCurrentPage().transform.Find("GameObject");


                                    thisItemType.TempPage2 = thisItemType.TypeFormPaged.GetCurrentPage().transform;

                                    //tempGameObject2.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                                    // Color para ese tipo de galeria de imagenes
                                    thisItemType.TempGameObject2.GetComponent<Image>().color = thisItemType.TypeColor;


                                    //isMovingV = false;

                                    //print("MOVING VERTICAL: " + isMovingV);

                                    //print("Seleccion objeto _AMARILLO" + " NUMERO TEMPORAL: " + tempNumber2);


                                }
                                }

                            }

                        

                    }




                



            }

        ////////////}
        //return;

    }


   
    



}


