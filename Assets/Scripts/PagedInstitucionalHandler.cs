﻿using EnhancedScrollerDemos.CellControllerInstitucional;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI.Pagination;
using UnityEngine;

public class PagedInstitucionalHandler : MonoBehaviour
{


    /// <summary>
    /// Controlador principal para manejar todas las fichas de los 5 bloques de institucional
    /// </summary>
    /// 
    [Serializable]
    public class formulariosInsticionales
    {
        [SerializeField]
        [Header("Nombre del formulario de institucional")]
        private string nombreFormulario;
        public string NombreFormulario
        {
            get
            {
                return nombreFormulario;
            }
        }
        [SerializeField]
        [Header("Paged rect del formulario")]
        private PagedRect pagedFormulario;
        public PagedRect PagedFormulario
        {
            get
            {
                return pagedFormulario;
            }
        }

        [SerializeField]
        private int tempNumber;
        public int TempNumber
        {
            get
            {
                return tempNumber;
            }
            set
            {
                tempNumber = value;
            }
        }
        [SerializeField]
        private Transform tempGameObject;
        public Transform TempGameObject
        {
            get
            {
                return tempGameObject;
            }
            set
            {
                tempGameObject = value;
            }
        }

    }


    [Header("Cantidad de Diferentes Tipos de institucionales")]
    [SerializeField]
    public List<formulariosInsticionales> formulariosInstitucionalesVarios;


    

    /*
    [Header("Paged Rect Respectivo del bloque Tecnologia")]
    public PagedRect tecnologiaPaged;

    [Header("Paged Rect Respectivo del bloque Educacion")]
    public PagedRect educacionPaged;

    [Header("Paged Rect Respectivo del bloque El Acuario")]
    public PagedRect acuarioPaged;

    [Header("Paged Rect Respectivo del bloque Laboratorio")]
    public PagedRect laboratorioPaged;

    [Header("Paged Rect Respectivo del bloque Trabajo con la Comunidad")]
    public PagedRect comunidadPaged;

    [Header("Paged Rect Respectivo del bloque Parque Autóctono")]
    public PagedRect parquePaged;
    */

    /// <summary>
    /// The list of inventory data
    /// </summary>
    [HideInInspector]
    public List<InstitucionalData> _data;


    private Page page;

    /// <summary>
    /// Flag de control para institucional handler
    /// </summary>
    [HideInInspector]
    public bool feedInstitucionalLoaded = false;


    private static PagedInstitucionalHandler instance;
    public static PagedInstitucionalHandler Instance
    {
        get
        {
            return instance;
        }
    }



    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);

        DontDestroyOnLoad(this);
    }


    void OnEnable()
    {


        foreach (var itemType in formulariosInstitucionalesVarios)
        {
            
                itemType.PagedFormulario.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(ChangeFormPage);
                print("Added institucional listener");
           
        }

    }



    public void ChangeFormPage(Vector2 value)
    {

        //////////////isMovingH = true;
        //isMovingV = false;

        /////////////if (!isMovingV)
        //////////{

        print("moving form");

        foreach (var thisItemType in formulariosInstitucionalesVarios)
        {


                var currentPageActive = thisItemType.PagedFormulario.CurrentPage;
                //var currentPageActive = 1;

                if (thisItemType.PagedFormulario.GetCurrentPage() != null)
                {

                    var number = thisItemType.PagedFormulario.GetCurrentPage().gameObject.GetComponent<CellViewInstitucional>().PageNumber;


                  

                            if (thisItemType.TempNumber != currentPageActive)
                            {
                                //if (tempGameObject != null)
                                if (thisItemType.TempNumber != 0)
                                {
                                    //

                                    // ST // PARA HABILITAR AMBOS OPCIONES DE LISTENERS AL MISMO TIEMPO
                                    //tempGameObject.GetComponent<Image>().color = noSelectedColor;
                                    print("close form expand content");
                                    thisItemType.TempGameObject.GetComponent<CellViewInstitucional>().btn_ClickMinus();
                                    
                                }
                            }

                            if (number == currentPageActive)
                            {

                                //thisItemType.PagedFormulario.SetCurrentPage(currentPageActive);


                                thisItemType.TempNumber = number;


                                thisItemType.TempGameObject = thisItemType.PagedFormulario.GetCurrentPage().transform;


                                print("detect current page");




                            }

                }

                    

        }
            

        
    }
    



    //public IEnumerator SetInstitucionalData(List<InstitucionalData> instData)
	public void SetInstitucionalData(List<InstitucionalData> instData)
    {
        _data = instData;
		Debug.Log ("Setup Institucional Data in PAGED");
        /////////////////
        ///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
        /////////////////
        ///// DESCOMENTAR

        
        // Recorre todos los items de la DB para instanciar las paginas
        for (int i = 0; i < _data.Count; i++)
        {
			//string searchType = _data.FindAll(x => x.type);
			Debug.Log (string.Format ("Data Type {0}.", _data[i].type));
			switch (_data[i].type)
            {
                case "tecnologia":
                    var tecnoForm = formulariosInstitucionalesVarios.Find(x => x.NombreFormulario == "Tecnologia");

                    page = tecnoForm.PagedFormulario.AddPageUsingTemplate();

                    page.PageTitle = "Number" + tecnoForm.PagedFormulario.NumberOfPages;

                    // Update Pagination to update title and fields
                    tecnoForm.PagedFormulario.UpdatePagination();



                    //textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + tecnoForm.PagedFormulario.NumberOfPages;

                    ////// DATA PARA QUE CARGUE DARIO

                    // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                    page.GetComponent<CellViewInstitucional>().PageNumber = tecnoForm.PagedFormulario.NumberOfPages;


                    break;
                
                case "educacion":

                    var educForm = formulariosInstitucionalesVarios.Find(x => x.NombreFormulario == "Educacion");

                    page = educForm.PagedFormulario.AddPageUsingTemplate();

                    page.PageTitle = "Number" + educForm.PagedFormulario.NumberOfPages;

                    // Update Pagination to update title and fields
                    educForm.PagedFormulario.UpdatePagination();



                    //textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + tecnoForm.PagedFormulario.NumberOfPages;

                    ////// DATA PARA QUE CARGUE DARIO

                    // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                    page.GetComponent<CellViewInstitucional>().PageNumber = educForm.PagedFormulario.NumberOfPages;


                    break;

                case "acuario":

                    var acuaForm = formulariosInstitucionalesVarios.Find(x => x.NombreFormulario == "Acuario");

                    page = acuaForm.PagedFormulario.AddPageUsingTemplate();

                    page.PageTitle = "Number" + acuaForm.PagedFormulario.NumberOfPages;

                    // Update Pagination to update title and fields
                    acuaForm.PagedFormulario.UpdatePagination();



                    //textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + tecnoForm.PagedFormulario.NumberOfPages;

                    ////// DATA PARA QUE CARGUE DARIO

                    // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                    page.GetComponent<CellViewInstitucional>().PageNumber = acuaForm.PagedFormulario.NumberOfPages;


                    break;

                case "laboratorio":

                    var laboForm = formulariosInstitucionalesVarios.Find(x => x.NombreFormulario == "Laboratorio");

                    page = laboForm.PagedFormulario.AddPageUsingTemplate();

                    page.PageTitle = "Number" + laboForm.PagedFormulario.NumberOfPages;

                    // Update Pagination to update title and fields
                    laboForm.PagedFormulario.UpdatePagination();



                    //textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + tecnoForm.PagedFormulario.NumberOfPages;

                    ////// DATA PARA QUE CARGUE DARIO

                    // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                    page.GetComponent<CellViewInstitucional>().PageNumber = laboForm.PagedFormulario.NumberOfPages;


                    break;

                case "comunidad":

                    var comuForm = formulariosInstitucionalesVarios.Find(x => x.NombreFormulario == "Comunidad");

                    page = comuForm.PagedFormulario.AddPageUsingTemplate();

                    page.PageTitle = "Number" + comuForm.PagedFormulario.NumberOfPages;

                    // Update Pagination to update title and fields
                    comuForm.PagedFormulario.UpdatePagination();



                    //textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + tecnoForm.PagedFormulario.NumberOfPages;

                    ////// DATA PARA QUE CARGUE DARIO

                    // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                    page.GetComponent<CellViewInstitucional>().PageNumber = comuForm.PagedFormulario.NumberOfPages;


                    break;

                case "parque":

                    var parqueForm = formulariosInstitucionalesVarios.Find(x => x.NombreFormulario == "Parque");

                    page = parqueForm.PagedFormulario.AddPageUsingTemplate();

                    page.PageTitle = "Number" + parqueForm.PagedFormulario.NumberOfPages;

                    // Update Pagination to update title and fields
                    parqueForm.PagedFormulario.UpdatePagination();



                    //textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + tecnoForm.PagedFormulario.NumberOfPages;

                    ////// DATA PARA QUE CARGUE DARIO

                    // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                    page.GetComponent<CellViewInstitucional>().PageNumber = parqueForm.PagedFormulario.NumberOfPages;


                    break;
                    
            }



	        // Page customization fields and functions
	        //var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

			page.GetComponent<CellViewInstitucional>().imageBackground.sprite = _data[i].sprite;  //json_imageBG_id;
			page.GetComponent<CellViewInstitucional>().textTitle.SetText(_data[i].titleName); //json_titleText;
			page.GetComponent<CellViewInstitucional>().mainText.SetText(_data[i].mainDescription); //json_mainText;
			page.GetComponent<CellViewInstitucional>().fullText.SetText(_data[i].mainDescription); //json_mainText;

            page.GetComponent<CellViewInstitucional>().subText.SetText(_data[i].subDescription);

            // Color del boton mas segun el color del titulo
            page.GetComponent<CellViewInstitucional>().moreInfoButtonContainer.color = _data[i].titleColor; //json_titleColor;

			page.GetComponent<CellViewInstitucional> ().titleColor.color = _data [i].titleColor;//json_titleColor;
			page.GetComponent<CellViewInstitucional>().mainColorField.color = _data [i].mainColor;//json_mainColorBack;
			page.GetComponent<CellViewInstitucional>().subColorField.color = _data [i].subColor;//json_bottomColor;

	        // Main Color representa el color del texto principal
			page.GetComponent<CellViewInstitucional>().mainText.color = _data [i].mainColorBack;//json_mainColor;

	        // Dejar para implementacion futura de colores de demas textos
	        //page.GetComponent<CellViewInstitucional>().textTitle = json_titleText;
	        //page.GetComponent<CellViewInstitucional>().fullText = json_mainText;

			page.GetComponent<CellViewInstitucional>().moreInfoPanel.color = _data [i].mainColor; //json_mainColorBack;
        
            // Deshabilita el boton de + si el tamaño del texto es menor a 300 caracteres
            if(_data[i].mainDescription.Length < 300)
            {
                page.GetComponent<CellViewInstitucional>().moreInfoButtonContainer.gameObject.SetActive(false);
            }
            
    	}

        //yield return null;
    }

    public void btn_CreatePage()
    {

        foreach (var item in formulariosInstitucionalesVarios)
        {
            var page = item.PagedFormulario.AddPageUsingTemplate();
            page.PageTitle = "Number" + item.PagedFormulario.NumberOfPages;

            // Update Pagination to update title and fields
            item.PagedFormulario.UpdatePagination();

            // Page customization fields and functions

            var textTitle = page.GetComponent<CellViewInstitucional>().textTitle;

            textTitle.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + item.PagedFormulario.NumberOfPages;

            page.GetComponent<CellViewInstitucional>().PageNumber = item.PagedFormulario.NumberOfPages;
        }

       
    }

    private void Start()
    {
        StartCoroutine(Init());
    }

    public IEnumerator Init()
    {
        if(ContentHandler.Instance != null)
        yield return new WaitUntil(() => ContentHandler.Instance.isLoaded == true);

        // if the data existed previously, loop through
        // and remove the selection change handlers before
        // clearing out the data.
        if (_data != null)
        {
            for (var i = 0; i < _data.Count; i++)
            {
                _data[i].selectedChanged = null;
            }
        }

        // set up a new inventory list
        //_data = new List<InstitucionalData>();

        print("WAIT FINISHED");

        // ACA IRIA LA FUNCION "SetInstitucionalData" - ESPERA A QUE SE COMPLETE PARA CONTINUAR
        //yield return StartCoroutine(SetInstitucionalData(_data));

       

        // Cuando cargue toda la data vuelve true ese flag
        feedInstitucionalLoaded = true;

        yield return null;

    }

}
