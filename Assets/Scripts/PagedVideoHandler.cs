﻿using System;
using System.Collections;
using System.Collections.Generic;
using UI.Pagination;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PagedVideoHandler : MonoBehaviour
{
    [Header("Color del texto del video Seleccionado")]
    [SerializeField]
    public Color32 selectionText;

    [Header("Color del texto del video NO Seleccionado")]
    [SerializeField]
    public Color32 noSelectionText;

    [Header("Color del video Seleccionado")]
    [SerializeField]
    public Color32 selectionColor;

    [Header("Color del video NO Seleccionado")]
    [SerializeField]
    public Color32 noSelectionColor;


    [HideInInspector]
    public List<videoFormView> _data;

    [HideInInspector]
    public SelectorVideoHelper selectorVideo;

    [HideInInspector]
    public bool allowDragging = false;


    // 
    /// <summary>
    /// Flag para determinar que todas las fichas de video se crearon correctamente 
    /// </summary>
    [HideInInspector]
    public bool feedVideosLoaded = false;

    [HideInInspector]
	public List<VideoData> videoData;
	private bool videoDataLoaded = false;


    [Serializable]
    public class formulariosVideos
    {


        [SerializeField]
        [Header("Paged Rect Respectivo del bloque video | Lista para las galerias de videos")]
        private PagedRect videoPaged;
        public PagedRect VideoPaged
        {
            get
            {
                return videoPaged;
            }
        }

        [SerializeField]
        [Header("Lista de todos los objetos que tengan integrado en componente de video player")]
        private videoPlayerController videoPlayer;
        public videoPlayerController VideoPlayer
        {
            get
            {
                return videoPlayer;
            }
        }


        [Header("Paginas de todos los videos invocados en runtime")]
        [SerializeField]
        private List<Page> videoPages = new List<Page>();
        public List<Page> VideoPages
        {
            get
            {
                return videoPages;
            }
        }



        private int tempNumber;
        public int TempNumber
        {
            get
            {
                return tempNumber;
            }
            set
            {
                tempNumber = value;
            }
        }

        private Transform tempObject;
        public Transform TempObject
        {
            get
            {
                return tempObject;
            }
            set
            {
                tempObject = value;
            }
        }


    }


    [Header("Cantidad de Diferentes Tipos de Galerias de Videos")]
    [SerializeField]
    public List<formulariosVideos> formulariosDeVideos;

    
    
    
    private static PagedVideoHandler instance;
    public static PagedVideoHandler Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);
    }
    

    // Use this for initialization
    void Start()
    {
        // Sacar para testeo
        StartCoroutine(Init());
    }

    // Update is called once per frame
    void Update()
    {

    }


    public IEnumerator Init()
    {
		while (!videoDataLoaded) {
			yield return null;
		}

		Debug.Log ("Datos cargados en PagedVideoHandler");

		for (int i = 0; i < videoData.Count; i++) {
			
			foreach (var item in formulariosDeVideos) {
				var page = item.VideoPaged.AddPageUsingTemplate ();
				page.PageTitle = "Number" + item.VideoPaged.NumberOfPages;


				// Update Pagination to update title and fields
				item.VideoPaged.UpdatePagination ();


                // Agregar paginas a lista en el reproductor
                item.VideoPlayer.VideoPages.Add(page);

				// TODO: Implementar random de paginas
				//int randomInit = UnityEngine.Random.Range(1 , ) 

                //ST PASAR POR SELECTOR
                /*
                item.TempNumber = 1;

                item.TempObject = item.VideoPaged.GetCurrentPage().transform.Find("GameObject/Nombre");
                item.TempObject.GetComponent<Image>().color = selectionColor;//new Color32(255, 207, 1, 255);
                item.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = selectionText;
                */


                // Setea el Video Inicial al principio

                //item.VideoPlayer.init_State();




                // Toda la data de cada ficha de video que se carga por medio del CMS Handler

                page.GetComponent<videoFormView> ().VideoNumber = item.VideoPaged.NumberOfPages;

				/////////////////
				///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR EL FORMULARIO DE CADA VIDEO EN EL FOREACH
				/////////////////
				///// DESCOMENTAR
				page.GetComponent<videoFormView>().TextoCaratulaDelVideo.text = videoData[i].titleDescText; //variable_titulo_texto_bloque.count; // Misma variable arriba y abajo
				page.GetComponent<videoFormView>().TituloTextoBloque = videoData[i].titleDescText; // Misma variable arriba
				page.GetComponent<videoFormView>().TextoDelBloque = videoData[i].descText; //variable_descripcion_json.count;
				page.GetComponent<videoFormView>().UlrPath = videoData[i].videoURL; //variable_de_url_del_json.count;

                //print("URL DE VIDEOS: " + videoData[i].videoURL);
                page.GetComponent<videoFormView>().VideoTags = videoData[i].tags.Split(',').ToList();

                //page.GetComponent<videoFormView>().TagList = videoData[i].tags; //variable_de_tags_para_ese_video.count;
				page.GetComponent<videoFormView>().TextoDireccion = videoData[i].textDireccion; //variable_direccion_json.count;
				page.GetComponent<videoFormView>().TextoGuion = videoData[i].textGuion; // variable_guion_json.count;
				page.GetComponent<videoFormView>().TextoProduccion = videoData[i].textProduccion; //variable_produccion_json.count;
				page.GetComponent<videoFormView>().MiniBG.sprite = videoData[i].miniBGSprite; //variable_minibg_json.count;
				page.GetComponent<videoFormView>().FullBG = videoData[i].fullBGSprite; //variable_fullbg_json.count;
			}

            selectorVideo = this.gameObject.GetComponent<SelectorVideoHelper>();

            yield return StartCoroutine(selectorVideo.Init());

            SetInPlayer();


            /*yield return StartCoroutine (SetVideoDataOLD ());


			feedVideosLoaded = true;

			yield return null;*/
        }
    }

	public void SetVideoData(List<VideoData> _videoData) {
		videoData = _videoData;
		videoDataLoaded = true;
	}

    public void btn_CreatePage()
    {
        foreach (var itemBlock in formulariosDeVideos)
        {

            
                var page = itemBlock.VideoPaged.AddPageUsingTemplate();
                page.PageTitle = "Number" + itemBlock.VideoPaged.NumberOfPages;


                // Update Pagination to update title and fields
                itemBlock.VideoPaged.UpdatePagination();

                

                //formulariosDeVideos.vi videoPages.Add(page);
                itemBlock.VideoPages.Add(page);


                if (itemBlock.VideoPaged.NumberOfPages == 1)
                    {

                        itemBlock.TempNumber = 1;

                        itemBlock.TempObject = itemBlock.VideoPaged.GetCurrentPage().transform.Find("GameObject/Nombre");
                        itemBlock.TempObject.GetComponent<Image>().color = selectionColor;//new Color32(255, 207, 1, 255);
                        itemBlock.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = selectionText;

                    }


                page.GetComponent<videoFormView>().VideoNumber = itemBlock.VideoPaged.NumberOfPages;

                var pageVideoCell = page.GetComponent<videoFormView>();
                
                page.GetComponentInChildren<Button>().onClick.AddListener(() => goToPage(pageVideoCell, page.GetComponent<videoFormView>().VideoNumber));


                //this.gameObject.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(ChangeParallelPageVideo);

        }
       
    }


    public void btn_DeletePage()
    {
        foreach (var item in formulariosDeVideos)
        {
            item.VideoPaged.RemoveAllPages();
        }
    }


    void goToPage(videoFormView videoBoton, int number)
    {

        foreach (var thisItemType in formulariosDeVideos)
        {

                if(thisItemType.VideoPlayer.isVideoPlaying == true)
                {
                    thisItemType.VideoPlayer.init_State();
                }


                thisItemType.VideoPaged.SetCurrentPage(number);
                //Debug.Log("GO TO PAGE NUMBER: " + number);
            
                
            


                if (thisItemType.TempNumber != thisItemType.VideoPaged.CurrentPage)
                {

                    //thisItemType.TempObject.GetComponent<Image>().color = noSelectionColor;
                    //thisItemType.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = noSelectionText;

                }


                if (number == thisItemType.VideoPaged.CurrentPage)
                {
                    thisItemType.VideoPaged.SetCurrentPage(number);

                    thisItemType.TempNumber = number;

                    thisItemType.TempObject = videoBoton.transform.Find("GameObject/Nombre");

                    thisItemType.TempObject.GetComponent<Image>().color = selectionColor;
                    thisItemType.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = selectionText;


                    thisItemType.VideoPlayer.init_State();


                    SetInPlayer();


                    // Seteo de todas las variables del player segun el video seleccionado
                    // Descomentar por Dario
                    /*
                    thisItemType.VideoPlayer.CurrentTitle = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TituloTextoBloque;
                    thisItemType.VideoPlayer.CurrentDescription = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoDelBloque;
                    thisItemType.VideoPlayer.CurrentFullBG = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().FullBG;
                    thisItemType.VideoPlayer.CurrentUrlPath = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().UlrPath;
                    thisItemType.VideoPlayer.CurrentGuion = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoGuion;
                    thisItemType.VideoPlayer.CurrentDireccion = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoDireccion;
                    thisItemType.VideoPlayer.CurrentProduccion = thisItemType.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoProduccion;
                    */

                }




                
            


        }

    }


    public void ChangeParallelPageVideo(Vector2 test)
    {
        // Permite el dragging solo una vez que termino la animacion de entrada de la galeria
        if (allowDragging)
        {

            foreach (var thisItemType in formulariosDeVideos)
            {

                if (thisItemType.VideoPlayer.isVideoPlaying == true)
                {
                    thisItemType.VideoPlayer.init_State();
                }

                var currentPageActive = thisItemType.VideoPaged.CurrentPage;
                print("PAGINA ACTIVA VIDEO + :" + currentPageActive);

                if (thisItemType.VideoPaged.GetCurrentPage() != null)
                {

                    var number = thisItemType.VideoPaged.GetCurrentPage().gameObject.GetComponent<videoFormView>().VideoNumber;


                    if (thisItemType.TempNumber != currentPageActive)
                    {
                        if (thisItemType.TempNumber != 0)
                        {


                            thisItemType.TempObject.GetComponent<Image>().color = noSelectionColor;
                            thisItemType.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = noSelectionText;

                            print("Cambio de objeto video a _GRIS");
                        }
                    }

                    if (number == currentPageActive)
                    {


                        thisItemType.TempNumber = number;

                        thisItemType.TempObject = thisItemType.VideoPaged.GetCurrentPage().transform.Find("GameObject/Nombre");

                        thisItemType.TempObject.GetComponent<Image>().color = selectionColor;

                        thisItemType.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = selectionText;


                        thisItemType.VideoPlayer.init_State();


                        SetInPlayer();




                    }

                }

            }

        }
    }



    public void SetInPlayer()
    {
        foreach (var item in formulariosDeVideos)
        {
            if (item != null && item.VideoPaged.GetCurrentPage() != null)
            {
                print("SET PLAYER");
                item.VideoPlayer.CurrentFullBG.sprite = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().FullBG;
                item.VideoPlayer.CurrentUrlPath = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().UlrPath;

                item.VideoPlayer.CurrentTitle.text = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoCaratulaDelVideo.text;
                item.VideoPlayer.CurrentDescription.text = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TituloTextoBloque;

                item.VideoPlayer.CurrentGuion.text = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoGuion;
                item.VideoPlayer.CurrentDireccion.text = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoDireccion;
                item.VideoPlayer.CurrentProduccion.text = item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().TextoProduccion;
            }
            else
            {
                return;
            }
        }
    }

    
}
