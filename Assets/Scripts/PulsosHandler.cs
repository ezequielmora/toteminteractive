﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsosHandler : MonoBehaviour
{

    [Header("Controlador de informacion del slider")]
    public SliderController controladorDeInformacion;

	private bool inunSequiaDataLoaded = false;

	private List<InunSequiaData> inunSequiaData;

	// Use this for initialization
	void Start()
	{
		// Sacar para testeo
		StartCoroutine(Init());
	}

	private static PulsosHandler instance;
	public static PulsosHandler Instance
	{
		get
		{
			return instance;
		}
	}

	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this);
		}

		DontDestroyOnLoad(this);
	}


	public IEnumerator Init()
	{
		while (!inunSequiaDataLoaded) {
			yield return null;
		}
		/////////////////
		///// ACA DARIO DEBE INSERTAR DEL LADO DERECHO DE LA FUNCION LOS ELEMENTOS DE JSON PARA ALIMENTAR INUNDACIONES Y SEQUIA
		///// http://192.168.1.8/api/v1/totemInundSeq
		///// EN DARI USAR LA IP Y PUERTO PUBLICO
		/////////////////
		///// DESCOMENTAR


		// Recorre todos los items de la DB para instanciar 
		for (int i = 0; i < inunSequiaData.Count; i++)
		{
			Debug.Log(string.Format("Data Type {0}.", inunSequiaData[i].status));
			switch (inunSequiaData[i].status)
			{
			case "alto":

				controladorDeInformacion.gameObject.GetComponent<SliderController> ().cacheAltoTitulo = inunSequiaData [i].generalTitle; //json_generalTitle;

				controladorDeInformacion.gameObject.GetComponent<SliderController>().cacheAltoDesc = inunSequiaData [i].generalText; //json_generalText;


				break;

			case "medio":

				controladorDeInformacion.gameObject.GetComponent<SliderController>().cacheMedioTitulo = inunSequiaData [i].generalTitle; //json_generalTitle;

				controladorDeInformacion.gameObject.GetComponent<SliderController>().cacheMedioDesc = inunSequiaData [i].generalText; //json_generalText;


				break;

			case "bajo":

				controladorDeInformacion.gameObject.GetComponent<SliderController>().cacheBajoTitulo = inunSequiaData [i].generalTitle; //json_generalTitle;

				controladorDeInformacion.gameObject.GetComponent<SliderController>().cacheBajoDesc = inunSequiaData [i].generalText; //json_generalText;


				break;

			}


		}

        //Setea el estado medio al principio
        controladorDeInformacion.CampoTitulo.text = controladorDeInformacion.cacheMedioTitulo;
        controladorDeInformacion.CampoDescripcion.text = controladorDeInformacion.cacheMedioDesc;
        
    }
    
    public void SetPulsosData(List<InunSequiaData> _inunSequiaData)
    {
        inunSequiaData = _inunSequiaData;
		inunSequiaDataLoaded = true;
    }

}
