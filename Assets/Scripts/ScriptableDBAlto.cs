﻿using UnityEngine;
using System.Collections;

[System.Serializable]                                                           //  Our Representation of an InventoryItem
public class ScriptableDBAlto
{
    public string juncos;
    public string cumuloHuevos;
    public string arboles;
    public string raton;
    public string serpientes;
    public string pecesGrandes;
    public string pecesRojos;
    public string pecesAmarillos;
    public string carpincho;
    public string pajaroAzul;
    public string garza;
}