﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptableDBAltoList : ScriptableObject
{
    public List<ScriptableDBAlto> altoList;
}