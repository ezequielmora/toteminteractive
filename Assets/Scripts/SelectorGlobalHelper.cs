﻿using EnhancedScrollerDemos.CellControllerInstitucional;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI.Pagination;
using UnityEngine;
using UnityEngine.UI;

public class SelectorGlobalHelper : MonoBehaviour
{

    //private PagedVideoHandler myVideoHandler;

    //private List<PagedRect> myPageRectList;

    //[HideInInspector]
    //public bool isLoaded = false;
    private PagedGlobalHandler pagedGlobal;


    private static SelectorGlobalHelper instance;
    public static SelectorGlobalHelper Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start()
    {

        //ST 
        //StartCoroutine(Init());

        pagedGlobal = this.gameObject.GetComponent<PagedGlobalHandler>();

    }

    // Update is called once per frame
    void Update()
    {

    }


    public IEnumerator Init()
    {

        if (ContentHandler.Instance != null)
        {
            yield return new WaitUntil(() => ContentHandler.Instance.isLoaded == true);
        }

        print("cargado selector global helper");

        


        yield return StartCoroutine(SetNumber());


        yield return StartCoroutine(AddListeners());

        //ST
        //yield return StartCoroutine(DisableAllColor());
        //ST
        //yield return StartCoroutine(SelectorCreation());


        yield return null;
    }



    public IEnumerator SelectorCreation()
    {

        //
        /*
        foreach (var itemClass in pagedGlobal.tiposDeContenido)
        {

            var floraForm = pagedGlobal.tiposDeContenido.Find(x => x.TypeName == "Flora");


            foreach (var item in floraForm.TypeFormPaged)
            {
                
            }

        
        */
        //



        foreach (var item in pagedGlobal.tiposDeContenido)
        {
            



                item.TempPage = item.TypeFormPaged.GetCurrentPage().transform;


                //if (thisItemType.TypeButtonPaged.Count == 1)
                //if (itemButton.NumberOfPages == 1)
                //{

                //item.TempGameObject

                //item.TempPage




                item.TempGameObject = item.TypeButtonPaged.GetCurrentPage().transform.Find("GameObject");




                //thisItemType.TempPage = itemForm.GetCurrentPage().transform;




                item.TempNumber = 1;

                //item.TempGameObjectCache = itemButton.GetCurrentPage().transform.Find("GameObject");
                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                // Cambia el color inicial del selector segun el tipo
                item.TempGameObject.GetComponent<Image>().color = item.TypeColor;

                //}

                yield return null;

            


        }

    }


    IEnumerator SetNumber()
    {
        foreach (var item in pagedGlobal.tiposDeContenido)
        {

                // Setea el numero temporal al principio para evitar cambio de color
                //ST item.TempNumber = item.TypeButtonPaged.CurrentPage;


                foreach (var page in item.TypeFormPaged.Pages)
                {

                    page.GetComponent<CellViewIMG>().PageNumber = item.TypeFormPaged.GetPageNumber(page);
                    print("NUMERO DE VIDEO: " + page.GetComponent<CellViewIMG>().PageNumber);
                    
                    
                    
                    yield return null;
                }

            
        }
    }



    IEnumerator AddListeners()
    {
        foreach (var itemType in pagedGlobal.tiposDeContenido)
        {

                itemType.TypeFormPaged.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(pagedGlobal.ChangeParallelPageNew);
                print("Added horizontal listener");




                itemType.TypeButtonPaged.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(pagedGlobal.ChangeParallelButtonNew);
                print("Added vertical listener");

                //TODO 
                //itemButton.GetComponent<PagedRect_ScrollRect>().DisableDragging = false;
                foreach (var buttonPage in itemType.TypeButtonPaged.Pages)
                {

                    Page pageButtonTemp = buttonPage;

                    var pageButtonCell = buttonPage.GetComponent<GlobalButtonView>();


                    buttonPage.GetComponentInChildren<Button>().onClick.AddListener(() => goToPage(pageButtonCell, pageButtonTemp.GetComponent<GlobalButtonView>().ButtonNumber));

                }

            
        }

        yield return null;

    }   



    public IEnumerator DisableAllColor()
    {
        foreach (var item in pagedGlobal.tiposDeContenido)
        {

            


                item.TempGameObject = item.TypeButtonPaged.GetCurrentPage().transform.Find("GameObject");
                //tempGameObject.GetComponent<Image>().color = selectedColor;//new Color32(255, 207, 1, 255);

                // Cambia el color del selector segun el tipo
                item.TempGameObject.GetComponent<Image>().color = pagedGlobal.noSelectedColor;


                yield return null;

            
        }
    }



    public void goToPage(GlobalButtonView paginaBoton, int number)
    {


        foreach (var thisItemType in pagedGlobal.tiposDeContenido)
        {





            



                if (thisItemType.TempNumber != thisItemType.TypeButtonPaged.CurrentPage)
                {

                    thisItemType.TempGameObject.GetComponent<Image>().color = pagedGlobal.noSelectedColor;

                }


                if (number == thisItemType.TypeButtonPaged.CurrentPage)
                {





                    thisItemType.TempNumber = number;



                // Cambia el color del selector segun el tipo 
                // Eliminar ya que define el color cuando cambia con el OnValueChanged - No es necesario aqui
                //thisItemType.TempGameObject = paginaBoton.transform.Find("GameObject");
                //thisItemType.TempGameObject.GetComponent<Image>().color = thisItemType.TypeColor;




            }




            if (thisItemType.TypeButtonPaged != null && thisItemType.TypeButtonPaged.isActiveAndEnabled)
            {
                thisItemType.TypeButtonPaged.SetCurrentPage(number);
            }



        }

    }


}
