﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI.Pagination;
using UnityEngine;
using UnityEngine.UI;

public class SelectorVideoHelper : MonoBehaviour {

    //private PagedVideoHandler myVideoHandler;

    //private List<PagedRect> myPageRectList;

    //[HideInInspector]
    //public bool isLoaded = false;

    private static SelectorVideoHelper instance;
    public static SelectorVideoHelper Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start () {

        //myVideoHandler = this.gameObject.GetComponent<PagedVideoHandler>();

        //myPageRectList = this.gameObject.GetComponent<PagedVideoHandler>()

        //StartCoroutine(Init());
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public IEnumerator Init()
    {

        //print("INIT VIDEO HELPER");

        //myVideoHandler = this.gameObject.GetComponent<PagedVideoHandler>();



        if (ContentHandler.Instance != null)
        {
            yield return new WaitUntil(() => ContentHandler.Instance.isLoaded == true);
        }

        print("cargado selector video helper");


        yield return StartCoroutine(SetNumber());


        //yield return StartCoroutine(ReplicatePages());


        yield return StartCoroutine(AddListeners());


        //yield return StartCoroutine(DisableAllColor());

        //yield return StartCoroutine(SelectorCreation());


        yield return null;
    }



    public IEnumerator SelectorCreation()
    {

        foreach (var item in PagedVideoHandler.Instance.formulariosDeVideos)
        {
            if (item.VideoPaged.GetCurrentPage() != null)
            {

                item.TempNumber = 1;

                item.VideoPaged.GetCurrentPage().GetComponent<videoFormView>().VideoNumber = 1;

                item.TempObject = item.VideoPaged.GetCurrentPage().transform.Find("GameObject/Nombre");
                item.TempObject.GetComponent<Image>().color = PagedVideoHandler.Instance.selectionColor;//new Color32(255, 207, 1, 255);
                item.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = PagedVideoHandler.Instance.selectionText;

                PagedVideoHandler.Instance.SetInPlayer();

                yield return null;
            }
        }

    }


    public IEnumerator SetNumber()
    {
        foreach (var item in PagedVideoHandler.Instance.formulariosDeVideos)
        {
            foreach (var itemPage in item.VideoPaged.Pages)
            {

                itemPage.GetComponent<videoFormView>().VideoNumber = item.VideoPaged.GetPageNumber(itemPage);
                print("NUMERO DE VIDEO: " + itemPage.GetComponent<videoFormView>().VideoNumber);


                yield return null;
            }
        }
    }



    IEnumerator AddListeners()
    {
        foreach (var item in PagedVideoHandler.Instance.formulariosDeVideos)
        {


            item.VideoPaged.GetComponent<PagedRect_ScrollRect>().onValueChanged.AddListener(PagedVideoHandler.Instance.ChangeParallelPageVideo);


            foreach (var itemPage in item.VideoPaged.Pages)
            {

                //itemPage.GetComponent<videoFormView>().VideoNumber = item.VideoPaged.GetPageNumber(itemPage);
                //print("NUMERO DE VIDEO: " + itemPage.GetComponent<videoFormView>().VideoNumber);

                var pageVideoCell = itemPage.GetComponent<videoFormView>();

                itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));

                //item.VideoPaged.UpdatePages();

                print("MOSTRAR NUMERO PAGINAS: " + item.VideoPaged.NumberOfPages);



                yield return null;

            }
        }
    }

    /*
    IEnumerator AddGotoListeners()
    {
        foreach (var itemPage in item.VideoPaged.Pages)
        {

            //itemPage.GetComponent<videoFormView>().VideoNumber = item.VideoPaged.GetPageNumber(itemPage);
            //print("NUMERO DE VIDEO: " + itemPage.GetComponent<videoFormView>().VideoNumber);

            var pageVideoCell = itemPage.GetComponent<videoFormView>();

            itemPage.GetComponentInChildren<Button>().onClick.AddListener(() => goToPage(pageVideoCell, itemPage.GetComponent<videoFormView>().VideoNumber));

            //item.VideoPaged.UpdatePages();

            print("MOSTRAR NUMERO PAGINAS: " + item.VideoPaged.NumberOfPages);



            yield return null;

        }
    }
    */

    public IEnumerator DisableAllColor()
    {
        foreach (var item in PagedVideoHandler.Instance.formulariosDeVideos)
        {

            foreach (var itemPage in item.VideoPaged.Pages)
            {


                item.TempObject = itemPage.transform.Find("GameObject/Nombre");

                item.TempObject.GetComponent<Image>().color = PagedVideoHandler.Instance.noSelectionColor;
                item.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = PagedVideoHandler.Instance.noSelectionText;


                yield return null;

            }
        }
    }



    IEnumerator ReplicatePages()
    {
        foreach (var item in PagedVideoHandler.Instance.formulariosDeVideos)
        {
            print("inicio instanciacion: " + item.VideoPaged.NumberOfPages);


            //if(item.VideoPaged != null && item.VideoPaged.gameObject.activeInHierarchy)
            //switch (item.VideoPaged.NumberOfPages)
            if (item.VideoPaged.NumberOfPages == 3)
            {
                //case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16:

                    for (int i = 1; i <= 3; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {


                            Page adicPage = Instantiate<Page>(item.VideoPaged.GetPageByNumber(i));
                            adicPage.transform.SetParent(item.VideoPaged.Viewport.transform);

                            print("instantiated nº time: " + i);

                            //var textTitleAdic = adicPage.GetComponent<CellViewInstitucional>().textTitle;

                            //textTitleAdic.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + item.PagedFormulario.NumberOfPages;

                            //this.AddPage(page);
                            item.VideoPaged.AddPage(adicPage);

                            item.VideoPaged.UpdatePagination();

                            adicPage.GetComponent<videoFormView>().VideoNumber = item.VideoPaged.NumberOfPages;


                            /*
                            var pageAdic = item.PagedFormulario.AddPageUsingTemplate();
                            pageAdic.PageTitle = "Number" + item.PagedFormulario.NumberOfPages;

                            // Update Pagination to update title and fields
                            item.PagedFormulario.UpdatePagination();

                            //var textTitleAdic = pageAdic.GetComponent<CellViewInstitucional>().textTitle;

                            //textTitleAdic.GetComponent<TextMeshProUGUI>().text = "Titulo Numero: " + item.PagedFormulario.NumberOfPages;

                            pageAdic.GetComponent<CellViewInstitucional>().PageNumber = item.PagedFormulario.NumberOfPages;
                            */

                            yield return null;

                        }





                    }

               //     break;

               // default:
               //     break;
            }
        }

    }


    public void goToPage(videoFormView videoBoton, int number)
    {

        foreach (var thisItemType in PagedVideoHandler.Instance.formulariosDeVideos)
        {

            if (thisItemType.VideoPaged.isActiveAndEnabled)
            {


                if (thisItemType.VideoPlayer.isVideoPlaying == true)
                {
                    thisItemType.VideoPlayer.init_State();
                }

                thisItemType.VideoPaged.SetCurrentPage(number);
                //Debug.Log("GO TO PAGE NUMBER: " + number);





                if (thisItemType.TempNumber != thisItemType.VideoPaged.CurrentPage)
                {

                    thisItemType.TempObject.GetComponent<Image>().color = PagedVideoHandler.Instance.noSelectionColor;
                    thisItemType.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = PagedVideoHandler.Instance.noSelectionText;

                }

                if (number == thisItemType.VideoPaged.CurrentPage)
                {
                    thisItemType.VideoPaged.SetCurrentPage(number);

                    thisItemType.TempNumber = number;

                    thisItemType.TempObject = videoBoton.transform.Find("GameObject/Nombre");

                    thisItemType.TempObject.GetComponent<Image>().color = PagedVideoHandler.Instance.selectionColor;
                    thisItemType.TempObject.GetComponentInChildren<TextMeshProUGUI>().color = PagedVideoHandler.Instance.selectionText;

                }







            }
        }

    }

}
