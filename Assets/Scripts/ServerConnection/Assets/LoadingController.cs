﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingController : MonoBehaviour {

	private static LoadingController instance;
	public static LoadingController Instance
	{
		get
		{ 
			return instance;
		}
	}

	[SerializeField]
	private GameObject noDataText;
	[SerializeField]
	private GameObject loadingTextObj;
	[SerializeField]
	private GameObject loadingSprite;
	[SerializeField]
	private GameObject loadingObject;

	[SerializeField]
	private Text loadingText;

	// Recarga la UI para recargar la DB
	public void ReloadUI()
	{
		loadingObject.SetActive(true);
	}


	void Awake()
	{
		instance = this;
	}


	public void UpdateLoadingInfo(string _text)
	{
		loadingText.text = _text;
	}

	public void LoadingDone(bool _dataBaseEmpty)
	{
		if (_dataBaseEmpty)
		{
			noDataText.SetActive (true);
			loadingTextObj.SetActive (false);
			//loadingSprite.SetActive (false);
		} 
		else
		{
			loadingObject.SetActive (false);
		}
	}
}
