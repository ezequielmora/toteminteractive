﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using SQL.Structs;
using EnhancedScrollerDemos.CellControllerInstitucional;
using EnhancedScrollerDemos.CellControllerIMG;
using EnhancedUI;

public class MediaController : MonoBehaviour {

	private string defaultImagesPath;
	private static MediaController instance;
	public static MediaController Instance
	{
		get
		{ 
			return instance;
		}
	}

	private List<MediaContent> mediaList;

	[SerializeField]
	private Sprite defaultSprite;

	public void Awake()
	{
		instance = this;
		defaultImagesPath = Application.dataPath;
		// Guarda data en mismo folder fix eze
		//defaultImagesPath = "";
	}

	public void SetMedia(List<MediaContent> _mediaList)
	{
		mediaList = _mediaList;
	}

	public void DeleteMedia(SQLMediaContent media)
	{
		Debug.Log(media.localPath);
		if (File.Exists(media.localPath))
		{
			File.Delete(media.localPath);    
		}
	}

	public IEnumerator CreateImage(SQLMediaContent media, System.Action<SQLMediaContent> callback) 
	{
		WWW www = null;

		yield return StartCoroutine(NetworkHandler.Instance.ResolveWWW(media.url, (result) =>
			{
				www = result;    
			}));


		/*
        Texture2D texture;
        if (www != null)
        {
            texture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.DXT1, false);    
            www.LoadImageIntoTexture(texture);
        }
        else
        {
            texture = defaultTexture;
        }

        byte[] textureBytes2 = texture.EncodeToPNG();
        string spritePath2 = defaultImagesPath + "/StreamingAssets/Images/" + img.id.ToString() + "_2.png";

        File.WriteAllBytes(spritePath2, textureBytes2);
        */
		//Debug.Log (www.error);
		Debug.Log (www);
		Sprite sprite;
		if (www == null)
		{
			sprite = defaultSprite;
			Debug.LogWarning(string.Format("Image ID: {0}, was not downloaded, default img created for this id.", media.id));
			yield return new WaitForSeconds(1);
		}
		else
		{
			sprite = Sprite.Create (www.texture, new Rect (0, 0, www.texture.width, www.texture.height), new Vector2 (0, 0));    
		}

		yield return sprite;

		string spritePath = defaultImagesPath + "/StreamingAssets/Media/" + media.id.ToString() + ".png";
		Debug.Log (spritePath);

		byte[]textureBytes = sprite.texture.EncodeToPNG();

		File.WriteAllBytes(spritePath, textureBytes);

		media.localPath = spritePath;

		// Chequeo si recibe bien el path de la imagen
		print("PATH DE LA IMAGEN EN MEMORIA: " + media.localPath);

		//TODO: Download img from url, create asset, get new assetPath save asset path in sqlimg return it.
		if (www != null)
		{
			www.Dispose();
		}

		callback(media);
	}

	public IEnumerator GetImagesFromAssets(List<InstitucionalData> institucionalData, List<VideoData> videoData, List<ImgData> imgData, System.Action<List<InstitucionalData>, List<VideoData>, List<ImgData>> callback)
	{

		foreach (MediaContent media in mediaList) {

			// Chequea si tenemos un path local
			if (!string.IsNullOrEmpty (media.localPath)) {
				Sprite newSprite = LoadNewSprite (media.localPath);
				yield return newSprite;
			

				foreach (InstitucionalData instData in institucionalData) {
					if (instData.imgId == media.id) {
						instData.spritePath = media.localPath;
						instData.sprite = newSprite;
					}
				}

				int imgDataBGIndex = imgData.FindIndex (item => item.imageBGId == media.id);
				if (imgDataBGIndex >= 0) {
					//videoFormViewData [videoMiniBGIndex].MiniBG.sprite = newSprite;
					imgData [imgDataBGIndex].imageBGSprite = newSprite;
				}


				// Imagenes para videoFormView
				int videoMiniBGIndex = videoData.FindIndex (item => item.miniBGId == media.id);
				if (videoMiniBGIndex >= 0) {
					//videoFormViewData [videoMiniBGIndex].MiniBG.sprite = newSprite;
					videoData [videoMiniBGIndex].miniBGSprite = newSprite;
				}

				int videoFullBGIndex = videoData.FindIndex (item => item.fullBGId == media.id);
				if (videoFullBGIndex >= 0) {
					videoData [videoFullBGIndex].fullBGSprite = newSprite;
				}


			} /*else {
				Debug.Log (string.Format ("No existe un path local para MEDIA ID: {0}.", media.id));
			}*/


			int videoUrlIndex = videoData.FindIndex (item => item.videoId == media.id);
			if (videoUrlIndex >= 0) {
				videoData [videoUrlIndex].videoURL = media.url;
			}

			/*
			int instIndex = institucionalData.FindIndex (item => item.imgId == img.id);
			if (instIndex >= 0) {
				institucionalData [instIndex].spritePath = img.imgLocalPath;
			}*/

			/*int fishIndex = fishes.FindIndex(item => item.imgId == img.id);
			if (fishIndex >= 0) 
				fishes[fishIndex].img = newSprite;

			int fishIndex2 = fishes.FindIndex(item => item.miniImgId == img.id);
			if (fishIndex2 >= 0)
				fishes[fishIndex2].miniImg = newSprite;

			int fishIndex3 = fishes.FindIndex(item => item.ImgIsInDetailedPoint(img.id));
			if (fishIndex3 >= 0)
			{
				fishes[fishIndex3].UpdateImgInDetailedPoint(img.id, newSprite);
			}

			int langIndex = langs.FindIndex(item => item.ImgId == img.id);
			if (langIndex >= 0)
				langs[langIndex].Img = newSprite;

			int iconIndex = icons.FindIndex(item => item.imgId == img.id);
			if (iconIndex >= 0)
				icons[iconIndex].img = newSprite;
			*/
		}

		callback(institucionalData, videoData, imgData);
	}

	public Sprite LoadNewSprite(string FilePath, float PixelsPerUnit = 100.0f) {

		// Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference


		//Debug Ezequiel para ver la ruta
		//print("RUTA DE LA IMAGE: " + FilePath);
		//print("RUTA DE APPLICATION DATA PATH: " + Application.dataPath);

		Sprite NewSprite = new Sprite();
		Texture2D SpriteTexture = LoadTexture(FilePath);
		NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0.5f, 0.5f), PixelsPerUnit);
		return NewSprite;
	}

	public Texture2D LoadTexture(string FilePath) {

		// Load a PNG or JPG file from disk to a Texture2D
		// Returns null if load fails

		Texture2D Tex2D;
		byte[] FileData;

		if (File.Exists(FilePath)){
			FileData = File.ReadAllBytes(FilePath);
			Tex2D = new Texture2D(2, 2);           // Create new "empty" texture
			if (Tex2D.LoadImage(FileData))           // Load the imagedata into the texture (size is set automatically)
				return Tex2D;                 // If data = readable -> return texture
		}  
		return null;                     // Return null if load failed
	}
}
