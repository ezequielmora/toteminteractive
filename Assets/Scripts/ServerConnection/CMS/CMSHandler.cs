﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WyrmTale;
using SQL.Structs;

namespace CMS
{
    public class CMSHandler : MonoBehaviour {

        private static CMSHandler instance;
        public static CMSHandler Instance
        {
            get
            { 
                return instance;
            }
        }

        [TextArea]
        [SerializeField]
        private string tempFishJson;

        [SerializeField]
        private bool useTempJson = false;

        private void Awake()
        {
            instance = this;
        }

        
        public IEnumerator GetImageList(System.Action<List<SQLImage>> callback)
        {

            Debug.Log("Getting all image data from server");


            string jsonString = string.Empty;

            yield return StartCoroutine(NetworkHandler.Instance.GetImagesJson((result) =>
                {
                    jsonString = result;    
                }));
            
            JSON json = new JSON();

            json.serialized = jsonString;

            int[] imgIds = json.ToArray<int>("ids");
            string[] hashes = json.ToArray<string>("hashes");
            string[] urls = json.ToArray<string>("urls");
			string[] mimeTypes = json.ToArray<string>("mime_types");

            List<SQLImage> imgs = new List<SQLImage>();

            for (int i = 0; i < imgIds.Length; i++)
            {
                string url = urls[i].Replace(@"\/", "/");
				string mimetype = mimeTypes [i];
				if (mimetype == "image/png" || mimetype == "image/jpeg") {
					Debug.Log (url);
					SQLImage img = new SQLImage (imgIds [i], hashes [i], url);
					imgs.Add (img);
				} else {
					Debug.Log ("Imagen excluida por mimetype " + imgIds [i]);
				}
            }

            if (callback != null)
                callback(imgs);
        }

		public IEnumerator GetMediaList(System.Action<List<SQLMediaContent>> callback)
		{

			Debug.Log("Getting all media data from server");


			string jsonString = string.Empty;

			yield return StartCoroutine(NetworkHandler.Instance.GetMediaJson((result) =>
				{
					jsonString = result;    
				}));

			JSON json = new JSON();

			json.serialized = jsonString;

			int[] ids = json.ToArray<int>("ids");
			string[] hashes = json.ToArray<string>("hashes");
			string[] urls = json.ToArray<string>("urls");
			string[] mimeTypes = json.ToArray<string>("mime_types");

			List<SQLMediaContent> mediaList = new List<SQLMediaContent>();

			for (int i = 0; i < ids.Length; i++)
			{
				string url = urls[i].Replace(@"\/", "/");
				string mimetype = mimeTypes [i];
				SQLMediaContent media = new SQLMediaContent (ids [i], hashes [i], url, mimetype);
				mediaList.Add (media);

			}

			if (callback != null)
				callback(mediaList);
		}

		public IEnumerator GetInsitucionalDataList(System.Action<List<SQLInstitucionalData>> callback)
		{

			Debug.Log("Getting all Institucional data from server");


			string jsonString = string.Empty;

			yield return StartCoroutine(NetworkHandler.Instance.GetInstitucionalDataJson((result) =>
				{
					jsonString = result;    
				}));

			JSON json = new JSON();

			json.serialized = jsonString;

			JSON[] jsonArray = json.ToArray<JSON>("fichaInstitucional");

			List<SQLInstitucionalData> instDataList = new List<SQLInstitucionalData>();


			Debug.Log ("Server INST DATA COUNT: " + jsonArray.Length);
			for (int i = 0; i < jsonArray.Length; i++)
			{
				//int id = jsonArray [i].ToInt ("id"); // REVISAR! no hay ID en el json
				int id = i + 1;
				Debug.Log ("ID: " + id);
				string hash = jsonArray [i].ToString ("hash"); 
				string type = jsonArray [i].ToString ("type");
				int enabled = jsonArray [i].ToInt ("enabled");
				int imageBG_id = jsonArray [i].ToInt ("imageBG_id");
				string titleText = jsonArray [i].ToString ("titleText");
				string titleColor = jsonArray [i].ToString ("titleColor");
				string bottomText = jsonArray [i].ToString ("bottomText");
				string bottomColor = jsonArray [i].ToString ("bottomColor");
				string mainText = jsonArray [i].ToString ("mainText");
				string mainColor = jsonArray [i].ToString ("mainColor");
				string mainColorBack = jsonArray [i].ToString ("mainColorBack");
				SQLInstitucionalData instData = new SQLInstitucionalData(id, hash, type, titleText, titleColor, mainText, mainColor, mainColorBack, mainText, bottomText, bottomColor, mainText, mainColor, mainColor, imageBG_id);
				instDataList.Add(instData);
			}

			if (callback != null)
				callback(instDataList);
		}

		public IEnumerator GetInunSequiaDataList(System.Action<List<SQLInunSequiaData>> callback)
		{

			Debug.Log("Getting all InunSequia data from server");


			string jsonString = string.Empty;

			yield return StartCoroutine(NetworkHandler.Instance.GetInunSequiaDataJson((result) =>
				{
					jsonString = result;    
				}));

			JSON json = new JSON();

			json.serialized = jsonString;

			JSON[] jsonArray = json.ToArray<JSON>("inundacionesSequia");

			List<SQLInunSequiaData> inunSequiaDataList = new List<SQLInunSequiaData>();


			//Debug.Log ("Server INST DATA COUNT: " + jsonArray.Length);
			for (int i = 0; i < jsonArray.Length; i++)
			{
				int id = jsonArray [i].ToInt ("id");
				string hash = jsonArray [i].ToString ("hash");
				string status = jsonArray [i].ToString ("status");
				string generalTitle = jsonArray [i].ToString ("generalTitle");
				string generalText = jsonArray [i].ToString ("generalText");
				string dynamicText = jsonArray [i].ToString ("dynamicText");


				SQLInunSequiaData videoData = new SQLInunSequiaData(id, hash, status, generalTitle, generalText, dynamicText);
				inunSequiaDataList.Add(videoData);
			}

			if (callback != null)
				callback(inunSequiaDataList);
		}


		public IEnumerator GetVideoDataList(System.Action<List<SQLVideoData>> callback)
		{

			Debug.Log("Getting all Video data from server");


			string jsonString = string.Empty;

			yield return StartCoroutine(NetworkHandler.Instance.GetVideoDataJson((result) =>
				{
					jsonString = result;    
				}));

			JSON json = new JSON();

			json.serialized = jsonString;

			JSON[] jsonArray = json.ToArray<JSON>("fichaVideos");

			List<SQLVideoData> videoDataList = new List<SQLVideoData>();


			Debug.Log ("Server INST DATA COUNT: " + jsonArray.Length);
			for (int i = 0; i < jsonArray.Length; i++)
			{
				int id = jsonArray [i].ToInt ("id");
				//Debug.Log ("ID: " + id);
				string hash = jsonArray [i].ToString ("hash");
				int miniBGId = jsonArray [i].ToInt ("miniBG_id");
				int fullBGId = jsonArray [i].ToInt ("fullBG_id");
				int videoId = jsonArray [i].ToInt ("video_id");
				string tag = jsonArray [i].ToString ("tag");
				string titleDescText = jsonArray [i].ToString ("titleDescText"); 
				string descText = jsonArray [i].ToString ("DescText"); 
				string textDireccion = jsonArray [i].ToString ("TextDireccion"); 
				string textGuion = jsonArray [i].ToString ("TextGuion"); 
				string textProduccion = jsonArray [i].ToString ("TextProduccion"); 

				SQLVideoData videoData = new SQLVideoData(id, hash, miniBGId, fullBGId, videoId, tag, titleDescText, descText, textDireccion, textGuion, textProduccion);
				videoDataList.Add(videoData);
			}

			if (callback != null)
				callback(videoDataList);
		}

		public IEnumerator GetImgDataList(System.Action<List<SQLImgData>> callback)
		{

			Debug.Log("Getting all image data from server");


			string jsonString = string.Empty;

			yield return StartCoroutine(NetworkHandler.Instance.GetImgDataJson((result) =>
				{
					jsonString = result;    
				}));

			JSON json = new JSON();

			json.serialized = jsonString;

			JSON[] jsonArray = json.ToArray<JSON>("fichaGlobal");

			List<SQLImgData> imgDataList = new List<SQLImgData>();

			for (int i = 0; i < jsonArray.Length; i++)
			{
				//int id = jsonArray [i].ToInt ("id");
				int id = i + 1; // Falta ID en el json
				Debug.Log ("Ficha global - ID: " + id);
				string hash = jsonArray [i].ToString ("hash");
				string type = jsonArray [i].ToString ("type");
				int enabled = jsonArray [i].ToInt ("enabled");
				int imageBG_id = jsonArray [i].ToInt ("imageBG_id");
				string nameText= jsonArray [i].ToString ("nameText");
				string nameClientText= jsonArray [i].ToString ("nameCientText");
				string titleColor = jsonArray [i].ToString ("titleColor");
				string TextA = jsonArray [i].ToString ("TextA");
				string ColorA = jsonArray [i].ToString ("ColorA");
				string TextB = jsonArray [i].ToString ("TextB");
				string ColorB = jsonArray [i].ToString ("ColorB");
				string TextC = jsonArray [i].ToString ("TextC");
				string ColorC = jsonArray [i].ToString ("ColorC");
				string TextD = jsonArray [i].ToString ("TextD");
				string ColorD = jsonArray [i].ToString ("ColorD");
				/*

				public int id { get; set;}
				public string hash { get; set;}
				public string type { get; set;}
				public int imageBGId { get; set;}
				public string nameText { get; set;}
				public string titleColor { get; set;}
				public string nameCientText { get; set;}
				public string textA { get; set;}
				public string textB { get; set;}
				public string textC { get; set;}
				public string textD { get; set;}
				public string colorA { get; set;}
				public string colorB { get; set;}
				public string colorC { get; set;}
				public string colorD { get; set;}

				*/


				SQLImgData imgData = new SQLImgData(id, hash, type, imageBG_id, nameText, titleColor, nameClientText, TextA, TextB, TextC, TextD, ColorA, ColorB, ColorC, ColorD);
				imgDataList.Add(imgData);
			}

			if (callback != null)
				callback(imgDataList);
		}
    }
}