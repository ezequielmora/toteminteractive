﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Net;

public class NetworkHandler : MonoBehaviour {

    //private string server = "192.168.1.238";
    //private int port = 8082;

    [Header("Dirección IP y PUERTO del CMS")]
    [SerializeField]
    private string server = "192.168.1.8";
    [SerializeField]
    private int port = 80;

    private string apiVersion = "v1";
    [SerializeField]
    private bool forceConnectionFailure = false;

    // IP Del servidor usando VPN Del acuario
    [Header("Marcar esta casilla para habilitar la carga de datos desde el VPN del acuario")]
    [Space(5)]
    public bool vpnCheck = false;


    public enum JsonStruct
    {
        fishHashes,
        fishes,
        icons,
        images,
        languages
    }

    private string fishHashesApiCode = "languages";

    private static NetworkHandler instance;
    public static NetworkHandler Instance
    {
        get
        { 
            return instance;
        }
    }

    ConnectionTesterStatus connectionTestResult = ConnectionTesterStatus.Undetermined;


    private void Awake()
    {
        instance = this;
        if (vpnCheck)
            instance.server = "10.50.50.243";
            //instance.server = "192.168.1.238:8082";
    }
        
    float maxTime = 2.0f;


    
    public IEnumerator CheckConnectionToMasterServer(System.Action<bool> callback) {

        Ping pingMasterServer = new Ping(server);

        Debug.Log(pingMasterServer.ip);

        float startTime = Time.time;

        while (!pingMasterServer.isDone && Time.time < startTime + maxTime) {
            yield return null;
        }

        Debug.Log(pingMasterServer.time);

        if (pingMasterServer.time == -1 || pingMasterServer.time >= maxTime * 1000)
        {
            callback(false);
        }
        else
        {
            if (forceConnectionFailure)
                callback(false);
            else
                callback(true);
        }
    }
    
    public IEnumerator GetInstitucionalDataJson(System.Action<string> callback)
    {
        string json = string.Empty;
		yield return StartCoroutine(GetJson("totemInstitucional", (result) =>
            {
                json = result;    
            }));

        callback(json);
    }

	public IEnumerator GetVideoDataJson(System.Action<string> callback)
	{
		string json = string.Empty;
		yield return StartCoroutine(GetJson("totemVideos", (result) =>
			{
				json = result;    
			}));

		callback(json);
	}

	public IEnumerator GetInunSequiaDataJson(System.Action<string> callback)
	{
		string json = string.Empty;
		yield return StartCoroutine(GetJson("totemInundSeq", (result) =>
			{
				json = result;    
			}));

		callback(json);
	}


	public IEnumerator GetImgDataJson(System.Action<string> callback)
	{
		string json = string.Empty;
		yield return StartCoroutine(GetJson("totemGlobal", (result) =>
			{
				json = result;    
			}));

		callback(json);
	}

    public IEnumerator GetImagesJson(System.Action<string> callback)
    {
        string json = string.Empty;
        yield return StartCoroutine(GetJson("images", (result) =>
            {
                json = result;    
            }));
        
        callback(json);
    }

	public IEnumerator GetMediaJson(System.Action<string> callback)
	{
		string json = string.Empty;
		yield return StartCoroutine(GetJson("images", (result) =>
			{
				json = result;    
			}));

		callback(json);
	}




    public IEnumerator ResolveWWW(string url, System.Action<WWW> callback)
    {
        WWW www = new WWW(url);
        yield return www;

        if (www.error == null)
        {
            callback(www);
        }
        else
        {
            callback(null);
            Debug.Log("ERROR: " + www.error);
        }
    }

    public IEnumerator GetJson(string apiName, System.Action<string> callback)
    {
        string url = string.Format("http://{0}:{1}/api/{2}/{3}", server, port, apiVersion, apiName);

        WWW www = null;
        yield return StartCoroutine(ResolveWWW(url, (result) =>
            {
                www = result;    
            }));

        if (www != null)
            callback(www.text);    
        else
            callback(string.Empty);


        www.Dispose();
    }   
	
    public IEnumerator GetJson(string apiName, System.Action<string> callback, params int[] ids)
    {
        string url = string.Format("http://{0}:{1}/api/{2}/{3}?ids=", server, port, apiVersion, apiName);

        for (int i = 0; i < ids.Length; i++) 
        {
            if (i == 0)
            {
                url = string.Format("{0}{1}", url, ids[i]);
            }
            else
            {
                url = string.Format("{0},{1}", url, ids[i]);
            }
        }

        Debug.Log(url);

        WWW www = null;
        yield return StartCoroutine(ResolveWWW(url, (result) =>
            {
                www = result;    
            }));
        
        if (www != null)
        {
            callback(www.text);
        }
        else
        {
            callback(string.Empty);
        }

        print("DATA ONLINE " + www.text);

        www.Dispose();
    }
}
