﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CMS;
using SQL;
using SQL.Structs;
using System.Linq;
using EnhancedScrollerDemos.CellControllerInstitucional;
using EnhancedScrollerDemos.CellControllerIMG;
using EnhancedUI;


public class ContentHandler : MonoBehaviour {

    public delegate void ConnectionStatusCallBack(bool status);

    public delegate void IsFinishedCallback(bool status);

    private CMSHandler cmsHandler;
    private DataHandler dataHandler;
    private ImageController imageController;
    private UIController uiController;
	private LoadingController loadingController;
	private MediaController mediaController;
    //private CellControllerInstitucional institucionalController;
    // Fix Eze
	private PagedInstitucionalHandler pagedInstitucionalHandler;
	private PagedVideoHandler pagedVideoHandler;
	private PagedGlobalHandler pagedGlobalHandler;
	private PulsosHandler pulsosHandler;

    // Flag de control
    [HideInInspector]
    public bool isLoaded = false;

    [Header("Carga de Base de datos OFFLINE")]
    [SerializeField]
    private bool offlineCheck;

	NetworkHandler networkHandler;

    [SerializeField]
    private int connectionsRetries = 3;


    private static ContentHandler instance;
    public static ContentHandler Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    //Funcion para recargar base de datos
    public void Reload()
    {
        //UIController.Instance.ReloadUI();
        Start();
    }


    private void Start()
    {
        //loadingObj.SetActive(true);

        cmsHandler = CMSHandler.Instance;
        dataHandler = DataHandler.Instance;
        imageController = ImageController.Instance;
		loadingController = LoadingController.Instance;
		pagedInstitucionalHandler = PagedInstitucionalHandler.Instance;
		pagedVideoHandler = PagedVideoHandler.Instance;
		pagedGlobalHandler = PagedGlobalHandler.Instance;
		mediaController = MediaController.Instance;
		pulsosHandler = PulsosHandler.Instance;

        //uiController = UIController.Instance;

        networkHandler = NetworkHandler.Instance;

        Run();
    }

    // Hago publica el comando run para poder llamarlo con el boton reload de fish preview
    private void Run()
    {
        ////////////////    
        //// Deshabilito temporalmente el ping check
        ///////////////
        if(!offlineCheck)
        StartCoroutine(CheckConnectionStatus(callBack_ConnStatus));
        else
        StartCoroutine(LoadDBData());
    }

    private IEnumerator CheckConnectionStatus(ConnectionStatusCallBack callBack)
    {
        DebugInfo("Trying to connect to server...");

        ////////////////    
        //// Deshabilito temporalmente el ping check
        ///////////////
        bool status = false;

        //bool status = true;
        /*
        yield return StartCoroutine(networkHandler.CheckConnectionToMasterServer((result) =>                 
            {
                status = result;    
            }));
        
        Debug.Log(status);
        */
        int retries = 1;
        while (status == false)
        {
            DebugInfo("Retrying to connect to server. #" + retries.ToString());

            status = false;

            yield return StartCoroutine(networkHandler.CheckConnectionToMasterServer((result) =>                 
                {
                    status = result;    
                }));
            
            if (retries >= connectionsRetries)
                break;

            retries++;
        }

        if (status)
        {
            DebugInfo("Connection with server succeeded.");    
        }
        else
        {
            // Si el estado de conexion es falso vuelve false el callback y usa LoadDBData() method
            DebugInfo("Connection with server failed.");
            yield return new WaitForSeconds(1f);
            DebugInfo("Loading data from local cache...");
            yield return new WaitForSeconds(1f);
        }
        
        callBack(status);

        yield return null;
    }

    // Estado de conexion Eze
    private void callBack_ConnStatus(bool status)
    {
        if (status)
            StartCoroutine(UpdateData());               //If internet connection load data from server and then from db
        else
            StartCoroutine(LoadDBData());               //If no internet connection bypass loading from server
    }

    private IEnumerator UpdateData()
    {
        DebugInfo("Checking for updates...");

		yield return StartCoroutine(UpdateMediaContentData());
		yield return StartCoroutine(UpdateInunSequiaData());
		yield return StartCoroutine(UpdateVideoData());
		yield return StartCoroutine(UpdateInstitucionalData());  
		yield return StartCoroutine(UpdateImgData());  
        DataUpdated();
    }

   
	private IEnumerator UpdateInunSequiaData()
	{

		DebugInfo("Getting Video Data data from server...");

		//Load from server/json updated icons.
		List<SQLInunSequiaData> serverInunSequiaData = new List<SQLInunSequiaData>();
		yield return StartCoroutine(cmsHandler.GetInunSequiaDataList((result) =>
			{
				serverInunSequiaData = result;
			}));


		DebugInfo("Removing obsolete Video Data...");
		//Removing Obsolete icons
		yield return StartCoroutine(dataHandler.RemoveObsoleteItems<SQLInunSequiaData>(serverInunSequiaData));


		DebugInfo("Updating Video Data in db...");
		List<int> toBeUpdatedIds = new List<int>();
		yield return StartCoroutine(dataHandler.GetOutdatedItemsIds<SQLInunSequiaData>(serverInunSequiaData, (result) =>
			{
				toBeUpdatedIds = result;    
			}));

		List<SQLInunSequiaData> updatedInunSequiaData = new List<SQLInunSequiaData>();

		foreach (SQLInunSequiaData inunSequia in serverInunSequiaData)
		{
			if (toBeUpdatedIds.Contains(inunSequia.id))
			{
				updatedInunSequiaData.Add(inunSequia);
			}    
		}

		yield return StartCoroutine(dataHandler.UpdateItemsData<SQLInunSequiaData>(updatedInunSequiaData));
	}
   
	private IEnumerator UpdateVideoData()
	{

		DebugInfo("Getting Video Data data from server...");

		//Load from server/json updated icons.
		List<SQLVideoData> serverVideoFormViewData = new List<SQLVideoData>();
		yield return StartCoroutine(cmsHandler.GetVideoDataList((result) =>
			{
				serverVideoFormViewData = result;
			}));


		DebugInfo("Removing obsolete Video Data...");
		//Removing Obsolete icons
		yield return StartCoroutine(dataHandler.RemoveObsoleteItems<SQLVideoData>(serverVideoFormViewData));


		DebugInfo("Updating Video Data in db...");
		List<int> toBeUpdatedIds = new List<int>();
		yield return StartCoroutine(dataHandler.GetOutdatedItemsIds<SQLVideoData>(serverVideoFormViewData, (result) =>
			{
				toBeUpdatedIds = result;    
			}));

		List<SQLVideoData> updatedVideoFormViewData = new List<SQLVideoData>();

		foreach (SQLVideoData videoFormView in serverVideoFormViewData)
		{
			if (toBeUpdatedIds.Contains(videoFormView.id))
			{
				updatedVideoFormViewData.Add(videoFormView);
			}    
		}

		yield return StartCoroutine(dataHandler.UpdateItemsData<SQLVideoData>(updatedVideoFormViewData));
	}

    private IEnumerator UpdateInstitucionalData()
    {

        DebugInfo("Getting icons data from server...");

        //Load from server/json updated icons.
		List<SQLInstitucionalData> serverInstData = new List<SQLInstitucionalData>();
		yield return StartCoroutine(cmsHandler.GetInsitucionalDataList((result) =>
            {
				serverInstData = result;
            }));


        DebugInfo("Removing obsolete institucionalData...");
        //Removing Obsolete icons
		yield return StartCoroutine(dataHandler.RemoveObsoleteItems<SQLInstitucionalData>(serverInstData));


        DebugInfo("Updating icons in db...");
        List<int> toBeUpdatedIds = new List<int>();
		yield return StartCoroutine(dataHandler.GetOutdatedItemsIds<SQLInstitucionalData>(serverInstData, (result) =>
            {
                toBeUpdatedIds = result;    
            }));

		List<SQLInstitucionalData> updatedInstData = new List<SQLInstitucionalData>();

		foreach (SQLInstitucionalData instData in serverInstData)
        {
			if (toBeUpdatedIds.Contains(instData.id))
            {
				updatedInstData.Add(instData);
            }    
        }

		yield return StartCoroutine(dataHandler.UpdateItemsData<SQLInstitucionalData>(updatedInstData));
    }

	/* No confundir con Image que tiene todas las imagenes, este es informacion de "Global" */
	private IEnumerator UpdateImgData()
	{

		DebugInfo("Getting ImgData (Global) data from server...");

		//Load from server/json updated icons.
		List<SQLImgData> serverImgData = new List<SQLImgData>();
		yield return StartCoroutine(cmsHandler.GetImgDataList((result) =>
			{
				serverImgData = result;
			}));


		DebugInfo("Removing obsolete imgData...");
		//Removing Obsolete icons
		yield return StartCoroutine(dataHandler.RemoveObsoleteItems<SQLImgData>(serverImgData));


		DebugInfo("Updating imgData in db...");
		List<int> toBeUpdatedIds = new List<int>();
		yield return StartCoroutine(dataHandler.GetOutdatedItemsIds<SQLImgData>(serverImgData, (result) =>
			{
				toBeUpdatedIds = result;    
			}));

		List<SQLImgData> updatedImgData = new List<SQLImgData>();

		foreach (SQLImgData imgData in serverImgData)
		{
			if (toBeUpdatedIds.Contains(imgData.id))
			{
				updatedImgData.Add(imgData);
			}    
		}

		yield return StartCoroutine(dataHandler.UpdateItemsData<SQLImgData>(updatedImgData));
	}

    private IEnumerator UpdateImageData()
    {
        DebugInfo("Getting image data from server...");

        //Load from server/json updated icons.
        List<SQLImage> serverImages = new List<SQLImage>();
        yield return StartCoroutine(cmsHandler.GetImageList((result) =>
            {
                serverImages = result;
            }));
		Debug.Log (serverImages[0].id);
        DebugInfo("Removing obsolete images...");

        List<SQLImage> removedImages = new List<SQLImage>();
        yield return StartCoroutine(dataHandler.RemoveObsoleteItems<SQLImage>(serverImages, (result) =>
            {
                removedImages = result;
            }));

        foreach (SQLImage img in removedImages)
        {
            imageController.DeleteImage(img);
        }

        DebugInfo("Updating images in db and assets...");

        List<int> toBeUpdatedIds = new List<int>();
        yield return StartCoroutine(dataHandler.GetOutdatedItemsIds<SQLImage>(serverImages, (result) =>
            {
                toBeUpdatedIds = result;    
            }));

        if (toBeUpdatedIds.Count == 0)
        {
            DebugInfo("Images are all up to date.");
        }
        else
        {
            DebugInfo("Updating images...");
            List<SQLImage> updatedImages = new List<SQLImage>();

            foreach (SQLImage image in serverImages)
            {
                if (toBeUpdatedIds.Contains(image.id))
                {
                    SQLImage newImage = new SQLImage();
					DebugInfo(string.Format("Creating Image id:{0}", image.imgUrl));
                    yield return StartCoroutine(imageController.CreateImage(image, (result) =>
                        {
                            newImage = result;
                        }));

                    updatedImages.Add(newImage);
                }    
            }

            DebugInfo("Updating images DB");
            yield return StartCoroutine(dataHandler.UpdateItemsData<SQLImage>(updatedImages));   
        }
    }


	private IEnumerator UpdateMediaContentData()
	{
		DebugInfo("Getting media data from server...");

		//Load from server/json updated icons.
		List<SQLMediaContent> serverMedia = new List<SQLMediaContent>();
		yield return StartCoroutine(cmsHandler.GetMediaList((result) =>
			{
				serverMedia = result;
			}));
		Debug.Log (serverMedia[0].id);
		DebugInfo("Removing obsolete images...");

		List<SQLMediaContent> removedMedia = new List<SQLMediaContent>();
		yield return StartCoroutine(dataHandler.RemoveObsoleteItems<SQLMediaContent>(serverMedia, (result) =>
			{
				removedMedia = result;
			}));

		foreach (SQLMediaContent media in removedMedia)
		{
			mediaController.DeleteMedia(media);
		}

		DebugInfo("Updating images in db and assets...");

		List<int> toBeUpdatedIds = new List<int>();
		yield return StartCoroutine(dataHandler.GetOutdatedItemsIds<SQLMediaContent>(serverMedia, (result) =>
			{
				toBeUpdatedIds = result;    
			}));

		if (toBeUpdatedIds.Count == 0)
		{
			DebugInfo("Images are all up to date.");
		}
		else
		{
			DebugInfo("Updating images...");
			List<SQLMediaContent> updatedMedia = new List<SQLMediaContent>();

			foreach (SQLMediaContent media in serverMedia)
			{
				if (toBeUpdatedIds.Contains(media.id))
				{
					if (media.mimeType == "image/png" || media.mimeType == "image/jpeg") {

						SQLMediaContent newMedia = new SQLMediaContent ();
						DebugInfo (string.Format ("Creating Image id:{0}", media.url));
						yield return StartCoroutine (mediaController.CreateImage (media, (result) => {
							newMedia = result;
						}));

						updatedMedia.Add (newMedia);
					} else {
						updatedMedia.Add (media);
					}
				}    
			}

			DebugInfo("Updating images DB");
			yield return StartCoroutine(dataHandler.UpdateItemsData<SQLMediaContent>(updatedMedia));   
		}
	}

    private void DataUpdated()
    {
        StartCoroutine(LoadDBData());
    }

    private IEnumerator LoadDBData()
    {
        DebugInfo("Loading Data from db...");

        DebugInfo("Loading Institucional data from DB");
		List<InstitucionalData> tempInstData = new List<InstitucionalData>();
		yield return StartCoroutine(dataHandler.GetInstitucionalData((result) =>
            {
				tempInstData = result;    
                //languageController.SetLanguages(tempLanguages);
            }));

		DebugInfo("Loading Video data from DB");
		List<VideoData> tempVideoData = new List<VideoData>();
		yield return StartCoroutine(dataHandler.GetVideoData((result) =>
			{
				tempVideoData = result;    
				//languageController.SetLanguages(tempLanguages);
			}));

		DebugInfo("Loading InunSequia data from DB");
		yield return StartCoroutine(dataHandler.GetInunSequiaData((result) =>
			{
				pulsosHandler.SetPulsosData(result);
			}));

		DebugInfo("Loading imgData data from DB");
		List<ImgData> tempImgData = new List<ImgData>();
		yield return StartCoroutine(dataHandler.GetImgData((result) =>
			{
				tempImgData = result;    
			}));
				

		DebugInfo("Loading Media data from DB");
		yield return StartCoroutine(dataHandler.GetMedia((result) =>
			{
				mediaController.SetMedia(result);
			}));
		
		DebugInfo("Loading Media files from DB");
		yield return StartCoroutine(mediaController.GetImagesFromAssets(tempInstData, tempVideoData, tempImgData, (result, result2, result3) =>
			{
				pagedInstitucionalHandler.SetInstitucionalData(result);
				pagedVideoHandler.SetVideoData(result2);
				pagedGlobalHandler.SetGlobalGalleryData(result3);
				//fishController.SetFishes(result);
				//languageController.SetLanguages(result1);
				//iconController.SetIcons(result2);
			})); 
		
       /* DebugInfo("Loading Images files from DB");
		yield return StartCoroutine(imageController.GetImagesFromAssets(tempInstData, (result) =>
            {
				institucionalController.SetInstitucionalData(result);
                //fishController.SetFishes(result);
                //languageController.SetLanguages(result1);
                //iconController.SetIcons(result2);
            }));*/
        
        DataLoaded();
    }

    private void DataLoaded()
    {

		StartCoroutine (CheckDataLoaded ());

		/*
        foreach (SphereMenuController controller in sphereControllers)
        {
            controller.Init(fishController.Fishes);
        }

        languageController.DoneLoading();*/
    }

	private IEnumerator CheckDataLoaded()
	{
		bool _dataBaseEmpty = false;

		yield return dataHandler.CheckDatabaseEmpty((result) =>
			{
				_dataBaseEmpty = result; 
			});


		loadingController.LoadingDone (_dataBaseEmpty);
        //uiController.LoadingDone (_dataBaseEmpy);

        // llamar creacion de botones una vez que se termino de cargar toda la data desde la base de datos
        // checkea que exista la instancia de fishpreview antes de llamar la funcion de creacion de botones (dropdown list)
        /*if (FishPreview.instance != null)
        {
            FishPreview.instance.ButtonCreation();
        }*/

        isLoaded = true;

    }

    public IEnumerator GetHandlerState(System.Action<bool> callback)
    {

        yield return isLoaded;

        callback(isLoaded);

    }

    private void DebugInfo(string msg)
    {
        Debug.Log(msg);
		loadingController.UpdateLoadingInfo (msg);
    }
}
