﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CMS;
using SQL.Structs;
using SQLite4Unity3d;
using EnhancedScrollerDemos.CellControllerInstitucional;
using EnhancedScrollerDemos.CellControllerIMG;
using EnhancedUI;

namespace SQL
{
    public class DataHandler : MonoBehaviour {

        DataService dataService;

        [SerializeField]
        private bool deleteAndCreateNewDb = false;

        private static DataHandler instance;
        public static DataHandler Instance
        {
            get
            {
                return instance;
            }
        }

        void Awake()
        {
            instance = this;
            dataService = new DataService("totem.db");
            //Crea una base de datos nueva
            if (deleteAndCreateNewDb)
                dataService.CreateDB();
        }

		public IEnumerator CheckDatabaseEmpty(System.Action<bool> callback = null)
		{
			bool _dataBaseEmpy = false;

			yield return dataService.CheckDatabaseEmpty((result) =>
				{
					_dataBaseEmpy = result; 
				});

			callback (_dataBaseEmpy);
		}

   
        public IEnumerator RemoveObsoleteItems<T>(List<T> serverData, System.Action<List<T>> callback = null) where T : ISQLItem, new()
        {
            List<T> obsoleteItems = new List<T>();
            yield return dataService.GetObsoleteItemsInDB<T>(serverData, (result) =>
                {
                    obsoleteItems = result; 
                });
            
            foreach (T item in obsoleteItems)
            {
                yield return dataService.DeleteItem<T>(item);
            }

            if (callback != null)
                callback(obsoleteItems);
        }

        public IEnumerator GetOutdatedItemsIds<T>(List<T> serverData, System.Action<List<int>> callback) where T : ISQLItem, new()
        {
            List<int> outDatedItemsIds = new List<int>();
            foreach (T item in serverData)
            {
                IEnumerable<T> tempItem = dataService.GetItem<T>(item.id);
                yield return tempItem;

                if (!tempItem.Any())
                {
                    outDatedItemsIds.Add(item.id);
                }
                else
                {
                    yield return StartCoroutine(dataService.CheckUpToDateStatus<T>(item, (result) =>
                        {
                            if (result == false)
                                outDatedItemsIds.Add(item.id);
                        }));
                }
            }

            callback(outDatedItemsIds);
        }
            
        /*public IEnumerator UpdateFishData(List<Fish> updatedFishes)
        {
            yield return StartCoroutine(dataService.UpdateFishes(updatedFishes));
        }

        public IEnumerator UpdateLanguageData(List<SQLLanguage> languages)
        {
            yield return StartCoroutine(dataService.UpdateLanguages(languages));
        }*/

        public IEnumerator UpdateItemsData<T>(List<T> updatedItems) where T : ISQLItem
        {
            foreach (T item in updatedItems)
            {
                yield return dataService.UpdateItem<T>(item);
            }
        }
			

        public IEnumerator GetImages(System.Action<List<StreamingImage>> callback)
        {
            List<StreamingImage> imageList = new List<StreamingImage>();

            IEnumerable<SQLImage> images = dataService.GetItems<SQLImage>();
            yield return images;

            foreach (SQLImage image in images)
            {
                StreamingImage img = new StreamingImage(image);
                imageList.Add(img);
            }

            callback(imageList);
        }

		public IEnumerator GetMedia(System.Action<List<MediaContent>> callback)
		{
			List<MediaContent> mediaList = new List<MediaContent>();

			IEnumerable<SQLMediaContent> mediaSQLList = dataService.GetItems<SQLMediaContent>();
			yield return mediaSQLList;

			foreach (SQLMediaContent mediaSQL in mediaSQLList)
			{
				MediaContent media = new MediaContent(mediaSQL);
				mediaList.Add(media);
			}

			callback(mediaList);
		}



		public IEnumerator GetInstitucionalData(System.Action<List<InstitucionalData>> callback)
        {
			
			List<InstitucionalData> institucionalDataList = new List<InstitucionalData>();
			IEnumerable <SQLInstitucionalData> institucionalDataSQL = dataService.GetItems<SQLInstitucionalData>();
			yield return institucionalDataSQL;

			foreach (SQLInstitucionalData instData in institucionalDataSQL)
            {
				InstitucionalData tempInstData = new InstitucionalData(instData);
				institucionalDataList.Add(tempInstData);
            }

			callback(institucionalDataList);
        }

		public IEnumerator GetVideoData(System.Action<List<VideoData>> callback)
		{

			List<VideoData> videoDataList = new List<VideoData>();
			IEnumerable <SQLVideoData> videoDataSQLList = dataService.GetItems<SQLVideoData>();
			yield return videoDataSQLList;

			foreach (SQLVideoData videoDataSQL in videoDataSQLList)
			{
				VideoData tempVideoData = new VideoData(videoDataSQL);
				videoDataList.Add(tempVideoData);
			}

			callback(videoDataList);
		}

		public IEnumerator GetInunSequiaData(System.Action<List<InunSequiaData>> callback)
		{

			List<InunSequiaData> inunSequiaDataList = new List<InunSequiaData>();
			IEnumerable <SQLInunSequiaData> inunSequiaDataSQLList = dataService.GetItems<SQLInunSequiaData>();
			yield return inunSequiaDataSQLList;

			foreach (SQLInunSequiaData inunSequiaDataSQL in inunSequiaDataSQLList)
			{
				InunSequiaData tempInunSequiaData = new InunSequiaData(inunSequiaDataSQL);
				inunSequiaDataList.Add(tempInunSequiaData);
			}

			callback(inunSequiaDataList);
		}

        // Hace un Retrieve de toda la data
		public IEnumerator GetImgData(System.Action<List<ImgData>> callback)
		{
			List<ImgData> imgDataList = new List<ImgData>();
			IEnumerable <SQLImgData> imgDataSQL = dataService.GetItems<SQLImgData>();
			yield return imgDataSQL;

			foreach (SQLImgData instData in imgDataSQL)
			{
				ImgData tempImgData = new ImgData(instData);
				imgDataList.Add(tempImgData);
			}

			callback(imgDataList);
		}
			
    }
}