﻿using SQLite4Unity3d;
using UnityEngine;
//using UnityEngine.Windows;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SQL.Structs;
using System.Linq;

#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif

public class DataService  {
    
	private SQLiteConnection _connection;

    public void CreateDB()
    {
        /*_connection.DropTable<SQLFish>();
        _connection.CreateTable<SQLFish>();

        _connection.DropTable<SQLFishHash>();
        _connection.CreateTable<SQLFishHash>();

        _connection.DropTable<SQLFishRegionName>();
        _connection.CreateTable<SQLFishRegionName>();

        _connection.DropTable<SQLFishIcon>();
        _connection.CreateTable<SQLFishIcon>();

        _connection.DropTable<SQLFishRelation>();
        _connection.CreateTable<SQLFishRelation>();

        _connection.DropTable<SQLIcon>();
        _connection.CreateTable<SQLIcon>();
*/
       // _connection.DropTable<SQLImage>();
       // _connection.CreateTable<SQLImage>();

		_connection.DropTable<SQLInstitucionalData>();
		_connection.CreateTable<SQLInstitucionalData>();

		_connection.DropTable<SQLImgData>();
		_connection.CreateTable<SQLImgData>();

		_connection.DropTable<SQLMediaContent>();
		_connection.CreateTable<SQLMediaContent>();

		_connection.DropTable<SQLVideoData>();
		_connection.CreateTable<SQLVideoData>();

		_connection.DropTable<SQLInunSequiaData>();
		_connection.CreateTable<SQLInunSequiaData>();


		/*
        _connection.DropTable<SQLLanguage>();
        _connection.CreateTable<SQLLanguage>();

        _connection.DropTable<SQLDetailedPoint>();
        _connection.CreateTable<SQLDetailedPoint>();
        */
    }
	/*
    public void DeleteFish(SQLFish f)
    {
        _connection.Delete<SQLFish>(f.id);
        _connection.Delete<SQLFishHash>(f.id);
        _connection.Delete<SQLFishRegionName>(f.id);
        _connection.Delete<SQLFishIcon>(f.id);
        _connection.Delete<SQLFishRelation>(f.id);

        Debug.Log("Removed item id: " + f.id);
    }
	*/

    public IEnumerator DeleteItem<T>(T item) where T : ISQLItem
    {
        yield return _connection.Delete<T>(item.id);
    }

    public IEnumerator GetObsoleteItemsInDB<T>(List<T> serverData, System.Action<List<T>> callback) where T : ISQLItem, new()
    {
        string idQuery = string.Empty;

        string structFullName = typeof(T).ToString();
        string[] structName = structFullName.Split('.');
        string tableName = structName[structName.Length - 1];

        foreach (T item in serverData)
        {
            idQuery += item.id.ToString() + ",";
        }

        idQuery = idQuery.Remove(idQuery.Length - 1);
        Debug.Log(idQuery);

        string query = string.Format("SELECT * FROM {0} WHERE {0}.id NOT IN ({1})", tableName, idQuery);

        List<T> obsoleteItems = new List<T>();
        obsoleteItems = _connection.Query<T>(query);
        yield return obsoleteItems;

        callback(obsoleteItems);
    }

    public IEnumerable<T> GetItem<T>(int id) where T : new()
    {
        string structFullName = typeof(T).ToString();
        string[] structName = structFullName.Split('.');
        string tableName = structName[structName.Length - 1];

        string query = string.Format("SELECT * FROM {0} WHERE {0}.id == {1}", tableName, id);
        return _connection.Query<T>(query);
    }

    public IEnumerator CheckUpToDateStatus<T>(T item, System.Action<bool> callback) where T : ISQLItem, new()
    {
        string structFullName = typeof(T).ToString();
        string[] structName = structFullName.Split('.');
        string tableName = structName[structName.Length - 1];

        string query = string.Format("SELECT * FROM {0} WHERE {0}.id == {1} AND {0}.hash != '{2}'", tableName, item.id, item.hash);
        IEnumerable<T> itemResult = _connection.Query<T>(query);
        yield return itemResult;    

        if (itemResult.Any())
        {
            callback(false);
        }
        else
        {
            callback(true);
        }
    }

   /* public IEnumerable<SQLFish> GetFishesToRemove(List<SQLFishHash> updatedElements)
    {
        string idQuery = string.Empty;

        for (int i = 0; i < updatedElements.Count; i++)
        {
            string preChar = "";
            if (i != 0)
            {
                preChar = ",";
            }

            idQuery += preChar + updatedElements[i].fishId.ToString();
        }

        string structFullName = typeof(SQLFish).ToString();
        string[] structName = structFullName.Split('.');

        idQuery = string.Format("SELECT * FROM {0} WHERE {0}.id NOT IN ({1})", structName[structName.Length-1], idQuery);
        return _connection.Query<SQLFish>(idQuery);
        //return _connection.Query<Fishes>("SELECT * FROM fishes WHERE fishes.id NOT IN (?)", idQuery);
    }*/

    /*
    public IEnumerator GetImagesToRemove(List<SQLImage> serverData, System.Action<List<SQLImage>> callback)
    {
        string idQuery = string.Empty;

        string structFullName = typeof(SQLImage).ToString();
        string[] structName = structFullName.Split('.');
        string tableName = structName[structName.Length - 1];

        List<SQLImage> imgsToRemove = new List<SQLImage>();

        for (int i = 0; i < serverData.Count; i++)
        {
            string preChar = "";
            if (i != 0)
            {
                preChar = ",";
            }

            idQuery += preChar + serverData[i].id.ToString();

            string query = string.Format("SELECT * FROM  {0} WHERE {0}.id = {1} AND {0}.hash != '{2}'", tableName, serverData[i].id, serverData[i].hash);

            List<SQLImage> imgToRemove = _connection.Query<SQLImage>(query);

            yield return imgToRemove;

            if (imgToRemove.Count > 0)
                imgsToRemove.Add(imgToRemove[0]);
        }

        idQuery = string.Format("SELECT * FROM {0} WHERE {0}.id NOT IN ({1})", tableName, idQuery);
        List<SQLImage> imgs = _connection.Query<SQLImage>(idQuery);

        imgsToRemove.Concat(imgs);

        callback(imgsToRemove);
    }
    */

    /*
    public IEnumerator GetIconsToRemove(List<SQLIcon> serverData, System.Action<List<SQLIcon>> callback)
    {
        string idQuery = string.Empty;

        string structFullName = typeof(SQLIcon).ToString();
        string[] structName = structFullName.Split('.');
        string tableName = structName[structName.Length - 1];

        List<SQLIcon> iconsToRemove = new List<SQLIcon>();

        for (int i = 0; i < serverData.Count; i++)
        {
            string preChar = "";
            if (i != 0)
            {
                preChar = ",";
            }

            idQuery += preChar + serverData[i].id.ToString();

            string query = string.Format("SELECT * FROM  {0} WHERE {0}.id == {1} AND {0}.hash != '{2}'", tableName, serverData[i].id, serverData[i].hash);
            List<SQLIcon> iconToRemove = _connection.Query<SQLIcon>(query);
            yield return iconsToRemove;

            if (iconToRemove.Count > 0)
                iconsToRemove.Add(iconToRemove[0]);
        }

        idQuery = string.Format("SELECT * FROM {0} WHERE {0}.id NOT IN ({1})", tableName, idQuery);
        List<SQLIcon> icons = _connection.Query<SQLIcon>(idQuery);

        iconsToRemove.Concat(icons);

        callback(iconsToRemove);
    }
    */

	public IEnumerator CheckDatabaseEmpty(System.Action<bool> callback = null)
	{
		bool _dataBaseEmpty = false;


		int _amountOfDataInDB = _connection.Table<SQLInstitucionalData> ().Count ();

		Debug.Log (_amountOfDataInDB);

		yield return _amountOfDataInDB;

		if (_amountOfDataInDB == 0)
		{
			_dataBaseEmpty = true;
		}

		callback (_dataBaseEmpty);
	}

    public IEnumerable<SQLImage> GetImage(int imgId)
    {
        return _connection.Table<SQLImage>().Where(x => x.id == imgId);
    }
        
    public IEnumerable<T> GetItems<T>() where T : new()
    {
        return _connection.Table<T>();
    }

	public IEnumerator UpdateInstitucionalData(List<SQLInstitucionalData> institucionalData)
    {
		foreach (SQLInstitucionalData instData in institucionalData)
        {
			yield return _connection.InsertOrReplace(instData);
        }
    }

    public IEnumerator UpdateImages(List<SQLImage> images)
    {
        foreach (SQLImage img in images)
        {
            yield return _connection.InsertOrReplace(img);
        }
    }

    public IEnumerator UpdateItem<T>(T item)
    {
        yield return _connection.InsertOrReplace(item);
    }

    public DataService(string DatabaseName)
    {
        #if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#endif


#if !UNITY_EDITOR

        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.dataPath + "/StreamingAssets/", DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
            var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#elif UNITY_WP8
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#elif UNITY_WINRT
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#else
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#endif
            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif

        _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);     
    }
}
    