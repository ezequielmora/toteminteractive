﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using SQLite4Unity3d;

namespace SQL.Structs
{
    public interface ISQLItem
    {
        int id
        {
            get;
            set;
        }

        string hash
        {
            get;
            set;
        }
    }

    public class SQLInstitucionalData : ISQLItem
    {
        [PrimaryKey]
        public int id { get; set;}
        public string hash { get; set;}
		public string type { get; set;}
        public string titleName { get; set;}
		public string titleColor { get; set;}
		public string mainDescription { get; set;}
		public string mainColor { get; set;}
		public string mainColorBack { get; set;}
		public string mainTextColor { get; set;}
		public string subDescription { get; set;}
		public string subColor { get; set;}
		public string fullDescription { get; set;}
		public string fullColor { get; set;}
		public string fullTextColor { get; set;}
        public int imgId { get; set;}

		public SQLInstitucionalData()
        {
            
        }

		public SQLInstitucionalData(int _id, string _hash,  string _type, string _titleName, string _titleColor, string _mainDescription, string _mainColor, string _mainColorBack, string _mainTextColor, string _subDescription, string _subColor, string _fullDescription, string _fullColor, string _fullTextColor, int _imgId)
        {
            id = _id;
            hash = _hash;
			type = _type;
			titleName = _titleName;
			titleColor = _titleColor;
			mainDescription = _mainDescription;
			mainColor = _mainColor;
			mainColorBack = _mainColorBack;
			mainTextColor = _mainTextColor;
			subDescription = _subDescription;
			subColor = _subColor;
			fullDescription = _fullDescription;
			fullColor = _fullColor;
			fullTextColor = _fullTextColor; 
            imgId = _imgId;
        }
    }

	public class SQLImgDataOLD : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public string type { get; set;}
		public string titleName { get; set;}
		public string titleColor { get; set;}
		public string subTitleName { get; set;}
		public string mainDescription { get; set;}
		public string mainColor { get; set;}
		public string mainTextColor { get; set;}
		public string subDescription_1 { get; set;}
		public string subDescriptionTextColor_1 { get; set;}
		public string subColor_1 { get; set;}
		public string subDescription_2 { get; set;}
		public string subDescriptionTextColor_2 { get; set;}
		public string subColor_2 { get; set;}
		public string subDescription_3 { get; set;}
		public string subDescriptionTextColor_3 { get; set;}
		public string subColor_3 { get; set;}
		public string fullDescription { get; set;}
		public string fullColor { get; set;}
		public string fullTextColor { get; set;}
		public int imgId { get; set;}



		public SQLImgDataOLD()
		{

		}

		public SQLImgDataOLD(int _id, string _hash, string _type, string _titleName, string _titleColor, string _subTitleName, string _mainDescription, string _mainColor, string _mainTextColor, string _subDescription_1, string _subDescriptionTextColor_1, string _subColor_1, string _subDescription_2, string _subDescriptionTextColor_2, string _subColor_2, string _subDescription_3, string _subDescriptionTextColor_3, string _subColor_3, string _fullDescription, string _fullColor, string _fullTextColor, int _imgId)
		{
			id = _id;
			hash = _hash;
			type = _type;
			titleName = _titleName;
			titleColor = _titleColor;
			subTitleName = _subTitleName;
			mainDescription = _mainDescription;
			mainColor = _mainColor;
			mainTextColor = _mainTextColor;
			subDescription_1 = _subDescription_1;
			subDescriptionTextColor_1 = _subDescriptionTextColor_1;
			subColor_1 = _subColor_1;
			subDescription_2 = _subDescription_2;
			subDescriptionTextColor_2 = _subDescriptionTextColor_2;
			subColor_2 = _subColor_2;
			subDescription_3 = _subDescription_3;
			subDescriptionTextColor_3 = _subDescriptionTextColor_3;
			subColor_3 = _subColor_3;
			fullDescription = _fullDescription;
			fullColor = _fullColor;
			fullTextColor = _fullTextColor; 
			imgId = _imgId;
		}

		/* 		- titleName = "Ficha Especie Numero 1",
                subTitleName = "Nombre cientifico especie 1",
                -mainDescription = "Esta es una especie unica en el mundo...",
                -fullDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet volutpat augue, ut sollicitudin lorem. Fusce ac ullamcorper orci. Vestibulum faucibus at odio suscipit scelerisque. Ut pellentesque eros diam, at iaculis tellus ultricies at. Aliquam facilisis condimentum sollicitudin. Pellentesque nec tortor a ligula pretium facilisis et at erat. Donec efficitur blandit dolor, eu volutpat nulla. Maecenas condimentum, dui at efficitur dapibus, tortor ipsum porta lacus, et suscipit nulla felis vel felis. Pellentesque efficitur elit vitae nisl tincidunt tempor. Cras efficitur, lectus eget posuere vulputate, nibh felis gravida ipsum, nec aliquet felis elit ut elit. Ut gravida dui quis tortor mattis, id posuere metus varius. Mauris rhoncus arcu eu elit lacinia eleifend. In sem neque, rutrum a lectus quis, mollis aliquet mauris. Morbi ac ex ornare nibh interdum consectetur.In ornare velit non diam venenatis mattis.Etiam pellentesque dictum nunc,id pretium quam.Fusce mauris turpis,laoreet nec neque non,sagittis sodales lectus.Ut volutpat ex cursus risus finibus,sit amet ornare nisi gravida.Phasellus congue neque ut condimentum accumsan.",
                -imgPath = imgResourcePath + "/ANFIB_BG1",
                -titleColor = newColor1a,
                -mainColor = newColor1b,
                mainTextColor = textColorA,
                -fullTextColor = textColorA,

                -subDescription_1 = "Aditional info number 1",
                subDescriptionTextColor_1 = newColor1b,
                -subColor_1 = newColor1c,

                subDescription_2 = "Aditional info number 2",
                subDescriptionTextColor_2 = newColor1c,
                subColor_2 = newColor1a,

                subDescription_3 = "Aditional info number 3",
                subDescriptionTextColor_3 = newColor1a,
                subColor_3 = newColor2b,
                */


	}


	public class SQLImgData : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public string type { get; set;}
		public int imageBGId { get; set;}
		public string nameText { get; set;}
		public string titleColor { get; set;}
		public string nameCientText { get; set;}
		public string textA { get; set;}
		public string textB { get; set;}
		public string textC { get; set;}
		public string textD { get; set;}
		public string colorA { get; set;}
		public string colorB { get; set;}
		public string colorC { get; set;}
		public string colorD { get; set;}


		public SQLImgData()
		{

		}

		public SQLImgData(int _id, string _hash, string _type, int _imageBGId, string _nameText, string _titleColor, string _nameCientText, string _textA, string _textB, string _textC, string _textD, string _colorA, string _colorB, string _colorC, string _colorD)
		{
			id = _id;
			hash = _hash;
			type = _type;
			imageBGId = _imageBGId;
			nameText = _nameText;
			titleColor = _titleColor;
			nameCientText = _nameCientText;
			textA = _textA;
			textB = _textB;
			textC = _textC;
			textD = _textD;
			colorA = _colorA;
			colorB = _colorB;
			colorC = _colorC;
			colorD = _colorD;
		}
		/*


	  "hash": "06973c4c293faa0d6bf3740e9b7664c7",
      "type": "flora",
      "enabled": 1,
      "imageBG_id": 207,
      "nameText": "Planta",
      "nameCientText": "Plantaulis naturalis",
      "TextA": "Lorem Ipsum mortis",
      "TextB": "Lorem Ipsum mortis",
      "TextC": "Lorem Ipsum mortis",
      "TextD": "Lorem Ipsum mortis",
      "titleColor": "#549379",
      "ColorA": "#2b686b",
      "ColorB": "#c8d9d8",
      "ColorC": "#f05323",
      "ColorD": "#2b686b"
		*/

	}

	public class SQLImage : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public string imgUrl { get; set;}
		public string imgLocalPath { get; set;}

		public SQLImage()
		{

		}

		public SQLImage(int _id, string _hash, string _imgUrl)
		{
			id = _id;
			hash = _hash;
			imgUrl = _imgUrl;
			imgLocalPath = string.Empty;
		}
	}


	public class SQLMediaContent : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public string url { get; set;}
		public string localPath { get; set;}
		public string mimeType { get; set;}

		public SQLMediaContent()
		{

		}

		public SQLMediaContent(int _id, string _hash, string _url, string _mimeType)
		{
			id = _id;
			hash = _hash;
			url = _url;
			localPath = string.Empty;
			mimeType = _mimeType;
		}
	}
		

	public class SQLVideoFormView : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public int miniBGId { get; set;}
		public int fullBGId { get; set;} 
		public int videoId { get; set;}
		public string tag { get; set;}
		public string titleDescText { get; set;}
		public string descText { get; set;}
		public string textDireccion { get; set;}
		public string textGuion { get; set;}
		public string textProduccion { get; set;}

		public SQLVideoFormView()
		{

		}

		public SQLVideoFormView(int _id, string _hash, int _miniBGId, int _fullBGId, int _videoId, string _tag, string _titleDescText, string _descText, string _textDireccion, string _textGuion, string _textProduccion)
		{
			id = _id;
			hash = _hash;
			miniBGId = _miniBGId;
			fullBGId = _fullBGId; 
			videoId = _videoId;
			tag = _tag;
			titleDescText = _titleDescText;
			descText = _descText;
			textDireccion = _textDireccion;
			textGuion = _textGuion;
			textProduccion = _textProduccion;
		}

		/* JSON
		"id": 1,
		"hash": "1de59697ceea79792e6b191b49523d1b",
		"type": "video",
		"enabled": 1,
		"miniBG_id": 213,
		"fullBG_id": 214,
		"video_id": 217,
		"tag": "Ciencia",
		"titleDescText": "Titulo del video",
		"DescText": "Descripcion del video",
		"TextDireccion": "Lorem Ipsum mortis",
		"TextGuion": "Lorem Ipsum mortis",
		"TextProduccion": "Lorem Ipsum mortis",
		"created_at": "2017-09-18 19:07:15",
		"updated_at": "2017-11-09 18:39:17"
		*/
	}

	public class SQLVideoData : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public int miniBGId { get; set;}
		public int fullBGId { get; set;} 
		public int videoId { get; set;}
		public string tag { get; set;}
		public string titleDescText { get; set;}
		public string descText { get; set;}
		public string textDireccion { get; set;}
		public string textGuion { get; set;}
		public string textProduccion { get; set;}

		public SQLVideoData()
		{

		}

		public SQLVideoData(int _id, string _hash, int _miniBGId, int _fullBGId, int _videoId, string _tag, string _titleDescText, string _descText, string _textDireccion, string _textGuion, string _textProduccion)
		{
			id = _id;
			hash = _hash;
			miniBGId = _miniBGId;
			fullBGId = _fullBGId; 
			videoId = _videoId;
			tag = _tag;
			titleDescText = _titleDescText;
			descText = _descText;
			textDireccion = _textDireccion;
			textGuion = _textGuion;
			textProduccion = _textProduccion;
		}
	}


	public class SQLInunSequiaData : ISQLItem
	{
		[PrimaryKey]
		public int id { get; set;}
		public string hash { get; set;}
		public string status { get; set;}
		public string generalTitle { get; set;} 
		public string generalText { get; set;}
		public string dynamicText { get; set;}

		public SQLInunSequiaData()
		{

		}

		public SQLInunSequiaData(int _id, string _hash, string _status, string _generalTitle, string _generalText, string _dynamicText)
		{
			id = _id;
			hash = _hash;
			status = _status;
			generalTitle = _generalTitle;
			generalText = _generalText;
			dynamicText = _dynamicText;
		}
	}

}