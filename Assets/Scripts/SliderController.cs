﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderController : MonoBehaviour {

    [Header("Tiempo de espera para mover la palanca")]
    public float waitTime = 5f;

    [Header("Handlers de Objetos interactivos")]
    public UIControllerInundSeq handlerBajo;
    public UIControllerInundSeq handlerMedio;
    public UIControllerInundSeq handlerAlto;

    // All state
    public enum EstadosRio { alto, medio, bajo};

    // Current State
    [SerializeField]
    public EstadosRio ActiveState = EstadosRio.medio;

    [SerializeField]
    public EstadosRio cacheState = EstadosRio.medio;

    [Header("Animator que controlas las animaciones generales del bloque")]
    public Animator generalAnimator;

    [Header("Mini animators correspondientes a animaciones varias")]
    public Animator carpinchoAnimator;
    public Animator ciguenaAnimator;
    public Animator hojaSecasAnimator;
    public Animator bichitosAnimator;
    //public Animator cardenalAnimator;

    [Header("Animaciones para obtener duracion")]
    public AnimationClip animAltoBajo;
    public AnimationClip animMedioBajo;
    public AnimationClip animMedioAlto;
    public AnimationClip animAltoMedio;
    public AnimationClip animBajoMedio;
    public AnimationClip animBajoAlto;

    [Header("Animator que controla animaciones del puntero")]
    public Animator pointerAnimator;


    [Header("Animator para colores de los estados del nivel de rio")]
    public Animator selectorAnimator;

    [Header("Animator para setear transicion del texto")]
    public Animator textAnimator;

    [Header("Campo de Titulo que se modificara por CMS")]
    [SerializeField]
    private TextMeshProUGUI campoTitulo;
    public TextMeshProUGUI CampoTitulo
    {
        get
        {
            return campoTitulo;
        }
        set
        {
            campoTitulo = value;
        }
    }

    [Header("Campo de Descripcion de Estado que se modificara por CMS")]
    [SerializeField]
    private TextMeshProUGUI campoDescripcion;
    public TextMeshProUGUI CampoDescripcion
    {
        get
        {
            return campoDescripcion;
        }
        set
        {
            campoDescripcion = value;
        }
    }

    [HideInInspector]
    public string cacheAltoTitulo;
    [HideInInspector]
    public string cacheAltoDesc;

    [HideInInspector]
    public string cacheMedioTitulo;
    [HideInInspector]
    public string cacheMedioDesc;

    [HideInInspector]
    public string cacheBajoTitulo;
    [HideInInspector]
    public string cacheBajoDesc;

    private Slider mySlider;

    private int myValue;

    private bool switched = false;

    //public enum sliderStates {first, second, third };
    //public sliderStates myStates;

    private bool valueFirst = true;
    private bool valueSecond = true;
    private bool valueThird = true;


    // tiempo que tarda en moverse desde el punto de inicio hasta el punto de llegada
    [HideInInspector]
    float timeTakenDuringLerp = 1f;
    // Variable donde guardaremos el tiempo desde que comenzo el lerp
    [HideInInspector]
    private float _timeStartedLerping;


    // animate the game object from -1 to +1 and back
    public float minimum = 0f;
    public float maximum = 1F;

    // starting value for the Lerp
    static float t = 0.0f;

    float tParam = 0f;
    //float valToBeLerped = 0f;
    float speed = 0.3f;

    // Controla que se este arrastrando el slider
    private bool isDragging = false;

    private bool isSetted = false;


    private bool animationTriggered = false;


    //private float valueField;

    // Use this for initialization
    void Start () {

    cacheAltoTitulo = "test 1";
    cacheAltoDesc = "test 1";

    cacheMedioTitulo = "test 2";
    cacheMedioDesc = "test 2";

    cacheBajoTitulo = "test 3";
    cacheBajoDesc = "test 3";


    mySlider = gameObject.GetComponent<Slider>();

        //ST// mySlider.wholeNumbers = true;

        //ST// mySlider.onValueChanged.AddListener(ListenerMethod);


        //myValue = (int)mySlider.value;

        //myStates = sliderStates.first;

        //valueField = this.gameObject.GetComponent<Slider>().value;

        mySlider.value = 2;

        ChangeState(EstadosRio.medio);

        

    }



    void Update()
    {


        if (isDragging)
        {
            if (this.gameObject.GetComponent<Slider>().value > 1 && this.gameObject.GetComponent<Slider>().value < 1.5)
            {
                //if (tParam < 1)
                //{
                //tParam += Time.deltaTime * speed; //This will increment tParam based on Time.deltaTime multiplied by a speed multiplier
                this.gameObject.GetComponent<Slider>().value = Mathf.Lerp(this.gameObject.GetComponent<Slider>().value, 1, 0.5f);
                //}

                if (this.gameObject.GetComponent<Slider>().value <= 1.25f)
                //if (this.gameObject.GetComponent<Slider>().value == 1)
                {
                    this.gameObject.GetComponent<Slider>().value = 1;
                    pointerAnimator.SetTrigger("updown");
                    //print("MOVING TO TOP");
                    isSetted = false;

                }

            }

            if ((this.gameObject.GetComponent<Slider>().value > 1.5 && this.gameObject.GetComponent<Slider>().value < 2))
            {
                //if (tParam < 1)
                //{
                //tParam += Time.deltaTime * speed; //This will increment tParam based on Time.deltaTime multiplied by a speed multiplier
                this.gameObject.GetComponent<Slider>().value = Mathf.Lerp(this.gameObject.GetComponent<Slider>().value, 2, 0.5f);
                //}

                if (this.gameObject.GetComponent<Slider>().value >= 1.75f)
                //if (this.gameObject.GetComponent<Slider>().value == 2)
                {
                    this.gameObject.GetComponent<Slider>().value = 2;
                    pointerAnimator.SetTrigger("downup");
                    //print("MOVING TOP TO CENTER");

                    isSetted = false;


                }

            }


            if ((this.gameObject.GetComponent<Slider>().value > 2 && this.gameObject.GetComponent<Slider>().value < 2.5))
            {
                //if (tParam < 1)
                //{
                //tParam += Time.deltaTime * speed; //This will increment tParam based on Time.deltaTime multiplied by a speed multiplier
                this.gameObject.GetComponent<Slider>().value = Mathf.Lerp(this.gameObject.GetComponent<Slider>().value, 2, 0.5f);
                //}

                if (this.gameObject.GetComponent<Slider>().value <= 2.25f)
                //if (this.gameObject.GetComponent<Slider>().value == 2)
                {
                    this.gameObject.GetComponent<Slider>().value = 2;
                    pointerAnimator.SetTrigger("updown");
                    //print("MOVING BOTTOM TO CENTER");

                    isSetted = false;

                }

            }

            if (this.gameObject.GetComponent<Slider>().value > 2.5 && this.gameObject.GetComponent<Slider>().value < 3)
            {
                //if (tParam < 1)
                //{
                //tParam += Time.deltaTime * speed; //This will increment tParam based on Time.deltaTime multiplied by a speed multiplier
                this.gameObject.GetComponent<Slider>().value = Mathf.Lerp(this.gameObject.GetComponent<Slider>().value, 3, 0.5f);
                //}

                if (this.gameObject.GetComponent<Slider>().value >= 2.75f)
                //if (this.gameObject.GetComponent<Slider>().value == 3)
                {
                    this.gameObject.GetComponent<Slider>().value = 3;
                    pointerAnimator.SetTrigger("downup");
                    //print("MOVING TO BOTTOM");

                    isSetted = false;


                }

            }

        }
    }
    
    public void PointerRelease()
    {
        isDragging = true;
    }

    public void PointerHold()
    {
        isDragging = false;

        
    }


    public void PointerDown()
    {
        if (this.gameObject.GetComponent<Slider>().interactable == false)
        {
            pointerAnimator.SetTrigger("waiting");
        }
    }

   
    public void ListenerMethod()//(float value)
    {
        // Usar if para que el switch no confunda valores enteros con valores flotantes
        if (this.gameObject.GetComponent<Slider>().value == 1)
            isSetted = false;
        if (this.gameObject.GetComponent<Slider>().value == 2)
            isSetted = false;
        if (this.gameObject.GetComponent<Slider>().value == 3)
            isSetted = false;
        /*
        switch ((int)this.gameObject.GetComponent<Slider>().value)
        {
            case 1:

                isSetted = false;

                break;

            case 2:

                isSetted = false;

                break;

            case 3:

                isSetted = false;

                break;

        }
        */
    }
    


    void FixedUpdate()
    {
        

        if (!isSetted)
        {
            switch ((int)this.gameObject.GetComponent<Slider>().value)
            {
                // Estado ALTO
                case 1:
                    // Transicion de Medio a Alto
                    if (cacheState == EstadosRio.medio)
                    {
                        StartCoroutine(EstadoMedioAlto());
                    }

                    // Transicion de Bajo a Alto
                    if (cacheState == EstadosRio.bajo)
                    {
                        StartCoroutine(EstadoBajoAlto());
                    }


                    cacheState = EstadosRio.alto;

                    //switched = !switched;
                    //switched = !switched;
                    print("value 1 slided");

                    selectorAnimator.SetBool("alto", true);
                    selectorAnimator.SetBool("medio", false);
                    selectorAnimator.SetBool("bajo", false);

                    //ActiveState = EstadosRio.alto;

                    StartCoroutine(ChangeState(EstadosRio.alto));

                    //isDragging = false;

                    isSetted = true;

                    

                    textAnimator.SetTrigger("appear");

                    campoTitulo.text = cacheAltoTitulo;
                    campoDescripcion.text = cacheAltoDesc;

                    break;

                // Estado MEDIO
                case 2:

                    // Transicion de Alto a Medio
                    if (cacheState == EstadosRio.alto)
                    {
                        StartCoroutine(EstadoAltoMedio());
                                                    

                    }
                    // Transicion de Bajo a Medio
                    if (cacheState == EstadosRio.bajo)
                    {
                        StartCoroutine(EstadoBajoMedio());
                    }


                    cacheState = EstadosRio.medio;


                    //switched = !switched;
                    //print("value 2 slided");
                    print("value 2 slided");


                    selectorAnimator.SetBool("alto", false);
                    selectorAnimator.SetBool("medio", true);
                    selectorAnimator.SetBool("bajo", false);

                    //ActiveState = EstadosRio.medio;

                    StartCoroutine(ChangeState(EstadosRio.medio));

                    //isDragging = false;

                    isSetted = true;

                    

                    textAnimator.SetTrigger("appear");

                    campoTitulo.text = cacheMedioTitulo;
                    campoDescripcion.text = cacheMedioDesc;

                    break;

                // Estado BAJO
                case 3:

                    // Trasicion de Alto a Bajo
                    if (cacheState == EstadosRio.alto)
                    {

                        StartCoroutine(EstadoAltoBajo());

                    }

                    // Transicion de Medio a Bajo
                    if (cacheState == EstadosRio.medio)
                    {
                        StartCoroutine(EstadoMedioBajo());
                    }


                    cacheState = EstadosRio.bajo;

                    //switched = !switched;
                    //print("value 3 slided");
                    print("value 3 slided");


                    selectorAnimator.SetBool("alto", false);
                    selectorAnimator.SetBool("medio", false);
                    selectorAnimator.SetBool("bajo", true);

                    //ActiveState = EstadosRio.bajo;

                    StartCoroutine(ChangeState(EstadosRio.bajo));

                    //isDragging = false;

                    isSetted = true;

                    

                    textAnimator.SetTrigger("appear");

                    campoTitulo.text = cacheBajoTitulo;
                    campoDescripcion.text = cacheBajoDesc;

                    break;

            }

        }
        

        /*
        if(value > 1 && value < 1.5)
        {

            print("VALOR ENTRE 1 y 1.5");

            //float percentageComplete = _timeStartedLerping / timeTakenDuringLerp;


            //float _preSnapPosition = Mathf.Lerp(value, 1, percentageComplete);


            //value = Mathf.CeilToInt(_preSnapPosition);

            value = Mathf.Lerp(value, 1, Time.deltaTime);
                    
        }
        */
        /*
        if (value > 1 && value < 1.5)
        {
            print("VALOR EN EL MEDIO");
            // animate the position of the game object...
            value = Mathf.Lerp(value, maximum, t);

            // .. and increate the t interpolater
            t += 0.5f * Time.deltaTime;

            // now check if the interpolator has reached 1.0
            // and swap maximum and minimum so game object moves
            // in the opposite direction.
            if (t > 1.0f)
            {
                float temp = maximum;
                maximum = minimum;
                minimum = temp;
                t = 0.0f;
            }
        }
        */
    }


    public IEnumerator ChangeState(EstadosRio state)
    {
        // Check the state variable
        switch (state)
        {
            case EstadosRio.alto:
                {
                    print("SELECTED ESTADO ALTO");
                    ActiveState = EstadosRio.alto;
                    this.gameObject.GetComponent<Slider>().interactable = false;
                    //pointerAnimator.SetBool("waiting", true);
                    yield return new WaitForSeconds(waitTime);
                    this.gameObject.GetComponent<Slider>().interactable = true;
                    //pointerAnimator.SetBool("waiting", false);
                }
                break;

            case EstadosRio.medio:
                {
                    print("SELECTED ESTADO MEDIO");
                    ActiveState = EstadosRio.medio;
                    this.gameObject.GetComponent<Slider>().interactable = false;
                    //pointerAnimator.SetBool("waiting", true);
                    yield return new WaitForSeconds(waitTime);
                    this.gameObject.GetComponent<Slider>().interactable = true;
                    //pointerAnimator.SetBool("waiting", false);
                }
                break;

            case EstadosRio.bajo:
                {
                    print("SELECTED ESTADO BAJO");
                    ActiveState = EstadosRio.bajo;
                    this.gameObject.GetComponent<Slider>().interactable = false;
                    //pointerAnimator.SetBool("waiting", true);
                    yield return new WaitForSeconds(waitTime);
                    this.gameObject.GetComponent<Slider>().interactable = true;
                    //pointerAnimator.SetBool("waiting", false);
                }
                break;
        }
    }



    public IEnumerator EstadoAltoMedio()
    {


        if(handlerAlto != null)
        handlerAlto.GetComponent<UIControllerInundSeq>().ClosePage(handlerAlto.GetComponent<UIControllerInundSeq>().currentPageIndex);// currentPage.gameObject.GetComponent<Animator>().SetTrigger("hide");

        yield return new WaitForSeconds(1.25f);
        handlerAlto.GetComponent<UIControllerInundSeq>().currentPage.gameObject.SetActive(false);

        handlerAlto.gameObject.SetActive(false);
        handlerMedio.gameObject.SetActive(true);

        //carpinchoAnimator.SetTrigger("idle");



        bichitosAnimator.SetTrigger("exiting");


        generalAnimator.SetTrigger("AltoMedio");

        //yield return new WaitForSeconds(animAltoMedio.length);

        yield return new WaitForSeconds(1f);
        //carpinchoAnimator.SetTrigger("comiendo");
        carpinchoAnimator.SetBool("swimming", false);

        // Todo hacer que se cierre con la funcion de Close




        yield return null;
    }

    public IEnumerator EstadoAltoBajo()
    {
        if (handlerAlto != null)
        handlerAlto.GetComponent<UIControllerInundSeq>().ClosePage(handlerAlto.GetComponent<UIControllerInundSeq>().currentPageIndex);

        yield return new WaitForSeconds(1.25f);
        handlerAlto.GetComponent<UIControllerInundSeq>().currentPage.gameObject.SetActive(false);

        handlerAlto.gameObject.SetActive(false);
        handlerBajo.gameObject.SetActive(true);

        //carpinchoAnimator.SetTrigger("idle");



        bichitosAnimator.SetTrigger("exiting");


        generalAnimator.SetTrigger("AltoBajo");

        //yield return new WaitForSeconds(pointerAnimator.GetCurrentAnimatorStateInfo(0).length);

        yield return new WaitForSeconds(1f);
        carpinchoAnimator.SetBool("swimming", false);



        yield return new WaitForSeconds(animAltoBajo.length);

        hojaSecasAnimator.SetBool("moving", true);

        ciguenaAnimator.gameObject.GetComponent<Animator>().enabled = true;
        ciguenaAnimator.SetBool("moving", true);
        // Normalized time para poner la animacion de nuevo al inicio
        ciguenaAnimator.GetComponent<Animator>().Play("comiendo", 0, 0);


        //carpinchoAnimator.SetTrigger("comiendo");




        yield return null;

    }

    public IEnumerator EstadoMedioAlto()
    {
        if(handlerMedio != null)
        handlerMedio.GetComponent<UIControllerInundSeq>().ClosePage(handlerMedio.GetComponent<UIControllerInundSeq>().currentPageIndex);

        yield return new WaitForSeconds(1.25f);
        handlerMedio.GetComponent<UIControllerInundSeq>().currentPage.gameObject.SetActive(false);

        handlerMedio.gameObject.SetActive(false);
        handlerAlto.gameObject.SetActive(true);


        //carpinchoAnimator.SetTrigger("nadando");
        carpinchoAnimator.SetBool("swimming", true);


        bichitosAnimator.SetTrigger("entering");



        generalAnimator.SetTrigger("idleMedioAlto");


        //yield return new WaitForSeconds(animMedioAlto.length);

        // Resetea la animacion de comida de la garza
        ciguenaAnimator.SetBool("moving", false);
        ciguenaAnimator.gameObject.GetComponent<Animator>().enabled = false;



        yield return null;

    }

    public IEnumerator EstadoMedioBajo()
    {
        if(handlerMedio != null)
        handlerMedio.GetComponent<UIControllerInundSeq>().ClosePage(handlerMedio.GetComponent<UIControllerInundSeq>().currentPageIndex);

        yield return new WaitForSeconds(1.25f);
        handlerMedio.GetComponent<UIControllerInundSeq>().currentPage.gameObject.SetActive(false);


        handlerMedio.gameObject.SetActive(false);
        handlerBajo.gameObject.SetActive(true);

        //carpinchoAnimator.SetTrigger("idle");



        generalAnimator.SetTrigger("idleMedioBajo");

        //yield return new WaitForSeconds(pointerAnimator.GetCurrentAnimatorStateInfo(0).length);

        yield return new WaitForSeconds(animMedioBajo.length);

        hojaSecasAnimator.SetBool("moving", true);

        ciguenaAnimator.gameObject.GetComponent<Animator>().enabled = true;
        ciguenaAnimator.SetBool("moving", true);
        // Normalized time para poner la animacion de nuevo al inicio
        ciguenaAnimator.GetComponent<Animator>().Play("comiendo", 0, 0);


        //carpinchoAnimator.SetTrigger("comiendo");
        carpinchoAnimator.SetBool("swimming", false);





        yield return null;

    }

    public IEnumerator EstadoBajoMedio()
    {
        if(handlerBajo != null)
        handlerBajo.GetComponent<UIControllerInundSeq>().ClosePage(handlerBajo.GetComponent<UIControllerInundSeq>().currentPageIndex);

        yield return new WaitForSeconds(1.25f);
        handlerBajo.GetComponent<UIControllerInundSeq>().currentPage.gameObject.SetActive(false);

        handlerBajo.gameObject.SetActive(false);
        handlerMedio.gameObject.SetActive(true);


        hojaSecasAnimator.SetBool("moving", false);

        ciguenaAnimator.SetBool("moving", false);
        ciguenaAnimator.gameObject.GetComponent<Animator>().enabled = false;


        //carpinchoAnimator.SetTrigger("idle");



        // Seteo estado
        generalAnimator.SetTrigger("BajoMedio");

        yield return new WaitForSeconds(animBajoMedio.length);

        //carpinchoAnimator.SetTrigger("comiendo");
        carpinchoAnimator.SetBool("swimming", false);





        yield return null;
    }

    public IEnumerator EstadoBajoAlto()
    {
        if(handlerBajo != null)
        handlerBajo.GetComponent<UIControllerInundSeq>().ClosePage(handlerBajo.GetComponent<UIControllerInundSeq>().currentPageIndex);

        yield return new WaitForSeconds(1.25f);
        handlerBajo.GetComponent<UIControllerInundSeq>().currentPage.gameObject.SetActive(false);

        handlerBajo.gameObject.SetActive(false);
        handlerAlto.gameObject.SetActive(true);



        //carpinchoAnimator.SetTrigger("nadando");

        carpinchoAnimator.SetBool("swimming", true);

        hojaSecasAnimator.SetBool("moving", false);
        ciguenaAnimator.SetBool("moving", false);
        ciguenaAnimator.gameObject.GetComponent<Animator>().enabled = false;



        bichitosAnimator.SetTrigger("entering");


        // Setea estado
        generalAnimator.SetTrigger("BajoAlto");

        //yield return new WaitForSeconds(animBajoAlto.length);



        yield return null;
    }


    /*
    //Funcion que controla snap en puntos de canvas
    IEnumerator Snap()
    {
        for (int i = 0; i < snapTransform.Length; i++)
        {

            // Checkeo que el centro de la camara este dentro del Area A,B,C,etc , si es asi ejecutar el drag
            if (transform.position.x > snapXAreaMin[i] && transform.position.x < snapXAreaMax[i] && transform.position.y > snapYAreaMin[i] && transform.position.y < snapYAreaMax[i])
            {

                // Si es posible ejecutar el snapping previene de no hacer drag
                doDrag = false;


                //float timeSinceStarted = Time.time - _timeStartedLerping;
                float percentageComplete = _timeStartedLerping / timeTakenDuringLerp;


           


                Vector2 _preSnapPosition = Vector3.Lerp(transform.position, snapTransform[i].transform.position, percentageComplete / 3);

                transform.position = new Vector3(

                    Mathf.CeilToInt
                    (_preSnapPosition.x),
                    Mathf.CeilToInt
                    (_preSnapPosition.y),
                    -30);

                print("PORCENTAGE: " + percentageComplete);

                yield return null;

            }

        }

    }
    */

}
