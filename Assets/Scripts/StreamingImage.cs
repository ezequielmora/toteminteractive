﻿using UnityEngine;
using System.Collections;
using SQL.Structs;

[System.Serializable]
public class StreamingImage{

    public int id;
    public string hash;
    public string imgUrl;
    public string imgLocalPath;

    public StreamingImage()
    {
        
    }

    public StreamingImage(int _id, string _hash, string _imgUrl, string _imgLocalPath)
    {
        id = _id;
        hash = _hash;
        imgUrl = _imgUrl;
        imgLocalPath = _imgLocalPath;
    }

    public StreamingImage(SQLImage img)
    {
        id = img.id;
        hash = img.hash;
        imgUrl = img.imgUrl;
        imgLocalPath = img.imgLocalPath;
    }
	
}
