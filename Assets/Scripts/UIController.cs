﻿using EnhancedScrollerDemos.CellControllerInstitucional;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public RectTransform alphaLayer;

    [Header("Objeto de Loading UI")]
    public GameObject canvasLoading;

    [Header("AnimationClip que tiene duracion de aparicion de la ficha global")]
    [SerializeField]
    public AnimationClip clipEntrada;

    [SerializeField]
    private float fadeProportion = 0.75F;

    private Image imageComponent;


    public List<GameObject> pages = new List<GameObject>();


    public bool cameraInPlace = false;

    public bool detectChildAnimation = false;


    public int currentPageIndex = 0;
    public GameObject currentPage;
    public CanvasGroup cg;

    public bool fadingOut = false;

    public bool fadingIn = false;

    public float fadeRate = 8f;
    public float growRate = 0.5f;

    private bool transparent = false;

    private bool doButtonSnap = false;

    public GameObject expandTextWindow;

 

    [Header("Tiempo que tarda en abrir nueva ventana")]
    public bool delayedStart = false;

    [Header("Grupo de animadores dentro del conteiner de cada bloque")]
    public Animator[] animatorGroup;


    //private bool allDataLoaded = false;

    //[SerializeField]
    //RectTransform snapPopArea;

    // Use this for initialization
    void Start () {

        // Arranca Inicio de controlador de UI Management
        StartCoroutine(Init());
		
	}
	
	// Update is called once per frame
	void Update () {
        // dismininuye la opacidad (aumenta transparencia)
        if (fadingOut)
            cg.alpha -= fadeRate * Time.deltaTime;

            
        if (fadingIn)
        {
            // aumenta la opacidad
            cg.alpha += fadeRate * Time.deltaTime;
            // Cambio de escala con cambio de pagina
            currentPage.transform.localScale += Vector3.one * growRate * Time.deltaTime;

            

        }

        /*
        if (doButtonSnap)
        {

            ButtonSnap();

        }
        */


    }

    IEnumerator Init()
    {
        /*
        while( 
            !PagedInstitucionalHandler.Instance.feedInstitucionalLoaded &&
            !PagedGlobalHandler.Instance.feedGlobalLoaded &&
            !PagedVideoHandler.Instance.feedVideosLoaded)
            {
                yield return null;
            }
        */

        //yield return new WaitUntil(() => allDataLoaded == true);

        imageComponent = alphaLayer.GetComponent<Image>();

        imageComponent.color = new Color(0f, 0f, 0f, 0f);

        //cg = currentPage.GetComponent<CanvasGroup>();

        cg = cg.GetComponent<CanvasGroup>();

        
        // Espera la carga de datos antes de ocultas las fichas
        if (ContentHandler.Instance != null)
        {
            yield return new WaitUntil(() => ContentHandler.Instance.isLoaded == true);

            //yield return new WaitUntil(() => SelectorVideoHelper.Instance.isLoaded == true);

        }
        // Setea elementos de la lista en falso luego de cargar las cells views
        
        // ST
        
        yield return new WaitForSeconds(30f);

        /*
        yield return StartCoroutine(SelectorVideoHelper.Instance.DisableAllColor());

        yield return StartCoroutine(SelectorVideoHelper.Instance.SelectorCreation());



        yield return StartCoroutine(SelectorGlobalHelper.Instance.DisableAllColor());

        yield return StartCoroutine(SelectorGlobalHelper.Instance.SelectorCreation());
        */
        if(canvasLoading != null)
        canvasLoading.SetActive(false);
        

        for (int i = 0; i < pages.Count; i++)
        {
            pages[i].SetActive(false);
        }
        


        yield return null;
    }


    /*
    public void FixedUpdate()
    {
        if(PagedGlobalHandler.Instance.feedGlobalLoaded && PagedVideoHandler.Instance.feedVideosLoaded && PagedVideoHandler.Instance.feedVideosLoaded)
        {
            allDataLoaded = true;
        }
    }
    */


    //TODO: LOGICA PARA Cambio de pagina en base a cambio canvas, llama esta funcion el boton de la pagina, pasando como parametro ese index de pagina (primera 0)
    public void NewPage(int newPage)
    {

        StartCoroutine("NewPageCoroutine", newPage);
        
        //Bloquea el raycast
        cg.blocksRaycasts = true;

    }


    public IEnumerator NewPageCoroutine(int newPage)
    {

        // solo cambia la pagina si no se esta sobre la pagina actual
        // cambio comportamiento para que si esta cerrada o abierta lo ejecute igualmente

        //if(newPage == 0 && currentPage.active == false )
        if (newPage == currentPageIndex && currentPage.active == false)
        {

            // TODO Poner delay en caso de botones Globales
            if (delayedStart)
                yield return new WaitForSeconds(1f);

            StartCoroutine("ChangePage", newPage);


            if (!transparent)
            {

                


                // Fade del fondo en capa de transicion separada
                StartCoroutine(FadeOut());

                // MOVER BLOQUE PARA NO IMPIDA MOVIMIENTO
                // Si existe una transicion impide el movimiento de la camara
                CameraDragController.instance.allowFollow = false;
                print(CameraDragController.instance.allowFollow);

                //Bloquea el raycast
                //cg.blocksRaycasts = true;


                if (!cameraInPlace)
                {
                    // Cuando el usuario hace click en un boton (fuera del limite) automaticamente hace el snap a la ventana correspondiente al mismo
                    //doButtonSnap = true;
                    ButtonSnap(newPage);

                }
                else
                {
                    // Si esta chequeado la variable cameraInPlace la posicion de la ventana se define como centro de la posicion de la camara
                    //currentPage.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, currentPage.transform.position.z);
                    pages[newPage].transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, currentPage.transform.position.z);

                }

            }



        }


        else if (newPage != currentPageIndex)
        {

            // TODO Poner delay en caso de botones Globales
            if (delayedStart)
                yield return new WaitForSeconds(1f);

            StartCoroutine("ChangePage", newPage);


            if (!transparent)
            {

                

                // sacar transicion si cambia una activa por otra activa     
                StartCoroutine(FadeOut());

                // MOVER BLOQUE PARA NO IMPIDA MOVIMIENTO
                // Si existe una transicion impide el movimiento de la camara
                CameraDragController.instance.allowFollow = false;
                print(CameraDragController.instance.allowFollow);

                //Bloquea el raycast
                cg.blocksRaycasts = true;



                if (!cameraInPlace)
                {
                    // Cuando el usuario hace click en un boton (fuera del limite) automaticamente hace el snap a la ventana correspondiente al mismo
                    //doButtonSnap = true;
                    ButtonSnap(newPage);

                }
                else
                {
                    // Si esta chequeado la variable cameraInPlace la posicion de la ventana se define como centro de la posicion de la camara
                    //currentPage.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, currentPage.transform.position.z);
                    pages[newPage].transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, currentPage.transform.position.z);


                }

            }
        }


        
    }

    public void OpenExpandText()
    {


        expandTextWindow.SetActive(true);


    }


    public void CloseExpandText()
    {


        expandTextWindow.SetActive(false);


    }



    //TODO: Logica para cerrar con cruz ventana o pop up
    public void ClosePage(int thisPageNumber)
    {


        if (thisPageNumber == currentPageIndex)
        {

            // cierra la pagina y vuelve a la opacidad normal del paño
            StartCoroutine("ClosingPage", thisPageNumber);
            // Mover de lugar
            // StartCoroutine(FadeIn());

            // cuando se elimina la transicion vuelve a habilitar el movimiento de camara
            CameraDragController.instance.allowFollow = true;
            print(CameraDragController.instance.allowFollow);


        }


    }
    public void ButtonSnap(int page)
    {

        CameraDragController.instance.doDrag = false;
        CameraDragController.instance.doDrag = false;


        for (int i = 0; i <= pages.Count; i++)
        {

            // currentPage es el numero de pagina pero con la referencia de tipo gameobject
            Vector2 _preSnapButtonPosition = Vector2.Lerp(CameraDragController.instance.gameObject.transform.position, pages[page].transform.position, Time.deltaTime * 50);

            CameraDragController.instance.gameObject.transform.position = new Vector3(

                Mathf.CeilToInt
                (_preSnapButtonPosition.x),
                Mathf.CeilToInt
                (_preSnapButtonPosition.y),
                -25);

            print("DOING BUTTON SNAP!!!" + pages[page].transform.position);

            doButtonSnap = false;

        }

        /*
        foreach (var item in pages)
        {
            // currentPage es el numero de pagina pero con la referencia de tipo gameobject
            Vector2 _preSnapButtonPosition = Vector2.Lerp(CameraDragController.instance.gameObject.transform.position, item.transform.position, Time.deltaTime * 50);

            CameraDragController.instance.gameObject.transform.position = new Vector3(

                Mathf.CeilToInt
                (_preSnapButtonPosition.x),
                Mathf.CeilToInt
                (_preSnapButtonPosition.y),
                -25);

            print("DOING BUTTON SNAP!!!" + item.transform.position);

            doButtonSnap = false;
        }
        */

        /*
        // currentPage es el numero de pagina pero con la referencia de tipo gameobject
        Vector2 _preSnapButtonPosition = Vector2.Lerp(CameraDragController.instance.gameObject.transform.position, snapPopArea.transform.position, Time.deltaTime * 50);

        CameraDragController.instance.gameObject.transform.position = new Vector3(

            Mathf.CeilToInt
            (_preSnapButtonPosition.x),
            Mathf.CeilToInt
            (_preSnapButtonPosition.y),
            -25);

        print("DOING BUTTON SNAP!!!" + snapPopArea.transform.position);

        doButtonSnap = false;
        */

    }


    //Corrutina de cambio de pagina, pasando como parametro ese numero de pagina
    public IEnumerator ChangePage(int newPage)
    {
        


        //Cierra la pagina actual

        // El grupo de canvas debe poseer un componente de layout del tipo canvas group
        //cg = currentPage.GetComponent<CanvasGroup>();

        // SET CODE ANIMATION // DISABLE
        /*
        cg.alpha = 1f;
        fadingIn = false;
        fadingOut = true;

        while (cg.alpha > 0)
        {
            yield return 0;
        }
        */
        currentPage.SetActive(false);

        //Abre la nueva pagina

        
        // SET CODE ANIMATION // DISABLE
        /*
        fadingIn = true;
        fadingOut = false;
        */

        // Pasa como pagina de index del grupo la nueva pagina ingresada
        currentPageIndex = newPage;
        // Pasa como pagina la activa la pagina ingresada
        currentPage = pages[currentPageIndex];


        //if (delayedStart)
        //yield return new WaitForSeconds(1.5f);


        currentPage.SetActive(true);
        // Activa trigger del animator
        //currentPage.GetComponent<Animator>().SetTrigger("appear");
        currentPage.GetComponent<Animator>().SetTrigger("hide");

        // Espera un tiempo que finalice la animacion de aparicion de pagina para habilitar drag
        if (clipEntrada != null)
        {
            yield return new WaitForSeconds(clipEntrada.length);
            PagedGlobalHandler.Instance.allowDragging = true;
            PagedVideoHandler.Instance.allowDragging = true;
        }

        //currentPage.GetComponent<ColorAnimatorHelper>().textTitleAnimator.SetTrigger("hide");


        // MANEJAR DESDE ON ENABLE - DEJAR PRIMERA FICHA OCULTA PARA PAGED
        /*
        foreach (var item in PagedInstitucionalHandler.Instance.formulariosInstitucionalesVarios)
        {

            foreach (var itemPages in item.PagedFormulario.Pages)
            {
                itemPages.GetComponent<FichasAnimatorHelper>().Appear();
            }
            
        }
        

        foreach (var item in PagedGlobalHandler.Instance.tiposDeContenido)
        {

            foreach (var itemPages in item.TypeFormPaged.Pages)
            {
                itemPages.GetComponent<FichasAnimatorHelper>().Appear();
            }

            foreach (var itemPages in item.TypeButtonPaged.Pages)
            {
                itemPages.GetComponent<FichasAnimatorHelper>().Appear();
            }

        }
        */

        //TODO// SETEAR LA ANIMACION DE ENTRADA DE TODAS LAS FICHAS DE ESE BLOQUE
        // IF SET ACTIVE EN CellViewInstitucional
        //


        //CONTINUAR// List<Animator> listaFichas =

        /////////////////////////////////////////////////// TODO: CAMBIAR METODO DE CHILD ANIMATION
        /*
        if (detectChildAnimation)
        {

            Transform allFichasContainer = currentPage.transform.Find("Skin/Mask_Scroller/hScroller/Container");
            animatorGroup = new Animator[allFichasContainer.childCount];

            for (int i = 0; i < allFichasContainer.childCount; i++)
            {
                if (allFichasContainer.GetChild(i).gameObject.GetComponent<Animator>() != null)
                {
                    animatorGroup[i] = allFichasContainer.GetChild(i).gameObject.GetComponent<Animator>();
                    animatorGroup[i].SetTrigger("appear");
                    print("GRUPO DE FICHA Nº" + i);
                }
            }

        }
        */
        /*
        cg = currentPage.GetComponent<CanvasGroup>();
        cg.alpha = 0f;
        currentPage.transform.localScale = Vector3.one * 0.95f;

        

        while (cg.alpha < 1f || currentPage.transform.localScale.x < 1f)
        {
            
            yield return 0;

        }

        

        cg.alpha = 1f;
        currentPage.transform.localScale = Vector3.one;

        fadingIn = false;
        */

        //yield return 0;

        yield return null;
    }


    public IEnumerator ClosingPage(int thisPageNumber)
    {
        // Inhabilita el drag de la ventana si la misma esta cerrada
        if (clipEntrada != null)
        {
            PagedGlobalHandler.Instance.allowDragging = false;
            PagedVideoHandler.Instance.allowDragging = false;
        }


        //Solo Cierra la pagina actual

        // El grupo de canvas debe poseer un componente de layout del tipo canvas group
        //cg = currentPage.GetComponent<CanvasGroup>();

        //Desbloquea el raycast
        cg.blocksRaycasts = false;

        //ST PARA SETEO POR ANIMACION
        /*
        cg.alpha = 1f;
        fadingIn = false;
        fadingOut = true;

        while (cg.alpha > 0)
        {
            yield return 0;
        }
        */



        /*
        foreach (var item in pages)
        {
            item.gameObject.GetComponentInChildren<Animator>().SetTrigger("ChangeClose");
            item.gameObject.GetComponentInChildren<Animator>().SetBool("hasOpened", false);
            
        }
        */

        //GameObject.Find("Canvas/InstitucionalInfo")

        // TODO // FUNCIONA CORREGIR VENTANAS ABIENTAS
        //GameObject.FindGameObjectWithTag("fichasInst").GetComponent<Animator>().SetTrigger("ChangeClose");
        //GameObject.FindGameObjectWithTag("fichasInst").GetComponent<Animator>().SetBool("hasOpened", false);

        //<CellViewInstitucional>().btn_ClickMinus();

        // Fix cerrar textos expandidos para el caso en que se encuentren abiertos al cerrar formularios
        /*
        GameObject[] conjuntoFichas = GameObject.FindGameObjectsWithTag("fichasInst");

        foreach (var item in conjuntoFichas)
        {
            item.GetComponent<CellViewInstitucional>().btn_ClickMinus();
        }
            
        */  
            

        // ST - SACAR ESPERA DE CIERRE
        //yield return new WaitForSeconds(0.1f);

        //Fix mover de lugar
        //yield return StartCoroutine(FadeIn());
        
        // Espera a que se complete la corrutina del FedeIn para luego continuar con la siguiente funcion
        //yield return StartCoroutine(FadeIn());
        currentPage.SetActive(false);
        StartCoroutine(FadeIn());

        yield return null;
    }

   


    public IEnumerator FadeOut()
    {
        // Activa el chequeo de que esta activada la capa de transparencia
        transparent = true;
        alphaLayer.gameObject.SetActive(true);

        //NOTE: Valores de alpha son proporciones
        for (float alpha = 0f; alpha < fadeProportion; alpha += Time.deltaTime * 2f)
        {

            //TODO: FADE CAMBIO OPACIDAD
            imageComponent.color = new Color(imageComponent.color.r, imageComponent.color.g, imageComponent.color.b, alpha);

            yield return null;

        }

    }


    public IEnumerator FadeIn()
    {

        // Solo deshabilita el fade por cierre de paginas, no por cambios entre ellas
        transparent = false;
        //alphaLayer.gameObject.SetActive(false);

        //NOTE: Valores de alpha son proporciones
        for (float alpha = fadeProportion; alpha > 0f; alpha -= Time.deltaTime)
        {

            //TODO: FADE CAMBIO OPACIDAD
            imageComponent.color = new Color(imageComponent.color.r, imageComponent.color.g, imageComponent.color.b, alpha);

            yield return null;

            if(alpha == 0)
            {
                alphaLayer.gameObject.SetActive(false);
            }

        }

    }


}
