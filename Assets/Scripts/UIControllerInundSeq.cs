﻿using EnhancedScrollerDemos.CellControllerInstitucional;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControllerInundSeq : MonoBehaviour
{

    //public RectTransform alphaLayer;

    //[Header("Objeto de Loading UI")]
    //public GameObject canvasLoading;

    [Header("AnimationClip que tiene duracion de salida de la ventana interactiva")]
    [SerializeField]
    public AnimationClip clipSalida;

    [Header("AnimationClip que tiene duracion de entrada de la ventana interactiva")]
    [SerializeField]
    public AnimationClip clipEntrada;

    //Permite un unico CLICK del objeto interactivo
    private bool allowButtonClick = true;
    private bool allowButtonClose = true;

    [SerializeField]
    private float fadeProportion = 0.75F;

    private Image imageComponent;


    public List<GameObject> pages = new List<GameObject>();


    public bool cameraInPlace = false;

    public bool detectChildAnimation = false;


    public int currentPageIndex = 0;
    public GameObject currentPage;



    public float fadeRate = 8f;
    public float growRate = 0.5f;

    private bool transparent = false;

    private bool doButtonSnap = false;

    public GameObject expandTextWindow;



    [Header("Tiempo que tarda en abrir nueva ventana")]
    public bool delayedStart = false;

    [Header("Grupo de animadores dentro del conteiner de cada bloque")]
    public Animator[] animatorGroup;

    // Contador de tiempo que se resetea al cerrar las paginas y arranca al abrir nueva
    private float timeCounter = 0;
    private bool timeEnable = false;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        // Devuelve contador en tiempo real
        //print(timeCounter);

        if (timeEnable)
        {
            // Calcula en update el counter con la cantidad de segundos incremental
            timeCounter += Time.deltaTime;
        }

        if(timeCounter > 20)
        {
            ClosePage(currentPageIndex);
        }

    }

   


    /*
    public void FixedUpdate()
    {
        if(PagedGlobalHandler.Instance.feedGlobalLoaded && PagedVideoHandler.Instance.feedVideosLoaded && PagedVideoHandler.Instance.feedVideosLoaded)
        {
            allDataLoaded = true;
        }
    }
    */


    //TODO: LOGICA PARA Cambio de pagina en base a cambio canvas, llama esta funcion el boton de la pagina, pasando como parametro ese index de pagina (primera 0)
    public void NewPage(int newPage)
    {


        // Si la pagina está precisamente activa la cierra
        //if (currentPage.gameObject.activeSelf == true)
        //{
        //    ClosePage(newPage);
        //    print("cerrar pagina");
        //}
        //else
        //{
        if (allowButtonClick)
        {
            StartCoroutine("NewPageCoroutine", newPage);
            print("nueva pagina");


            timeEnable = true;
            // Resetea el tiempo si hay un cambio de pagina
            timeCounter = 0;

        }

        //}

    }


    public IEnumerator NewPageCoroutine(int newPage)
    {

        // solo cambia la pagina si no se esta sobre la pagina actual
        // cambio comportamiento para que si esta cerrada o abierta lo ejecute igualmente

        //if(newPage == 0 && currentPage.active == false )
        if (newPage == currentPageIndex && currentPage.active == false)
        {

            // TODO Poner delay en caso de botones Globales
            if (delayedStart)
                yield return new WaitForSeconds(1f);


            allowButtonClick = false;


            //if (!allowButtonClick)
            //{
            //allowButtonClick = false;
            yield return StartCoroutine("ChangePage", newPage);
                //allowButtonClick = true;
                print("unable to click");
            //}





        }


        else if (newPage != currentPageIndex)
        {

            // TODO Poner delay en caso de botones Globales
            if (delayedStart)
                yield return new WaitForSeconds(1f);


            allowButtonClick = false;


            //if (!allowButtonClick)
            //{
            //allowButtonClick = false;
            yield return StartCoroutine("ChangePage", newPage);
                //allowButtonClick = true;
                print("unable to click");
            //}

        } else if(currentPage.gameObject.activeSelf == true)
        {
            if (allowButtonClose)
            {
                allowButtonClose = false;
                ClosePage(newPage);
                print("cerrar pagina activa");
            }
        }


    }

    public void OpenExpandText()
    {


        expandTextWindow.SetActive(true);


    }


    public void CloseExpandText()
    {


        expandTextWindow.SetActive(false);


    }



    //TODO: Logica para cerrar con cruz ventana o pop up
    public void ClosePage(int thisPageNumber)
    {


        if (thisPageNumber == currentPageIndex)
        {

            // cierra la pagina y vuelve a la opacidad normal del paño
            StartCoroutine("ClosingPage", thisPageNumber);
            // Mover de lugar
            // StartCoroutine(FadeIn());

            // cuando se elimina la transicion vuelve a habilitar el movimiento de camara
            //CameraDragController.instance.allowFollow = true;
            //print(CameraDragController.instance.allowFollow);

            // deja de contar el tiempo
            timeEnable = false;

        }


    }
    public void ButtonSnap(int page)
    {

        CameraDragController.instance.doDrag = false;
        CameraDragController.instance.doDrag = false;


        for (int i = 0; i <= pages.Count; i++)
        {

            // currentPage es el numero de pagina pero con la referencia de tipo gameobject
            Vector2 _preSnapButtonPosition = Vector2.Lerp(CameraDragController.instance.gameObject.transform.position, pages[page].transform.position, Time.deltaTime * 50);

            CameraDragController.instance.gameObject.transform.position = new Vector3(

                Mathf.CeilToInt
                (_preSnapButtonPosition.x),
                Mathf.CeilToInt
                (_preSnapButtonPosition.y),
                -25);

            print("DOING BUTTON SNAP!!!" + pages[page].transform.position);

            doButtonSnap = false;

        }

        /*
        foreach (var item in pages)
        {
            // currentPage es el numero de pagina pero con la referencia de tipo gameobject
            Vector2 _preSnapButtonPosition = Vector2.Lerp(CameraDragController.instance.gameObject.transform.position, item.transform.position, Time.deltaTime * 50);

            CameraDragController.instance.gameObject.transform.position = new Vector3(

                Mathf.CeilToInt
                (_preSnapButtonPosition.x),
                Mathf.CeilToInt
                (_preSnapButtonPosition.y),
                -25);

            print("DOING BUTTON SNAP!!!" + item.transform.position);

            doButtonSnap = false;
        }
        */

        /*
        // currentPage es el numero de pagina pero con la referencia de tipo gameobject
        Vector2 _preSnapButtonPosition = Vector2.Lerp(CameraDragController.instance.gameObject.transform.position, snapPopArea.transform.position, Time.deltaTime * 50);

        CameraDragController.instance.gameObject.transform.position = new Vector3(

            Mathf.CeilToInt
            (_preSnapButtonPosition.x),
            Mathf.CeilToInt
            (_preSnapButtonPosition.y),
            -25);

        print("DOING BUTTON SNAP!!!" + snapPopArea.transform.position);

        doButtonSnap = false;
        */

    }


    //Corrutina de cambio de pagina, pasando como parametro ese numero de pagina
    public IEnumerator ChangePage(int newPage)
    {

        //if (allowButtonClick)
        //{
            //allowButtonClick = false;

            //Cierra la pagina actual
            if (currentPage.gameObject.activeSelf)
            {

                //yield return new WaitForSeconds(clipEntrada.length);


                currentPage.GetComponent<Animator>().SetTrigger("hide");

                //yield return new WaitForSeconds(clipSalida.length);

                yield return new WaitForSeconds(2f);

                allowButtonClose = true;


                currentPage.SetActive(false);

                
            }

            //Abre la nueva pagina


            // Pasa como pagina de index del grupo la nueva pagina ingresada
            currentPageIndex = newPage;
            // Pasa como pagina la activa la pagina ingresada
            currentPage = pages[currentPageIndex];


            //if (delayedStart)
            //yield return new WaitForSeconds(1.5f);


            currentPage.SetActive(true);

            yield return new WaitForSeconds(clipEntrada.length);

            //if (currentPage.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !currentPage.GetComponent<Animator>().IsInTransition(0))
            
            // ST
            allowButtonClick = true;
            print("ready to click");


            yield return null;

        //}
    }


    public IEnumerator ClosingPage(int thisPageNumber)
    {

        if (currentPage.gameObject.activeSelf)
        {
            //yield return new WaitForSeconds(clipEntrada.length);


            currentPage.GetComponent<Animator>().SetTrigger("hide");

            //yield return new WaitForSeconds(clipSalida.length);

            yield return new WaitForSeconds(2f);

            currentPage.SetActive(false);

            allowButtonClose = true;
        }

        //StartCoroutine(FadeIn());

        yield return null;
    }



    /*
    public IEnumerator FadeOut()
    {
        // Activa el chequeo de que esta activada la capa de transparencia
        transparent = true;
        alphaLayer.gameObject.SetActive(true);

        //NOTE: Valores de alpha son proporciones
        for (float alpha = 0f; alpha < fadeProportion; alpha += Time.deltaTime * 2f)
        {

            //TODO: FADE CAMBIO OPACIDAD
            imageComponent.color = new Color(imageComponent.color.r, imageComponent.color.g, imageComponent.color.b, alpha);

            yield return null;

        }

    }


    public IEnumerator FadeIn()
    {

        // Solo deshabilita el fade por cierre de paginas, no por cambios entre ellas
        transparent = false;
        //alphaLayer.gameObject.SetActive(false);

        //NOTE: Valores de alpha son proporciones
        for (float alpha = fadeProportion; alpha > 0f; alpha -= Time.deltaTime)
        {

            //TODO: FADE CAMBIO OPACIDAD
            imageComponent.color = new Color(imageComponent.color.r, imageComponent.color.g, imageComponent.color.b, alpha);

            yield return null;

            if (alpha == 0)
            {
                alphaLayer.gameObject.SetActive(false);
            }

        }

    }
    */

}
