﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class VideoStream : MonoBehaviour
{
    public RawImage image;

    public string url;


    void Start()
    {
        StartCoroutine(GetMovieTexture());
    }

    IEnumerator GetMovieTexture()
    {
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetMovieTexture(url))
        {

            Debug.Log("Cargando video...");

            yield return www.Send();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                MovieTexture myClip = DownloadHandlerMovieTexture.GetContent(www);
                image.texture = myClip;
                AudioSource myAudio = GetComponent<AudioSource>();
                myAudio.clip = myClip.audioClip;
                myClip.Play();
                myAudio.Play();
            }
        }
    }
}
