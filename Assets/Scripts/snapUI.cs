﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class snapUI : MonoBehaviour, IEndDragHandler, IBeginDragHandler, IDragHandler {

    public RectTransform centerSnapObject;

    public RectTransform movingListContent;

    private ScrollRect myScrollRect;

    // Relacion de area sobre la que se hará el snapping
    [SerializeField]
    private int snapAreaRatio = 2;

    [SerializeField]
    RectTransform[] snappingHelperObjects;
    //List<RectTransform> snappingHelperObjects = new List<RectTransform>();

    //private UI_InfiniteScroll myInfiniteComponent;
    private float[] snapXAreaMin;
    private float[] snapXAreaMax;
    private float[] snapYAreaMin;
    private float[] snapYAreaMax;

    private bool doSnap = false;

    // tiempo que tarda en moverse desde el punto de inicio hasta el punto de llegada
    [HideInInspector]
    float timeTakenDuringLerp = 1f;
    // Variable donde guardaremos el tiempo desde que comenzo el lerp
    [HideInInspector]
    private float _timeStartedLerping;



    private UI_InfiniteScroll myInfiniteComponent;



    // Use this for initialization
    void Start () {

        ScrollRect myScrollRect = GetComponent<ScrollRect>();

        //myScrollRect.onValueChanged.AddListener(MoveScroll);


        UI_InfiniteScroll myInfiniteComponent = GetComponent<UI_InfiniteScroll>();

        snappingHelperObjects = new RectTransform[movingListContent.childCount];


        for (int i = 0; i < movingListContent.childCount; i++)
        {
            snappingHelperObjects[i] = movingListContent.GetChild(i).GetComponent<RectTransform>(); ;
        }


        //snappingHelperObjects = myInfiniteComponent.items;

        // Asignacion de las areas de pantalla sobre las que se realizará el snapping de la camara para el area A, B, C...etc
        // NO PONER NEW PORQUE BORRA REFERENCIAS DESDE EDITOR
        snapXAreaMin = new float[snappingHelperObjects.Length];
        snapXAreaMax = new float[snappingHelperObjects.Length];
        snapYAreaMin = new float[snappingHelperObjects.Length];
        snapYAreaMax = new float[snappingHelperObjects.Length];


        //snapXAreaMin[0] = snappingHelperObjects[0].anchoredPosition.x - snappingHelperObjects[0].rect.size.x / snapAreaRatio;
        //snapXAreaMax[0] = snappingHelperObjects[0].anchoredPosition.x + snappingHelperObjects[0].rect.size.x / snapAreaRatio;



        Debug.Log("BOTON RANGE: " + snappingHelperObjects[0].name.ToString() + " MIN VALUE: " + snapXAreaMin[0] + " MAX VALUE: " + snapXAreaMax[0]);



        for (int i = 0; i < snappingHelperObjects.Length; i++)
        {

            // Posicion en base anchored position
            //snapXAreaMin[i] = snappingHelperObjects[i].anchoredPosition.x - snappingHelperObjects[i].rect.size.x / snapAreaRatio;
            //snapXAreaMax[i] = snappingHelperObjects[i].anchoredPosition.x + snappingHelperObjects[i].rect.size.x / snapAreaRatio;
            ////snapYAreaMin[i] = snappingHelperObjects[i].anchoredPosition.y - snappingHelperObjects[i].rect.size.y / snapAreaRatio; 
            ////snapYAreaMax[i] = snappingHelperObjects[i].anchoredPosition.y + snappingHelperObjects[i].rect.size.y / snapAreaRatio;

            //RectTransform[] snappingHelperObjects;
            snappingHelperObjects[i] = snappingHelperObjects[i].GetComponent<RectTransform>();



            snapXAreaMin[i] = snappingHelperObjects[i].anchoredPosition.x - snappingHelperObjects[i].rect.size.x / snapAreaRatio;
            snapXAreaMax[i] = snappingHelperObjects[i].anchoredPosition.x + snappingHelperObjects[i].rect.size.x / snapAreaRatio;



            print("STARTING POSITION: " + snappingHelperObjects[i].anchoredPosition);
            print("STARTING SIZE: " + snappingHelperObjects[i].rect.size.x);

            Debug.Log("BOTON RANGE: " + snappingHelperObjects[i].name.ToString() + " MIN VALUE: " + snapXAreaMin[i] + " MAX VALUE: " + snapXAreaMax[i]);
        }



        //print("CENTRO SNAP: " + centerSnapObject.anchoredPosition.x);// position.x);
        //print("CENTRO LIST CONTENT: " + movingListContent.anchoredPosition.x); //RectTransformUtility.WorldToScreenPoint(null, movingListContent.transform.position));




    }
	
	// Update is called once per frame
	void Update () {

        _timeStartedLerping += Time.deltaTime;
        if (_timeStartedLerping > timeTakenDuringLerp)
        {
            _timeStartedLerping = timeTakenDuringLerp;
        }


        if (doSnap)
        {

            StartCoroutine(Snap());

        }


    }


    //Funcion que controla snap en puntos de canvas
    IEnumerator Snap()
    {
        for (int i = 0; i < snappingHelperObjects.Length; i++)
        {
            
            //Posicion en screen(pixel) en base anchored position
            //print("CENTRO SNAP: " + centerSnapObject.anchoredPosition.x);// position.x);
            //print("CENTRO LIST CONTENT: " + movingListContent.anchoredPosition.x); //RectTransformUtility.WorldToScreenPoint(null, movingListContent.transform.position));


            // Checkeo que el centro de la camara este dentro del Area A,B,C,etc , si es asi ejecutar el drag
            if (movingListContent.anchoredPosition.x > snapXAreaMin[i] && movingListContent.anchoredPosition.x < snapXAreaMax[i]) //&& centerSnapObject.position.y > snapYAreaMin[i] && centerSnapObject.position.y < snapYAreaMax[i])
            {

                print("ENTRADA EN AREA DE SNAP CHILD");

                //float timeSinceStarted = Time.time - _timeStartedLerping;
                float percentageComplete = _timeStartedLerping / timeTakenDuringLerp;

                // Calculo del area sobre la que esta dispuesto el child element de esa pagina, sistema de paginacion
                Vector3[] childPosition = new Vector3[snappingHelperObjects.Length];
                
                childPosition[i] = new Vector3(movingListContent.rect.size.x / snappingHelperObjects.Length + snappingHelperObjects[i].anchoredPosition.x, 0, 0);


                Vector2 _preSnapPosition = Vector3.Lerp(movingListContent.anchoredPosition, (Vector3)centerSnapObject.anchoredPosition + childPosition[i], percentageComplete / 3); //snapTransform[i].transform.position, percentageComplete / 3);

                movingListContent.anchoredPosition = new Vector3(

                    Mathf.CeilToInt
                    (_preSnapPosition.x),
                    Mathf.CeilToInt
                    (_preSnapPosition.y),
                    0);

                //print("PORCENTAGE: " + percentageComplete);

                //doSnap = false;

                yield return null;

            }
            

        }



    }


    public void OnDrag(PointerEventData data)
    {

        doSnap = false;

        print("dragging" + this.name);


    }



    public void OnBeginDrag(PointerEventData data)
    {


        doSnap = false;

        print("start drag" + this.name);


    }


    public void OnEndDrag(PointerEventData data)
    {

        doSnap = true;

        print("Finished drag" + this.name);

    }


    void MoveScroll()
    {

    }


}
