﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


[RequireComponent(typeof(AudioSource))]

public class videoTextureController : MonoBehaviour {

    [Header("Path para el streaming de video")]
    public string movieUrl;

    //[Header("Locacion de archivo importado como movietexture - Legacy")]
    //public MovieTexture myMovie;

    private AudioSource myAudio;


	// Use this for initialization
	void Start () {

        //Logica para streaming online de videos
        //var www = new WWW(movieUrl);
        //var onLineMovieTexutre = www.GetMovieTexture;// www.movie;

        StartCoroutine(GetMovieTexture());
        // Sacar temporal videos from Resources
        /*
        GetComponent<RawImage>().texture = myMovie as MovieTexture;

        myAudio = GetComponent<AudioSource>();

        myAudio.clip = myMovie.audioClip;

        //myAudio.Play();
        //myMovie.Play();

        myMovie.loop = true;
        */
        

        
	}

    IEnumerator GetMovieTexture()
    {
        //Start download data
        var www = new WWW(movieUrl);

        // Asegurarse que la data esta totalmente cargada // Todo generar un Loading animation o gif
        var onlineMovieTexture = www.GetMovieTexture();
        //var onlineClip = www.GetAudioClip();

        //yield return new WaitUntil(onlineMovieTexture.isReadyToPlay);


        while (!onlineMovieTexture.isReadyToPlay)
        {
            print("cargando video");
            yield return null;
        }
        
        // Asigna el video bajado a la textura
        GetComponent<RawImage>().texture = onlineMovieTexture;

        // Play both movie & sound
        onlineMovieTexture.Play();


        // Assign clip to audio source
        // Sync playback with audio
        var aud = GetComponent<AudioSource>();
        //if (onlineMovieTexture.audioClip != null)
        //{
            aud.clip = onlineMovieTexture.audioClip;
            //aud.clip = onlineClip;
            aud.clip.name = "sonido del stream de video";
        //aud.clip.loadInBackground;

        //if (aud != null)
        try
        {
            aud.Play();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
        //}

        Debug.Log("Video is Playing");

        yield return null;
        /*
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetMovieTexture(movieUrl));
        {
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                MovieTexture myClip = DownloadHandlerMovieTexture.GetContent(www);
            }
        }
        */
    }

    /*
    IEnumerator Play()
    {

        m.Play();
        audio.Play();

    }
    */

    /*
	// Update is called once per frame
	void Update () {

        if (!myMovie.isPlaying)
        {


            Play();
            Debug.Log("reproducir video");


        }
		
	}

    public void Play()
    {
        myMovie.Play();
        myAudio.Play();
    }

    public void Pause() { myMovie.Pause(); myAudio.Pause(); }

    public void Again() { myMovie.Stop(); myMovie.Play(); myAudio.Stop(); myAudio.Play(); }
    */
}
